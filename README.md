``# <p align="center">HududGaz project</p>

## Install

1. clone project    
   ``
   git clone https://github.com/Bekmurod3388/HududGaz.git
   ``
2. create .env file     
   ``
   cp .env.example .env
   ``
3. edit .env file db

```
APP_URL=http://yourhost
DB_HOST=dbhost
DB_DATABASE=homestead
DB_USERNAME=homestead
DB_PASSWORD=secret
```

4. Install composer packages     
   ``
   composer install   
   ``
5. Generate app key  
   ``
   php artisan key:generate
   ``
6. Create user  
``
php artisan migrate --seed 
``
7.  migrate and seed db  
    ``
    php artisan voyager:install
    ``
8. npm and vue  
    ``
    npm install; npm run dev
    ``
9. Use cron for schedule  
   ``
   php artisan schedule:run
   ``
9. Run db Queue for background jobs and queue  
   `` php artisan queue:work ``
   
You are done with config.  
Happy hacking
