<?php

namespace App\Console\Commands;

use App\Models\DateSetting;
use App\Models\Proposition;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CheckTimeLimitCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:time';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check time limit and change status of elements';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $now = Carbon::now();
        $start = Carbon::now()->setTime(9, 0);
        $end = Carbon::now()->setTime(18, 0);
        if ($now < $start and $now > $end)
            return false;

        $dateSetting = DateSetting::query()
            ->where('from', '<', $now->format(Carbon::DEFAULT_TO_STRING_FORMAT))
            ->where('to', '>', $now->format(Carbon::DEFAULT_TO_STRING_FORMAT))->first();

        if (!is_null($dateSetting) and $dateSetting->type == DateSetting::HOLIDAYS)
            return false;
        elseif (!is_null($dateSetting) and $dateSetting->type == DateSetting::WORKING_DAYS)
            return $this->checkTime();
        elseif ($now->isSaturday() or $now->isSunday())
            return false;
        return $this->checkTime();
    }

    public function checkTime()
    {
        $propositions = Proposition::query()->whereNotIn('status', [9, 21])->get();
        if (is_null($propositions))
            return false;
        foreach ($propositions as $proposition) {
            if (!is_null($proposition->minutes)) {
                if ($proposition->delay_s == 0) {
                    if ($proposition->minutes > 5) {
                        $proposition->minutes = $proposition->minutes - 5;
                    } else {
                        $proposition->minutes = $proposition->minutes + 5;
                        $proposition->delay_s = 1;
                    }
                } elseif ($proposition->delay_s == 1) {
                    $proposition->minutes = $proposition->minutes + 5;
                }
                $proposition->save();
            }
//            else dd($proposition->id);
        }

        return true;
    }
}
