<?php


namespace App\Filters;


use App\QueryFilter\QueryFilter;

class EmployeesFilter extends QueryFilter
{
    public function id($value)
    {
        return $this->builder->where('id', $value);
    }


    public function role_id($value)
    {
        return $this->builder->where('role_id', $value);
    }

    public function name($value)
    {
        return $this->builder->where('name', $value);
    }

    public function email($value)
    {
        return $this->builder->where('email', $value);
    }



}
