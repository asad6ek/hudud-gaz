<?php


namespace App\Filters;


use App\QueryFilter\QueryFilter;

class EquipmentFilter extends QueryFilter
{
    public function id($value)
    {
        return $this->builder->where('id', $value);
    }




    public function name($value)
    {
        return $this->builder->where('name', $value);
    }





}

