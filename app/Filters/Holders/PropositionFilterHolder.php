<?php


namespace App\Filters\Holders;


use App\Traits\ClassFormInputableTrait;
use App\View\Interfaces\IFormInputable;
use ArrayAccess;
use Illuminate\Contracts\Support\Arrayable;
use App\QueryFilter\Traits\ArrayAccessTrait;
use App\QueryFilter\Traits\LazyLoadTrait;
use App\QueryFilter\Traits\ToArrayTrait;

class PropositionFilterHolder implements Arrayable, ArrayAccess, IFormInputable
{
    use ToArrayTrait;
    use LazyLoadTrait;
    use ArrayAccessTrait;
    use ClassFormInputableTrait;


    public $id;

    public $number;

    public $type;

    public $status;

    public $delay_s;

    public $activity_type;

    public $region_id;

}
