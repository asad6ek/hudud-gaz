<?php


namespace App\Filters\Holders;


use App\QueryFilter\Traits\ArrayAccessTrait;
use App\QueryFilter\Traits\LazyLoadTrait;
use App\QueryFilter\Traits\ToArrayTrait;
use App\Traits\ClassFormInputableTrait;
use App\View\Interfaces\IFormInputable;
use Illuminate\Contracts\Support\Arrayable;
use ArrayAccess;

class RecommendationFilterHolder implements Arrayable, ArrayAccess, IFormInputable
{
    use ToArrayTrait;
    use LazyLoadTrait;
    use ArrayAccessTrait;
    use ClassFormInputableTrait;


    public $id;

    public $number;

    public $type;

    public $customer_id;

    public $region_id;

    public $recom_s;

    public function getAttributeLabels(): array
    {
        return [
            'number' => 'Ariza raqami',
            'type' => 'Type',
        ];
    }
}
