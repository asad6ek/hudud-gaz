<?php


namespace App\Filters;


use App\QueryFilter\QueryFilter;

class PropositionFilter extends QueryFilter
{
    public function id($value)
    {
        return $this->builder->where('id', $value);
    }

    public function number($value)
    {
        return $this->builder->where('number', $value);
    }

    public function type($value)
    {
        return $this->builder->where('type', $value);
    }

    public function region_id($value)
    {
        return $this->builder->where('region_id', $value);
    }

    public function status($value)
    {
        return $this->builder->whereIn('status', $value);
    }

    public function dealy_s($value)
    {
        return $this->builder->where('dealy_s', $value);
    }

    public function activity_type($value)
    {
        return $this->builder->where('activity_type', $value);

    }

}
