<?php


namespace App\Filters;


use App\QueryFilter\QueryFilter;

class TechConditionFilter extends QueryFilter
{
    public function id($value)
    {
        return $this->builder->where('id', $value);
    }

    public function qrcode($value)
    {
        return $this->builder->where('qrcode', $value);
    }

    public function prop_id($value)
    {
        return $this->builder->where('prop_id', $value);
    }


}
