<?php

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;


function isActiveRoute($route, $strict = false, $className = 'active')
{
    $name = Route::currentRouteName();
    if (!$strict) {
        return Str::is($route . '*', $name) ? $className : '';
    }
    return $name === $route ? $className : '';
}

function addAlert($message, $type = 'success') {
    session()->flash('message', $message);
    session()->flash('message-type', $type);
}

function modalMessage($type, $message)
{
    session()->flash('modal-message', $message);
    session()->flash('modal-type', $type);
}

function resourceWithUpload($url, $controller)
{
    Route::post($url . '/process', $controller . '@processImage');
    Route::delete($url . '/revert', $controller . '@revertImage');
    Route::post($url . '/{id}/process', $controller . '@processImage');
    Route::delete($url . '/{id}/revert', $controller . '@revertImage');
    Route::get($url . '/{id}/load/{path}', $controller . '@loadImage');
    Route::resource($url, $controller)->names([
        'index' => $url . '.index',
        'create' => $url . '.create',
        'store' => $url . '.store',
        'edit' => $url . '.edit',
        'update' => $url . '.update',
        'destroy' => $url . '.delete',
    ]);
}

function apiUpload($url, $controller)
{
    Route::post($url . '/process', $controller . '@processImage');
    Route::delete($url . '/revert', $controller . '@revertImage');
    Route::get($url . '/load/{path}', $controller . '@loadImage');
    Route::post($url . '/{id}/process', $controller . '@processImage');
    Route::delete($url . '/{id}/revert', $controller . '@revertImage');
    Route::get($url . '/{id}/load/{path}', $controller . '@loadImage');

}

function resource($url, $controller, $name = null)
{
    Route::resource($url, $controller)->names([
        'index' => ($name ?: $url) . '.index',
        'create' => ($name ?: $url) . '.create',
        'store' => ($name ?: $url) . '.store',
        'edit' => ($name ?: $url) . '.edit',
        'update' => ($name ?: $url) . '.update',
        'destroy' => ($name ?: $url) . '.delete',
        'show' => ($name ?: $url) . '.show',
    ]);
}

function setImage($user) {
    if ($user->avatar)
        return '/storage/users/' . $user->avatar;
    return '/img/avatar.svg';
}

if (!function_exists('ip')) {

    function ip()
    {
//        return Request::getClientIp();
    }
}

if (!function_exists('user_role')) {

    /**
     * @param string $role
     * @return mixed
     */
    function user_role(string $role)
    {
        return Session::get('role') === $role;
    }
}

if (!function_exists('menus')) {
    function menus()
    {
        return \App\Models\Page::query()
            ->where('status', 1)
            ->where('isShownMenu', 1)
            ->get();
    }
}
if (!function_exists('news')) {
    function news()
    {
        return \App\Models\ShopNews::query()
            ->where('status', 1)
            ->get();
    }
}

if (!function_exists('isActivePage')) {
    function isActivePage($url, $className = 'active')
    {

        return Request::path() === $url ? $className : '';
    }
}

if (!function_exists('shop')) {
    function shop()
    {
        return optional(Shop::shop());
    }
}

function numEmoji($number)
{

    $number = (string)$number;
    $text = '';
    $arr = [
        0 => "0️⃣",
        1 => "1️⃣",
        2 => "2️⃣",
        3 => "3️⃣",
        4 => "4️⃣",
        5 => "5️⃣",
        6 => "6️⃣",
        7 => "7️⃣",
        8 => "8️⃣",
        9 => "9️⃣",
    ];
    $count = strlen($number) - 1;
    for ($i = 0; $count >= $i; $i++) {
        $text .= $arr[$number[$i]];
    }
    return $text;
}


function menu_items($items)
{
    foreach ($items as $item) {
        $item->href = $item->link(true);
        $item->title = __($item->title);
        if (count($item->children) > 0)
            foreach ($item->children as $child){
                $child->href = $child->link(true);
                $child->title = __($child->title);
            }
    }

    return $items;
}

function dateFixed($date) {
    $res = date_create($date);
    return date_format($res, "d.m.Y");
}

function getOrganizationName(): string {
    return \Illuminate\Support\Facades\Cache::get('general_organization_name') ?? "Hududgaz Xorazm";
}

function get_general_settings()
{
    return Cache::get('general_settings');
}

function get_shareholder_name()
{
    return Cache::get('general_settings')['shareholder_name'] ?? 'Худуд Газ Таъминот';
}

function get_branch_name() {
    return Cache::get('general_settings')['branch_name'] ?? 'ХудудГазХоразм';
}

function checkMac() {
    $get_address = exec('getmac');
    $mac = substr($get_address, 0, 17);

    $json = auth()->user()->getAttribute('transition_step');
    $json = json_decode($json);
    if (!$json)
        $json = [[]];
    foreach ($json as $step) {
        if ($step == $mac) {
            $json = [];
            break;
        }
    }
    return $json;
//    if ($json) {
//        auth()->logout();
//        return redirect('/');
//    }
}

function extendedDate($date, bool $reverse = false): string {
    $months = [
        'Январ',
        "Феврал",
        "Март",
        "Апрел",
        "Май",
        "Июн",
        "Июл",
        "Август",
        "Сентябр",
        "Октябр",
        "Ноябр",
        "Декабр"
    ];

    $date = date_create($date);
    $day = date_format($date, 'j');
    $month = date_format($date, 'n');
    $year = date_format($date, 'Y');

    if ($reverse)
        return $year . " йил" . ' ' . $day . ' ' . $months[$month - 1];
    return $day . ' ' . $months[$month - 1] . ' ' . $year . " йил";
}
