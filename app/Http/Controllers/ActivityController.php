<?php

namespace App\Http\Controllers;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use App\Models\Activity;
use App\Http\Requests\ActivityRequest;

class ActivityController extends Controller
{
    public function index() {
        try {
            $this->authorize('browse_statuses');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        $data = Activity::all();
        return view('voyager::admin.activity_type', ['activities' => $data]);
    }

    public function store(ActivityRequest $request) {
        try {
            $this->authorize('add_users');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        $data = $request->validated();
        $activity = new Activity($data);
        $activity->save();

        addAlert(__('table.save_success'));
        return redirect()->route('admin.activity_type.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ActivityRequest $request
     * @param Activity $activity_type
     * @return Application|Redirector|RedirectResponse
     */
    public function update(ActivityRequest $request, Activity $activity_type) {
        try {
            $this->authorize('edit_users');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        $data = $request->validated();
        $activity_type->fill($data);
        $activity_type->save();

        addAlert(__('table.update_success'));
        return redirect()->route('admin.activity_type.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return Application|RedirectResponse|Redirector
     */
    public function destroy($id) {
        try {
            $this->authorize('delete_users');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        Activity::query()->find($id)->delete();
        addAlert(__('table.delete_success'));
        return redirect()->route('admin.activity_type.index');
    }
}
