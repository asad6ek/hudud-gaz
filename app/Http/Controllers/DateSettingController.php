<?php

namespace App\Http\Controllers;

use App\Http\Requests\DateSettingRequest;
use App\Models\DateSetting;
use App\Services\CRUD\DateSettingService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;

class DateSettingController extends Controller
{

    /**
     * @var DateSettingService
     */
    private $service;

    public function __construct(DateSettingService $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View|Response
     */
    public function index()
    {
        try {
            $this->authorize('browse_date_settings');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        return view('voyager::date_setting.index', ['models' => $this->service->filterData()->get()->groupBy('type')]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View|Redirector|RedirectResponse
     */
    public function create()
    {
        try {
            $this->authorize('add_date_settings');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }
        $model = new DateSetting();
        return view('voyager::date_setting.create', ['model' => $model]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return RedirectResponse
     */
    public function store(DateSettingRequest $request)
    {
        try {
            $this->authorize('add_date_settings');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        $this->service->create($request->validated());

        return redirect()->route('admin.date_settings.index');


    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return void
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DateSetting $date_setting
     * @return RedirectResponse
     */
    public function destroy(DateSetting $date_setting)
    {
        try {
            $this->authorize('delete_date_settings');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }
        $this->service->delete($date_setting->id);
        alertMessage('success', trans('messages.deleted_successfully'));
        return redirect()->route('admin.date_settings.index');
    }
}
