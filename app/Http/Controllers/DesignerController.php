<?php

namespace App\Http\Controllers;

use App\Exceptions\InvalidArgumentException;
use App\Http\Requests\DesignerRequest;
use App\Models\Designer;
use App\Services\CRUD\DesignerService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;

class DesignerController extends Controller {
    private $service;

    public function __construct(DesignerService $service) {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|RedirectResponse|Redirector
     */
    public function index() {
        try {
            $this->authorize('browse_designers');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        $models = Designer::all();
        return view('voyager::designers.index', ['models' => $models]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create() {
        try {
            $this->authorize('browse_designers');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        $model = new Designer();
        return view('voyager::designers.create', ['model' => $model]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param DesignerRequest $request
     * @return Application|RedirectResponse|Redirector void
     * void
     * @throws InvalidArgumentException
     */
    public function store(DesignerRequest $request) {
        try {
            $this->authorize('browse_designers');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        $data = $request->validated();
        $this->service->create($data);
        addAlert(__('table.save_success'));
        return redirect()->route('admin.designers.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param
     * Designer $designer
     * @return Application|RedirectResponse|Redirector
     */
    public function edit(Designer $designer) {
        try {
            $this->authorize('browse_designers');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        return view('voyager::designers.update', ['model' => $designer]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param DesignerRequest $request
     * @param Designer $designer
     * @return Application|RedirectResponse|Redirector
     */
    public function update(DesignerRequest $request, Designer $designer) {
        try {
            $this->authorize('browse_designers');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        $data = $request->validated();
        $this->service->update($data, $designer);
        addAlert(__('table.update_success'));
        return redirect()->route('admin.designers.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return RedirectResponse
     * @throws InvalidArgumentException
     */
    public function destroy($id) {
        try {
            $this->authorize('browse_designers');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        $this->service->delete($id);
        addAlert(__('table.delete_success'));
        return redirect()->route('admin.designers.index');
    }
}
