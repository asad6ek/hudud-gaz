<?php

namespace App\Http\Controllers;

use App\Http\Requests\EmployeeRequest;
use App\Models\BaseModel;
use App\Models\User;
use App\Services\CRUD\EmployeesService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use TCG\Voyager\Models\Role;

class EmployeesController extends Controller
{

    private $service;

    public function __construct(EmployeesService $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return View|RedirectResponse
     */
    public function index()
    {
        try {
            $this->authorize('browse_users');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }
        $users = User::query()->with('role')->orderBy('id', 'desc')->get();

        return view('voyager::employees.index', ['models' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View|RedirectResponse
     */
    public function create()
    {
        try {
            $this->authorize('add_users');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }
        $model = new User();
        $roles = Role::query()->pluck('display_name', 'id');

        return view('voyager::employees.create', ['roles' => $roles, 'model' => $model, 'locales' => BaseModel::localeName()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param EmployeeRequest $request
     * @return RedirectResponse
     */
    public function store(EmployeeRequest $request)
    {
        try {
            $this->authorize('add_users');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        $data = $request->validated();
        $this->service->create($data);
        addAlert(__('table.save_success'));
        return redirect()->route('admin.employees.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param User $employee
     * @return Application
     */
    public function edit(User $employee)
    {
        try {
            $this->authorize('edit_users');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }
        $roles = Role::query()->pluck('display_name', 'id');

        return view('voyager::employees.update', ['model' => $employee, 'roles' => $roles, 'locales' => BaseModel::localeName()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param EmployeeRequest $request
     * @param User $employee
     * @return RedirectResponse
     */
    public function update(EmployeeRequest $request, User $employee)
    {
        try {
            $this->authorize('edit_users');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }
        $data = $request->validated();
        $this->service->update($data, $employee);
        addAlert(__('table.update_success'));
        return redirect()->route('admin.employees.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $employee
     * @return RedirectResponse
     */
    public function destroy(User $employee)
    {
        try {
            $this->authorize('delete_users');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }
        $this->service->delete($employee->id);
        addAlert(__('table.delete_success'));
        return redirect()->route('admin.employees.index');
    }
}
