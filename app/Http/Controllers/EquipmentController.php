<?php

namespace App\Http\Controllers;


use App\Http\Requests\EquipmentRequest;
use App\Models\BaseModel;
use App\Models\Equipment;
use App\Models\EquipmentType;
use App\Models\User;

use App\Services\CRUD\EquipmentService;

use App\ViewModels\EquipmentListViewModel;
use Illuminate\Auth\Access\AuthorizationException;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Models\Role;

class EquipmentController extends Controller
{
    private $service;

    public function __construct(EquipmentService $service)
    {
        $this->service = $service;
    }

    public function index(Request $request)
    {
        try {
            $this->authorize('browse_equipments');
        } catch (AuthorizationException $e) {

            return redirect('/');
        }

        return view('voyager::equipments.index',
            new EquipmentListViewModel($this->service, $request, $this->guard()->user()));
    }

    public function add() {
        return Equipment::query()->where('id', '>', 1)->pluck('name', 'id');
    }

    public function types(Equipment $equipment) {
        return $equipment->children->pluck('type', 'id');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|RedirectResponse|Response|Redirector
     */
    public function create()
    {
        try {
            $this->authorize('add_users');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }
        $model = new User();
        $roles = Role::all();

        return view('voyager::equipments.create', ['roles' => $roles, 'model' => $model,'locales'=>BaseModel::localeName(),'regions'=>BaseModel::regions()]);
    }

    public function add_component(Request $request)
    {
        $equipments = Equipment::select('id', 'name')->get();
        return view('voyager::recommendations.components.add_equipment', [
            'index' => $request->get('index'), 'equipments' => $equipments
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param EquipmentRequest $request
     * @return RedirectResponse
     */
    public function store(EquipmentRequest $request)
    {
        try {
            $this->authorize('add_users');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        $data = $request->validated();
//        dd($data, $request->all());
        $this->service->create($data);

        addAlert(__('table.save_success'));
        return redirect()->route('admin.equipments.index');
    }

    /**
     * Display the specified resource.
     *
     * @param Equipment $equipment
     * @return Application|RedirectResponse|Response|Redirector
     */
    public function show(Equipment $equipment)
    {
        try {
            $this->authorize('browse_equipments_types');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }
        $equipment_types = $equipment->children()->orderBy('order')->get();

        return Voyager::view('voyager::equipments.index_type',['equipment_types'=>$equipment_types,'equipment'=>$equipment]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Equipment $equipment
     * @return Application|RedirectResponse|Response|Redirector
     */
    public function edit(Equipment $equipment)
    {
        try {
            $this->authorize('edit_equipments');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }
        return Voyager::view('voyager::equipments.update', ['model' => $equipment] );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param EquipmentRequest $request
     * @param Equipment $equipment
     * @return Application|RedirectResponse|Response|Redirector
     */
    public function update(EquipmentRequest $request, Equipment $equipment)
    {
        try {
            $this->authorize('edit_equipments');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }
        $data = $request->validated();
        $this->service->update($data, $equipment);

        addAlert(__('table.update_success'));
        return redirect()->route('admin.equipments.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Equipment $equipment
     * @return RedirectResponse
     */
    public function destroy(Equipment $equipment)
    {
        try {
            $this->authorize('delete_equipments');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }
        $this->service->delete($equipment->id);
        addAlert(__('table.delete_success'));
        return redirect()->route('admin.equipments.index');
    }

    public function save_equipment_type(Request $request, Equipment $equipment)
    {
        try {
            $this->authorize('add_equipments_types');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }
        $data = $request->validate([
            'type' => ['required'],
            'order' => ['required']
        ]);

        $this->service->save_types($data, $equipment);

        addAlert(__('table.save_success'));
        return redirect()->route('admin.equipments.show',['equipment'=>$equipment]);
    }

    public function delete_equipment_type(Equipment $equipment, EquipmentType $equipmentType)
    {
        try {
            $this->authorize('delete_equipments_types');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        $this->service->delete_type($equipmentType);
        addAlert(__('table.delete_success'));
        return redirect()->route('admin.equipments.show',['equipment'=>$equipment]);
    }

    public function update_equipment_type(Request $request, Equipment $equipment, EquipmentType $equipmentType)
    {
        try {
            $this->authorize('add_equipments_types');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        $data = $request->request->all();
        $this->service->update_types($data, $equipmentType);
        addAlert(__('table.update_success'));
        return redirect()->route('admin.equipments.show',['equipment'=>$equipment]);
    }
}
