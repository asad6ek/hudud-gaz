<?php

namespace App\Http\Controllers;

use App\Http\Requests\MontageEmployeeRequest;
use App\Models\MontageFirm;
use App\Models\MontageEmployee;
use App\Services\CRUD\MontageEmployeeService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;

class MontageEmployeeController extends Controller
{

    private $service;

    public function __construct(MontageEmployeeService $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @param MontageFirm $montage_firm
     * @return Application|Factory|View|RedirectResponse|Redirector
     */
    public function index(MontageFirm $montage_firm)
    {
        try {
            $this->authorize('browse_montage_employees');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        $models = $montage_firm->getEmployees()->get();
        return view('voyager::montage_firms.employees.index', ['montage_firm' => $montage_firm, 'models' => $models]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param MontageFirm $montage_firm
     * @return Application|Factory|View|RedirectResponse|Redirector
     */
    public function create(MontageFirm $montage_firm)
    {
        try {
            $this->authorize('browse_montage_employees');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        $model = new MontageEmployee();
        return view('voyager::montage_firms.employees.create', ['montage_firm' => $montage_firm, 'model' => $model]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param MontageEmployeeRequest $request
     * @param $montage_firm
     * @return Application|RedirectResponse|Redirector
     */
    public function store(MontageEmployeeRequest $request, $montage_firm)
    {
        try {
            $this->authorize('edit_montage_employees');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        $data = $request->validated();
        $this->service->create($data);
        addAlert(__('table.save_success'));
        return redirect()->route('admin.montage_employees.index', ['montage_firm' => $montage_firm]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param MontageFirm $montage_firm
     * @param MontageEmployee $employee
     * @return Application|Factory|View|RedirectResponse
     */
    public function edit(MontageFirm $montage_firm, MontageEmployee $employee)
    {
        try {
            $this->authorize('edit_montage_employees');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        return view('voyager::montage_firms.employees.update', ['montage_firm' => $montage_firm, 'model' => $employee]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param MontageEmployeeRequest $request
     * @param $montage_firm
     * @param MontageEmployee $employee
     * @return Application|RedirectResponse|Redirector
     */
    public function update(MontageEmployeeRequest $request, $montage_firm, MontageEmployee $employee)
    {
        try {
            $this->authorize('edit_montage_employees');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        $data = $request->validated();
        $this->service->update($data, $employee);
        addAlert(__('table.update_success'));
        return redirect()->route('admin.montage_employees.index', ['montage_firm' => $montage_firm]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $montage_firm
     * @param  $employee
     * @return Application|RedirectResponse|Redirector
     */
    public function destroy($montage_firm, $employee)
    {
        try {
            $this->authorize('edit_montage_employees');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        $this->service->delete($employee);
        addAlert(__('table.delete_success'));
        return redirect()->route('admin.montage_employees.index', ['montage_firm' => $montage_firm]);
    }
}
