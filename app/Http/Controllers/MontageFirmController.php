<?php

namespace App\Http\Controllers;

use App\Exceptions\InvalidArgumentException;
use App\Http\Requests\MontageFirmRequest;
use App\Models\BaseModel;
use App\Models\MontageFirm;
use App\Services\CRUD\MontageFirmService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;

class MontageFirmController extends Controller
{
    private $service;

    public function __construct(MontageFirmService $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        try {
            $this->authorize('browse_montage_firms');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        $models = MontageFirm::all();

        return view('voyager::montage_firms.index', ['models' => $models]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        try {
            $this->authorize('add_montage_firms');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        $model = new MontageFirm();
        return view('voyager::montage_firms.create', ['model' => $model, 'regions' => BaseModel::regions()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param MontageFirmRequest $request
     * @return Application|RedirectResponse|Redirector
     * @throws InvalidArgumentException
     */
    public function store(MontageFirmRequest $request)
    {
        try {
            $this->authorize('edit_montage_firms');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        $data = $request->validated();
        $this->service->create($data);
        addAlert(__('table.save_success'));
        return redirect()->route('admin.montage_firms.index');
    }

    /**
     * Display the specified resource.
     *
     * @param MontageFirm $firm
     * @return void
     */
    public function show(MontageFirm $firm)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param MontageFirm $montage_firm
     * @return Application|Factory|View|RedirectResponse|Redirector
     */
    public function edit(MontageFirm $montage_firm)
    {
        try {
            $this->authorize('browse_montage_firms');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        return view('voyager::montage_firms.update', ['model' => $montage_firm, 'regions' => BaseModel::regions()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param MontageFirmRequest $request
     * @param MontageFirm $montage_firm
     * @return Application|RedirectResponse|Redirector
     */
    public function update(MontageFirmRequest $request, MontageFirm $montage_firm)
    {
        try {
            $this->authorize('edit_montage_firms');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        $data = $request->validated();
        $this->service->update($data, $montage_firm);
        addAlert(__('table.update_success'));
        return redirect()->route('admin.montage_firms.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return RedirectResponse void
     * void
     */
    public function destroy($id): RedirectResponse
    {
        $this->service->delete($id);
        addAlert(__('table.delete_success'));
        return redirect()->route('admin.montage_firms.index');
    }
}
