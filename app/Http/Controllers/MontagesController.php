<?php

namespace App\Http\Controllers;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use App\Models\TechCondition;
use App\Services\CRUD\MontagesService;

class MontagesController extends Controller
{

    private $service;

    public function __construct(MontagesService $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        try {
            $this->authorize('browse_montages');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }
        $models = TechCondition::query()->whereHas('proposition', function ($query) {
            $query->whereIn('status', [17]);
        })->where(['firm_id' => auth()->user()->firm_id])->get();
        return view('voyager::montages.index', ['models' => $models]);
    }

    public function archive()
    {
        try {
            $this->authorize('browse_archive_montages');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }
        $models = TechCondition::query()->whereHas('proposition', function ($query) {
            $query->where('status', '>=', 18)->where('status', '<=', 22);
        });
        if (user_role('montage_firm')) {
            $models->where(['firm_id' => auth()->user()->firm_id]);
        }
        return view('voyager::montages.archive', ['models' => $models->get()]);
    }


    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function check(Request $request)
    {
        try {
            $this->authorize('check_qr_for_montages');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }
        $user = auth()->user();
        if (today() >= $user->firm->date_expired) {
            addAlert(__('table.alerts.date_expired'), 'warning');
            return redirect()->back();
        } else {
            $tech_condition = $this->service->qrcode_check($request['code'], 16);
            if (!is_null($tech_condition)) {
                $this->service->update(['firm_id' => $user->firm_id], $tech_condition);
                $this->service->update(['status' => 17], $tech_condition->proposition);
            }

            return redirect()->back();
        }
    }


    public function upload(Request $request, TechCondition $tech) {
        try {
            $this->authorize('add_montages');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        if ($request->hasFile('montage')) {
            return $this->service->upload($request, $tech);
        }

        $this->service->update(['pipe_size2' => $request['pipe_size']], $tech->proposition->recommendation);
        return \response()->json(['status' => 'success']);
    }


    public function process()
    {
        try {
            $this->authorize('browse_process_montages');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }
        if (user_role('engineer'))
            $data = [18, 19];
        else $data = [18];
        $models = TechCondition::query()->whereHas('proposition', function ($query) use ($data) {
            $query->whereIn('status', $data);
        });
        if (user_role('montage_firm')) $models->montageFirm();
        $models = $models->get();

        return view('voyager::montages.process', ['models' => $models]);
    }

    public function cancelled()
    {
        try {
            $this->authorize('browse_cancel_montages');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }
        $models = TechCondition::query()->whereHas('proposition', function ($query) {
            $query->where('status', 20);
        })->where(['firm_id' => auth()->user()->firm_id])->get();

        return view('voyager::montages.index', ['models' => $models, 'type' => 1]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @param TechCondition $tech
     * @return Application|Redirector|RedirectResponse
     */
    public function back(Request $request, TechCondition $tech)
    {
        try {
            $this->authorize('edit_montages');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        $tech->proposition->update(['status' => 20]);
        $tech->montage->update(['comment' => $request->get('comment')]);
    }

    public function cancel(TechCondition $tech)
    {
        $this->service->update(['firm_id' => null], $tech);
        $this->service->update(['status' => 16], $tech->proposition);
        return redirect()->route('admin.projects.index');
    }


    /**
     * Display the specified resource.
     *
     * @param TechCondition $montage
     * @return Application|RedirectResponse|Response|Redirector
     */
    public function show(TechCondition $montage)
    {
        try {
            $this->authorize('read_montages');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }
        return redirect('storage/projects/' . $montage->project->path);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param TechCondition $montage
     * @return RedirectResponse
     */
    public function edit(TechCondition $montage)
    {
        try {
            $this->authorize('edit_montages');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        $montage->proposition->update(['status' => 19]);
        return redirect()->back();

    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return void
     */
    public function update(Request $request, int $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return void
     */
    public function destroy($id)
    {
        //
    }


}
