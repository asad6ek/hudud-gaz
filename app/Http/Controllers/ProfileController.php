<?php

namespace App\Http\Controllers;

use App\Models\BaseModel;
use App\Models\User;
use App\Services\CRUD\EmployeesService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller {

    private $service;

    public function __construct(EmployeesService $service) {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param User $user
     * @return Application|Factory|View|RedirectResponse
     */
    public function edit(User $user)
    {
        if (auth()->id() == $user->id) return view('voyager::profile.edit', ['user' => $user, 'locales' => BaseModel::localeName()]);
        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param User $user
     * @return Application|Redirector|RedirectResponse
     */
    public function update(Request $request, User $user)
    {
        $data = $request->all();
        $this->check($request, $user);

        $data['role_id'] = auth()->user()->getAuthIdentifier();
        $data['firm_id'] = auth()->user()->firm_id;
        $this->service->update($data, $user);
        addAlert(__('table.update_success'));
        return redirect('/admin');
    }

    private function check(Request $request, $user) {
        if ($request->has('old_password')) {
            if (!$this->checkPass($request['old_password'], $user->password))
                $this->validate($request, [
                    'old_password' => ['email', 'required'],
                ], [
                    'old_password.email' => __('table.profile.wrong_pass'),
                ], [
                    'old_password' => __('table.profile.wrong_pass'),
                ]);

            if ($request['new_password'] != $request['confirm_new_password']) {
                $this->validate($request, [
                    'new_password' => ['email', 'required', 'min:6'],
                    'old_password' => ['required'],
                    'confirm_new_password' => ['required', 'min:6'],
                ], [
                    'old_password.email' => __('table.profile.not_confirm'),
                    'new_password.email' => __('table.profile.not_confirm'),
                ], [
                    'new_password' => __('table.profile.new_password'),
                    'confirm_new_password' => __('table.profile.confirming_pass')
                ]);
            }
        }
    }

    private function checkPass($pass, $db_pass) {
        return Hash::check($pass, $db_pass);
    }
}
