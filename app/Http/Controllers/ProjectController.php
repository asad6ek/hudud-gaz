<?php

namespace App\Http\Controllers;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use App\Models\TechCondition;
use App\Services\CRUD\ProjectService;

class ProjectController extends Controller
{
    private $service;

    public function __construct(ProjectService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        try {
            $this->authorize('browse_projects');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        $tech_conditions = TechCondition::query()->where('designer_id', auth()->user()->firm_id)
            ->whereHas('proposition', function ($q) {
                $q->where('status', 11);
            })->get();
        return view('voyager::projects.index', ['models' => $tech_conditions]);
    }

    public function check(Request $request)
    {
        try {
            $this->authorize('check_qr_for_projects');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        $user = auth()->user();
        if (today() >= $user->firm->date_end) {
            addAlert(__('table.alerts.date_expired'), 'warning');
            return redirect()->back();
        } else {
            $tech_condition = $this->service->qrcode_check($request['code'], 9);
            if (!is_null($tech_condition)) {
                $this->service->update_status(['designer_id' => $user->firm_id], $tech_condition);
                $this->service->update_status(['status' => 11], $tech_condition->proposition);
            }

            return redirect()->back();
        }
    }

    public function process()
    {
        try {
            $this->authorize('browse_process_projects');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }
        $tech_conditions = TechCondition::query();
        $tech_conditions = $tech_conditions->with('project', 'proposition')
            ->whereHas('proposition', function ($q) {
                $q->whereIn('status', [12, 13, 15]);
            });
        if (user_role('designer_id')) $tech_conditions->where(['designer_id' => auth()->user()->firm_id]);
        $tech_conditions = $tech_conditions->get();;


        return view('voyager::projects.process', ['models' => $tech_conditions]);
    }

    public function cancelled()
    {
        try {
            $this->authorize('browse_cancel_projects');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        $models = TechCondition::query()->whereHas('proposition', function ($query) {
            $query->where('status', 14);
        })->where(['designer_id' => auth()->user()->firm_id])->get();


        return view('voyager::projects.cancelled', ['models' => $models]);
    }

    public function archive()
    {
        try {
            $this->authorize('browse_archive_projects');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }
        $tech_conditions = TechCondition::query();
        if (user_role('designer')) $tech_conditions->where(['designer_id' => auth()->user()->firm_id]);

        $tech_conditions = $tech_conditions->whereHas('proposition', function ($q) {
            $q->where('status', '>=', 15);
        })->get();

        return view('voyager::projects.archive', ['models' => $tech_conditions]);
    }

    public function agree(TechCondition $tech)
    {
        $tech->proposition->update(['status' => 15]);
    }

    public function back(Request $request, TechCondition $tech)
    {
        $tech->project->update(['comment' => $request->get('comment')]);
        $tech->proposition->update(['status' => 14]);
    }

    public function cancel(TechCondition $tech)
    {
        $this->service->update_status(['designer_id' => null], $tech);
        $this->service->update_status(['status' => 9], $tech->proposition);
        return redirect()->route('admin.projects.index');
    }


    public function upload(Request $request, TechCondition $tech_condition)
    {
        if ($request->hasFile('project')) {
            $this->service->upload($request, $tech_condition);
        }

        if ($request->has('tech_id')) {
            $proposition = $tech_condition->proposition;
            $data = [
                'general' => (Cache::get('general_settings') ?? []), 'model' => $tech_condition,
                'proposition' => $proposition, 'recommendation' => $proposition->recommendation,
                'region' => $proposition->region->region_name, 'applicable' => $proposition->applicantable,
                'designer' => auth()->user()->firm->leader
            ];

            return view('voyager::projects.template', $data);
        }

        return null;
    }

    /**
     * Display the specified resource.
     *
     * @param TechCondition $tech
     * @return Application
     */


    public function show(TechCondition $tech)
    {
        if (user_role('engineer') and $tech->proposition->status < 13) {
            $tech->proposition->update(['status' => 13]);
        }
        return redirect('storage/projects/' . $tech->project->path);
    }
}
