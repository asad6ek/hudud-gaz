<?php

namespace App\Http\Controllers;

use App\Models\Individual;
use App\Models\LegalEntity;
use App\Models\Proposition;
use App\Http\Requests\PropositionRequest;
use App\Services\CRUD\PropositionService;
use App\ViewModels\PropositionListViewModel;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use TCG\Voyager\Facades\Voyager;

class PropositionController extends Controller {

    /**
     * @var PropositionService
     */
    private $service;

    public function __construct(PropositionService $propositionService)
    {
        $this->service = $propositionService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|RedirectResponse|Response|Redirector
     */
    public function index()
    {
        try {
            $this->authorize('browse_propositions');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }
        return view('voyager::propositions.index',
            new PropositionListViewModel($this->service, $this->guard()->user()));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|RedirectResponse|Response|Redirector
     */
    public function create()
    {
        try {
            $this->authorize('add_propositions');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }
        $model = new Proposition();
        return view('voyager::propositions.create', ['model' => $model]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PropositionRequest $request
     * @return Application|RedirectResponse|Response|Redirector
     */
    public function store(PropositionRequest $request)
    {
        try {
            $this->authorize('add_propositions');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        $data = $request->validated();
        $this->service->create($data);

        addAlert(__('table.save_success'));
        return redirect()->route('admin.propositions.index');
    }

    /**
     * Display the specified resource.
     *
     * @param Proposition $proposition
     * @return Application|RedirectResponse|Response|Redirector
     */
    public function show(Proposition $proposition)
    {
        if (user_role('region'))
        {
            $proposition->status = 2;
            $proposition->update();
        }

        return redirect('storage/propositions/' . $proposition->path);
    }

    public function show_more($proposition)
    {
        $proposition = Proposition::query()->with('recommendation', 'tech_condition.project', 'tech_condition.montage')->select([
            'number'
        ])->findOrFail($proposition);
        return view('voyager::propositions.show_more', [
            'model' => $proposition
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param $tin
     * @param $type
     * @return Application|Factory|View
     */
    public function show_application($tin, $type) {
        if ($type === 'legal_entity')
            return view('voyager::propositions.show', [
                'models' => LegalEntity::query()->where('legal_entity_tin', $tin)->firstOrFail()->propositions
            ]);
        return view('voyager::propositions.show', [
            'models' => Individual::query()->where('tin', $tin)->firstOrFail()->propositions
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Proposition $proposition
     * @return Application|RedirectResponse|Response|Redirector
     */
    public function edit(Proposition $proposition)
    {
        try {
            $this->authorize('edit_propositions');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        return view('voyager::propositions.update', ['model' => $proposition]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param PropositionRequest $request
     * @param Proposition $proposition
     * @return RedirectResponse
     */
    public function update(PropositionRequest $request, Proposition $proposition) {
        try {
            $this->authorize('edit_propositions');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }
        $data = $request->validated();
        $this->service->update($data, $proposition);

        if (user_role('region'))
            return redirect()->route('admin.recommendations.create', ['proposition' => $proposition->id, 'type' => $request->get('mode')]);

        addAlert(__('table.update_success'));
        return redirect()->route('admin.propositions.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Proposition $proposition
     * @return Application|RedirectResponse|Response|Redirector
     */
    public function destroy(Proposition $proposition)
    {
        try {
            $this->authorize('delete_propositions');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }
        $this->service->delete($proposition->id);
        addAlert(__('table.delete_success'));
        return redirect()->route('admin.propositions.index');
    }

    public function check_tin(Request $request)
    {
        $tin = $request->get('tin');
        $type = $request->get('type');
        if ($type == 'individual') {
            $applicant = Individual::query()->where('tin', $tin)->withCount('propositions')->first();
            if ($applicant == null) return 0;

            return $applicant->propositions_count;
        }
        $applicant = LegalEntity::query()->where('legal_entity_tin', $tin)->withCount('propositions')->first();
        if ($applicant == null) return 0;
        return $applicant->propositions_count;
    }
}
