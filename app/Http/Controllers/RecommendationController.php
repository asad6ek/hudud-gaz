<?php

namespace App\Http\Controllers;

use App\Http\Requests\RecommendationRequest;
use App\Models\Equipment;
use App\Models\Proposition;
use App\Models\Recommendation;
use App\Services\CRUD\RecommendationService;
use App\ViewModels\RecommendationListViewModel;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Validator;

class RecommendationController extends Controller {
    /**
     * @var RecommendationService
     */
    private $service;

    public function __construct(RecommendationService $recommendationService) {
        $this->service = $recommendationService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Application|View
     */
    public function index(Request $request)
    {
        try {
            $this->authorize('browse_recommendations');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }
        return view('voyager::recommendations.index',
            new RecommendationListViewModel($this->service, $request, $this->guard()->user()));
    }

    public function process()
    {
        try {
            $this->authorize('browse_process_recommendations');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        $models = Proposition::query()->with('recommendation')->whereIn('status', [4,5])->get();
        return view('voyager::recommendations.process', ['models' => $models]);
    }

    public function cancelled(Request $request)
    {
        try {
            $this->authorize('browse_cancel_recommendations');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }
        return view('voyager::recommendations.cancelled',
            new RecommendationListViewModel($this->service, $request, $this->guard()->user()));
    }

    public function archive() {
        try {
            $this->authorize('browse_archive_recommendations');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        $models = Proposition::query()->with('recommendation')
            ->where('status', '>', 9)
            ->where('status', '<', 22)->get();
        return view('voyager::recommendations.archive', ['models' => $models]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Proposition $proposition
     * @param $type
     * @return Application|Redirector|RedirectResponse
     */
    public function create(Proposition $proposition, $type)
    {
        try {
            $this->authorize('add_recommendations');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }


        $recommendation = $proposition->recommendation;
        if ($recommendation)
            return redirect()->route('admin.recommendations.edit', ['recommendation' => $recommendation, 'mode' => $type]);

        $equipments = Equipment::query()->get();
        $model = new Recommendation();
        return view('voyager::recommendations.create', compact('proposition', 'type', 'equipments', 'model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param RecommendationRequest $request
     * @return Application|RedirectResponse|Response|Redirector
     */
    public function store(RecommendationRequest $request)
    {
        try {
            $this->authorize('add_recommendations');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        $data = $request->validated();
        addAlert(__('table.save_success'));
        return $this->service->create($data);
    }

    /**
     * Display the specified resource.
     *
     * @param Proposition $proposition
     * @param $path
     * @return Application|Redirector|RedirectResponse
     */
    public function show(Proposition $proposition, $path)
    {
        if (user_role('technic')){
            $this->service->update_status(['status' => 5], $proposition);
        }
        return redirect('storage/recommendations/'.$path);
    }

    public function back(Request $request, Recommendation $recommendation)
    {
        $this->service->update_status(['comment' => $request->get('comment')], $recommendation);
        $this->service->update_status(['status' => 6], $recommendation->proposition);
        return true;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param Recommendation $recommendation
     * @return Application|Redirector|RedirectResponse
     */
    public function edit(Request $request, Recommendation $recommendation)
    {
        try {
            $this->authorize('edit_recommendations');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }
        if (user_role('technic')){
            $this->service->update_status(['status' => 7], $recommendation->proposition);
        }

        $recommendation['length'] = $recommendation['under_len'] ?? $recommendation['above_len'];
        return view('voyager::recommendations.update', ['model' => $recommendation, 'type' => $request->get('mode')]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param RecommendationRequest $request
     * @param Recommendation $recommendation
     * @return Application|Redirector|RedirectResponse
     */
    public function update(RecommendationRequest $request, Recommendation $recommendation)
    {
        try {
            $this->authorize('edit_recommendations');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }
        $data = $request->validated();
        $this->service->update($data, $recommendation);

        addAlert(__('table.update_success'));
        return redirect()->route('admin.recommendations.index');
    }

    public function upload(Request $request, Recommendation $recommend) {
        $request->validate(['recommendation' => 'required|mimes:pdf']);
        try {
            $this->authorize('edit_recommendations');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        $this->service->upload($request, $recommend);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Recommendation $recommendation
     * @return RedirectResponse
     */
    public function destroy(Recommendation $recommendation) {
        try {
            $this->authorize('delete_recommendations');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }
        $this->service->delete($recommendation->id);
        addAlert(__('table.delete_success'));
        return redirect()->route('admin.recommendations.index');
    }
}
