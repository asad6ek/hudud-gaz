<?php

namespace App\Http\Controllers;

use App\Exceptions\InvalidArgumentException;
use App\Http\Requests\RegionRequest;
use App\Models\BaseModel;
use App\Models\Region;
use App\Services\CRUD\RegionService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\RedirectResponse;

class RegionController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    private $service;

    public function __construct(RegionService $service) {
        $this->service = $service;
    }

    public function index() {
        try {
            $this->authorize('browse_statuses');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        $users = Region::all();

        return view("voyager::regions.index", ['model' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create() {
        $model = new Region();
        return view('voyager::regions.create', ['model' => $model, 'regions' => BaseModel::regions()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param RegionRequest $request
     * @return RedirectResponse
     * @throws InvalidArgumentException
     */
    public function store(RegionRequest $request) {
        try {
            $this->authorize('add_users');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        $data = $request->validated();
        $this->service->create($data);

        addAlert(__('table.save_success'));
        return redirect()->route('admin.regions.index');
    }

    /**
     * Display the specified resource.
     *
     * @return void
     */
    public function show() {}

    /**
     * Show the form for editing the specified resource.
     *
     * @return Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function edit(Region $region) {
        try {
            $this->authorize('edit_users');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        return view('voyager::regions.edit', ['model' => $region, 'regions' => BaseModel::regions()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param RegionRequest $request
     * @param \App\Models\Region $region
     * @return Application|\Illuminate\Routing\Redirector|RedirectResponse
     */
    public function update(RegionRequest $request, Region $region) {
        try {
            $this->authorize('edit_users');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        $data = $request->validated();
        $this->service->update($data, $region);

        addAlert(__('table.update_success'));
        return redirect()->route('admin.regions.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return Application|RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id) {
        try {
            $this->authorize('delete_users');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        $this->service->delete($id);
        addAlert(__('table.delete_success'));
        return redirect()->route('admin.regions.index');
    }
}
