<?php

namespace App\Http\Controllers;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Models\License;
use App\Models\Proposition;
use App\Services\CRUD\PropositionService;
use App\ViewModels\ReportListViewModel;

class ReportController extends Controller
{
    /**
     * @var PropositionService
     */
    private $service;


    public function __construct(PropositionService $propositionService)
    {
        $this->service = $propositionService;
    }

    /**
     * Handle the incoming request.
     *
     * @param Request $request
     * @return Application|Factory|View
     */
    public function index(Request $request)
    {
        try {
            $this->authorize("browse_reports");
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        return view('voyager::reports.index', new ReportListViewModel($this->service));
    }

    /**
     * @param $proposition
     * @return Application|Factory|View
     */
    public function show($proposition)
    {
        try {
            $this->authorize("browse_reports");
        } catch (AuthorizationException $e) {
            return redirect('/');
        }
        $proposition = Proposition::query()->with(['applicantable', 'recommendation', 'region',
            'tech_condition' => function($query){
                $query->with('designer', 'montage_firm', 'montage', 'project');
            },
        ])->where('id', $proposition)->firstOrFail();
        if ($proposition->recommendation->under_len != null) {
            $data['key'] = '(' . __('table.rec.label_underline') . ')';
            $data['len'] = $proposition->recommendation->under_len;
        }
        else if($proposition->recommendation->above_len != null){
            $data['key'] = '(' . __('table.rec.label_line') . ')';
            $data['len'] = $proposition->recommendation->above_len;
        }
//        dd($proposition);
        return view('voyager::reports.show', ['model' => $proposition], compact('data'));

    }

    public function upload(Request $request, License $license) {
        $license->update([
            'status' => 2,
            'file' => $this->storeFile($request->file('file'), $license)
        ]);
        $license->proposition->update(['status' => 22]);

        return redirect()->back();
    }

    private function storeFile($file, $model) {
        File::delete('storage/licenses/' . $model->file);
        $filename = time() . '.pdf';
        $file->move('storage/licenses', $filename);
        return $filename;
    }
}
