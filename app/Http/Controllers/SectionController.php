<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use App\Models\Proposition;
use App\Services\CRUD\PropositionService;
use App\Services\CRUD\SectionsService;
use App\ViewModels\DistrictSectionListViewModel;
use App\ViewModels\RegionalSectionListViewModel;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;

class SectionController extends Controller
{
    /**
     * @var PropositionService
     */
    private $service;

    public function __construct(SectionsService $service)
    {
        $this->service = $service;
    }

    public function region()
    {
        try {
            $this->authorize('browse_section_region');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }
        return view('voyager::section.regional', new RegionalSectionListViewModel($this->service));
    }

    public function district(Request $request)
    {
        try {
            $this->authorize('browse_section_district');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }
        return view('voyager::section.district', new DistrictSectionListViewModel($this->service, $request));
    }

    public function more()
    {
        try {
            $this->authorize('browse_section_more');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }
        return view('voyager::section.more', [
            'models' => Proposition::query()->with('getStatus', 'region')->get()
        ]);
    }
}
