<?php


namespace App\Http\Controllers;

use App\Http\Requests\GeneralRequest;
use App\Models\Designer;
use App\Models\MontageFirm;
use App\Models\Region;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class SettingsController extends Controller
{
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function fetch_data($type)
    {
        $result = "";
        $types = null;
        if ($type == 'Туман') {
            $types = Region::query()->pluck('org_name as name', 'id');
        } elseif ($type == 'Лойихачи') {
            $types = Designer::query()->pluck('organization as name', 'id');
        } elseif ($type == 'Монтажчи') {
            $types = MontageFirm::query()->pluck( 'full_name as name', 'id');
        }
        foreach ($types as $key => $type) {
            $result .= "<option value='" . $key . "'>" . $type . "</option>";
        }
        return response()->json(['types' => $result]);
    }

    public function index()
    {
        try {
            $this->authorize('browse_statuses');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        $data = Cache::get('general_settings') ?? [];
        return view("voyager::admin.generalSettings", ['data' => $data]);
    }

    public function update(GeneralRequest $request)
    {
        try {
            $this->authorize('edit_users');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        $data = $request->validated();
        if (isset($data['logo']))
            $data['logo'] = "/storage/temp/" . $this->createPath();

        Cache::put('general_settings', $data);
        addAlert(__('table.update_success'));
        return redirect()->route('admin.general_settings.index');
    }

//
    private function createPath(): string
    {
        $fileName = /*time() . '.' . */
            "rasm." . $this->request->file('logo')->extension();
        $this->request->file('logo')->move(public_path('../storage/app/public/temp'), $fileName);
        return $fileName;
    }
}
