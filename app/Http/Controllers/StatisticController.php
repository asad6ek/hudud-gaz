<?php

namespace App\Http\Controllers;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Storage;
use App\Models\BaseModel;
use App\Models\License;
use App\Services\CRUD\StatisticService;
use App\ViewModels\StatisticListViewModel;

class StatisticController extends Controller
{
    /**
     * @var StatisticService
     */
    private $service;

    public function __construct(StatisticService $service)
    {
        $this->service = $service;
    }

    public function index() {

//        dd(Proposition::where('status', 10)->get()->groupBy(function($val) {
//                return Carbon::parse($val->created_at)->format('Y');
//        }));

        return view('voyager::statistics.index', new StatisticListViewModel($this->service));
    }

    public function licenses() {
        try {
            $this->authorize('browse_licenses');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        $regions = BaseModel::regions();
        return view('voyager::statistics.licenses', ['models' => License::query()->get(), 'regions' => $regions]);
    }

    public function show(License $license) {
        return redirect(Storage::url('public/licenses/' . $license->getAttribute('file')));
    }
}
