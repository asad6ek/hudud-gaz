<?php

namespace App\Http\Controllers;

use App\Models\Status;
use App\Services\CRUD\StatusService;
use App\ViewModels\StatusListViewModel;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;

class StatusController extends Controller
{

    /**
     * @var StatusService
     */
    private $service;

    public function __construct(StatusService $propositionService)
    {
        $this->service = $propositionService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return View|RedirectResponse
     */
    public function index(Request $request)
    {
        try {
            $this->authorize('browse_statuses');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }
        return view('voyager::statuses.index',
            new StatusListViewModel($this->service, $request));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Status $status
     * @return View|RedirectResponse
     */
    public function edit(Status $status)
    {
        try {
            $this->authorize('read_statuses');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        return view('voyager::statuses.edit', ['status' => $status]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Status $status
     * @return Application|Redirector|RedirectResponse
     */
    public function update(Request $request, Status $status)
    {
        unset($request['_token']);
        unset($request['_method']);
        try {
            $this->authorize('edit_statuses');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }
        $data = $request->all();
        $data['minutes'] = $data['minutes'] * 60;
        $this->service->update($data, $status);

        addAlert(__('table.update_success'));
        return redirect()->route('admin.statuses.index');
    }
}
