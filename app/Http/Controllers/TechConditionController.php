<?php

namespace App\Http\Controllers;

use App\Http\Requests\TechConditionRequest;
use App\Models\Equipment;
use App\Models\Recommendation;
use App\Models\TechCondition;
use App\Services\CRUD\EmployeesService;
use App\Services\CRUD\TechConditionService;
use App\ViewModels\EmployeesListViewModel;
use App\ViewModels\TechConditionListViewModel;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use PHPUnit\Exception;

class TechConditionController extends Controller
{
    private $service;

    public function __construct(TechConditionService $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Application|RedirectResponse|Redirector
     */
    public function index(Request $request)
    {
        try {
            $this->authorize('browse_tech_conditions');
        }
        catch (AuthorizationException $e)
        {
            return redirect('/');
        }

        return view('voyager::tech_conditions.index',
            new TechConditionListViewModel($this->service, $request,  $this->guard()->user()));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param $recommendation
     * @return Application
     */
    public function create($recommendation)
    {
        $recommendation = Recommendation::with('equipments')->findOrFail($recommendation);
//        if (user_role('technic')){
//            $this->service->update_status(['status' => 7], $recommendation->proposition);
//        }
//        dd($recommendation->applicant);
        $equipments = $recommendation->getEquipments();
        return view('voyager::tech_conditions.create', ['recommendation' => $recommendation,
            'equipments' => $equipments]); //compact('recommendation')
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param TechConditionRequest $request
     * @return Application|RedirectResponse|Response|Redirector
     */
    public function store(TechConditionRequest $request)
    {
        try {
            $this->authorize('add_tech_conditions');
        }
        catch (AuthorizationException $e)
        {
            return redirect('/');
        }
        $data = $request->validated();
        addAlert(__('table.save_success'));
        return $this->service->create($data);
    }

    public function upload(Request $request, TechCondition $tech_condition)
    {
        $this->service->upload_pdf($request, $tech_condition);
        addAlert(__('table.alerts.upl_success'));
        return 1;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
