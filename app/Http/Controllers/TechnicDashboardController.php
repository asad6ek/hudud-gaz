<?php


namespace App\Http\Controllers;


use App\Models\Proposition;
use App\Models\Region;
use App\Models\Status;
use Illuminate\Auth\Access\AuthorizationException;

class TechnicDashboardController extends Controller
{


    public function index()
    {
        try {
            $this->authorize('browse_technic_index');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }
        $models = Region::query()->withCount(['propositions as prop_count' => function ($query) {
            $query->whereNotIn('status', [Status::$CANCELLED_STATUS, Status::$DONE_STATUS]);
        }
        ])->orderBy('prop_count','desc')->get();

        return view('voyager::technic.index', ['models' => $models]);
    }

    public function  show(Region $region){

        try {
            $this->authorize('browse_technic_index');
        } catch (AuthorizationException $e) {
            return redirect('/');
        }
        $models = Proposition::query()->whereNotIn('status', [Status::$CANCELLED_STATUS, Status::$DONE_STATUS])->where('region_id',$region->id)->with('getStatus', 'region')->get();

        return view('voyager::propositions.index', ['models' => $models]);
    }
}
