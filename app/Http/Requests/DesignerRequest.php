<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DesignerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        if ($this->request->get('date_reg') >= $this->request->get('date_end'))
            return [
                'date_reg' => ['email']
            ];

        return [
            'organization' => ['required'],
            'leader' => ['required'],
            'address' => ['required'],
            'phone_number' => ['required'],
            'date_reg' => ['required'],
            'date_end' => ['required'],
            'document' => []
        ];
    }

    public function attributes() {
        return [
            'organization' => __('table.regions.org_name'),
            'leader' => __('table.montage.reg_leader'),
            'address' => __('table.montage.address'),
            'phone_number' => __('table.designers.phone'),
            'date_reg' => __('table.montage.date_created'),
            'date_end' => __('table.montage.date_expired')
        ];
    }

    public function messages() {
        return [
            'date_reg.email' => __('table.montage.input_date_error')
        ];
    }
}
