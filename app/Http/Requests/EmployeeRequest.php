<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class EmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'admin.employees.store' =>
                'required|min:5',
            'admin.employees.update' => []
        ];

        if (($this->request->get('role_id') == 6 or
                $this->request->get('role_id') == 7 or
                $this->request->get('role_id') == 8) and $this->request->get('firm_id') == null)
            return [
                'firm_id' => ['required']
            ];

        return [
            'name' => ['required',],
            'password' => $rules[$this->route()->getName()],
            'email' => [
                'email',
                'required',
                Rule::unique('users', 'email')
                    ->ignore($this->route('employee'))
            ],
//            'avatar' => ['required',],
            'lastname' => [],
            'patronymic' => [],
            'position' => [],
            'role_id' => [],
            'firm_id' => [],
            'locale' => [],
        ];
    }

    public function attributes()
    {
        return [
            'name' => __('table.employees.firstname'),
            'password' => __('table.employees.password'),
            'email' => __('table.employees.email'),
        ];
    }

    public function messages()
    {
        return [
            'password.min' => __('validation.min_symbol'),
            'region_id.required' => __('table.employees.not_org_error')
        ];
    }
}
