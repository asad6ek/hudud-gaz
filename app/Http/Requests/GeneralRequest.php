<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GeneralRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'shareholder_name' => ['required'],
            'branch_name' => ['required'],
            'engineer_name' => ['required'],
            'helper_name' => ['required'],
            'tech_section' => ['required'],
            'metrology' => ['required'],
            'legal_section' => ['required'],
            'exp_section' => ['required'],
            'phone_number' => ['required'],
            'reg_num' => ['required'],
            'address_latin' => ['required'],
            'address' => ['required'],
            'email_address' => ['required'],
            'fax' => ['required'],
        ];
    }
}
