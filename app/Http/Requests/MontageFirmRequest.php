<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MontageFirmRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    public function attributes() {
        return [
            'record_number' => __('table.montage.record_number'),
            'reg_number' => __('table.montage.reg_number'),
            'full_name' => __('table.montage.full_name'),
            'shortname' => __('table.montage.short_name'),
            'leader' => __('table.montage.reg_leader'),
            'region' => __('table.montage.region_name'),
            'address' => __('table.montage.address'),
            'taxpayer_stir' => __('table.montage.taxpayer_stir'),
            'legal_form' => __('table.montage.legal_form'),
            'given_by' => __('table.montage.given_by'),
            'date_created' => __('table.montage.date_created'),
            'date_expired' => __('table.montage.date_expired'),
            'permission_to' => __('table.montage.permissions_to'),
            'implement_for' => __('table.montage.implements_for'),
            'path'=> []
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array {
        if ($this->request->get('date_created') >= $this->request->get('date_expired'))
            return [
                'date_created' => ['email']
            ];

        return [
            'record_number' => ['required'],
            'reg_number' => ['required'],
            'full_name' => ['required'],
            'shortname' => ['required'],
            'leader' => ['required'],
            'region' => ['required'],
            'address' => ['required'],
            'taxpayer_stir' => ['required'],
            'legal_form' => ['required'],
            'given_by' => ['required'],
            'date_created' => ['required'],
            'date_expired' => ['required'],
            'permission_to' => ['required'],
            'implement_for' => ['required'],
            'path'=> []
        ];
    }

    public function messages(): array {
        return [
            'date_created.email' => __('table.montage.input_date_error')
        ];
    }
}
