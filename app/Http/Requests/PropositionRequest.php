<?php


namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PropositionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if (user_role('region'))
            return $this->isRegion();
        return $this->isTechnic();
    }

    public function isTechnic()
    {
        $rules = [
            'admin.propositions.store' => 'required',
            'admin.propositions.update' => []
        ];
        return [
            'number' => [
                'required',
                Rule::unique('propositions', 'number')
                    ->ignore($this->route('proposition'))
                    ->whereNull('deleted_at')
            ],
            'region_id' => 'required',
            'path' => $rules[$this->route()->getName()],
        ];
    }

    public function isRegion()
    {
        $rules = [
            0 => ['type' => ['required']],
            1 => $this->individual(),
            2 => $this->legalEntity()
        ];

        return isset($rules[$this->input('type')]) ? $rules[$this->input('type')] : $rules[0];
    }

    public function attributes()
    {
        return [
            'number' => __('table.propositions.prop_number'),
            'tin' => __('table.propositions.stir'),
            'full_name' => __('table.propositions.full_name'),
            'path' => __('table.propositions.file_path'),
            'region_id' => __('table.propositions.district-city'),
            'legal_entity_tin' => __('table.propositions.legal_entity_tin'),
            'legal_entity_name' => __('table.propositions.legal_entity_name'),
            'leader_name' => __('table.propositions.leader_name'),
            'leader_tin' => __('table.propositions.leader_tin'),
            'tel_number' => __('table.propositions.tel_number'),
            'pass_id' => __('table.propositions.pass_id'),
            'email' => __('table.propositions.email')
        ];
    }


    private function individual(): array
    {
        return [
            'tin' => 'required',
            'type' => 'required',
            'building_type' => 'required',
            'full_name' => 'required',
            'tel_number' => 'required',
            'pass_id' => 'required',
            'activity_type' => 'required'
        ];
    }


    private function legalEntity(): array
    {

        return [
            'building_type' => 'required',
            'legal_entity_tin' => 'required', //'required|numeric|min:7'
            'legal_entity_name' => 'required',
            'email' => 'required',
            'leader_name' => 'required',
            'leader_tin' => 'required',
            'tel_number' => 'required',
            'activity_type' => 'required',
            'type' => ['required']
        ];
    }
}
