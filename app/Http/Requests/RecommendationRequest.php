<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RecommendationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->input('type') == 'success' ? $this->success() : $this->fail();

    }

    public function check()
    {
        if ($this->routes[$this->route()->getName()])
            return ['required', Rule::unique('recommendations', 'prop_id')];
        else
            return ['required'];
    }

    public $routes = [
        'admin.recommendations.store' => true,
        'admin.recommendations.update' => false
    ];

    public function attributes(): array {
        return [
            'description' => __('table.rec.cancel_description'),
            'additional' => __('table.rec.cancel_information'),
            'prop_id' => __('table.rec.prop_id'),
        ];
    }


    public function success()
    {

        return [
            'prop_id' => $this->check(),
            'address' => ['required'],
            'access_point' => ['required'],
            'length_type' => ['required'],
            'length' => ['required', 'numeric'],
            'pipe_size' => ['required'],
            'pipe_type' => ['required'],
            'gas_network' => ['required'],
            'depth' => ['required', 'numeric'],
            'capacity' => ['required'],
            'real_capacity' => ['required'],
            'grc' => ['required'],
            'avg_winter' => ['required'],
            'avg_summer' => ['required'],
            'gas_consumption' => ['required', 'numeric'],
            'additional' => [],
            'additional_test' => [],
            'type' => [],
            'gas_meter' => [],
            'equipments' => []
        ];
    }

    public function fail()
    {
        return [
            'prop_id' => $this->check(),
            'type' => [],
            'rec_date' => [],
            'description' => ['required'],
            'additional' => ['required']
        ];
    }
}
