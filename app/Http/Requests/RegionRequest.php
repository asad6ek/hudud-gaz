<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RegionRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'org_number' => ['required',
                Rule::unique('regions', 'org_number') //['required','unique:regions,org_number']
                    ->ignore($this->route('region'))
            ],
            'org_name' => ['required'],
            'region_name' => ['required'],
            'lead_engineer' => ['required'],
            'section_leader' => ['required'],
            'email' => ['required', 'email',
                Rule::unique('regions', 'email')
                ->ignore($this->route('region'))],
            'phone' => ['required'],
            'address_latin' => ['required'],
            'address' => ['required'],
            'fax' => ['required'],
        ];
    }

    public function attributes() {
        return [
            'org_number' => __('table.regions.org_number'),
            'org_name' => __('table.regions.org_name'),
            'region_name' => __('table.regions.org_region_name'),
            'lead_engineer' => __('table.regions.col_org_leader'),
            'section_leader' => __('table.regions.section_leader'),
            'email' => __('table.regions.email'),
            'email.unique' => "Бундай почта мавжуд",
            'phone' => __('table.regions.phone'),
            'address_latin' => __('table.regions.address_latin'),
            'address' => __('table.regions.address'),
            'fax' => __('table.regions.fax')
        ];
    }
}
