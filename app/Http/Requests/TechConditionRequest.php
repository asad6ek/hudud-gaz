<?php


namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class TechConditionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function attributes() {
        return [
            'prop_id' => __('table.tech.for_prop'),
            'additional' => __('table.rec.cancel_information')
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'prop_id' => ['required', Rule::unique('tech_conditions', 'prop_id')],
            'type' => ['required'],
            'content' => $this->input('type') ? ['required'] : [],
            'additional' => ['required']
        ];
    }

}
