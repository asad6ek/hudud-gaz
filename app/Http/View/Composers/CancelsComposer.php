<?php


namespace App\Http\View\Composers;


use App\Services\CRUD\PropositionService;
use Illuminate\View\View;

class CancelsComposer
{
    private $service;

    public function __construct(PropositionService $service)
    {
        $this->service = $service;
    }

    public function compose(View $view)
    {
        if (user_role('region')) {
            $view->with('badge_names', ['propositions', 'recommendations', 'progress', 'cancelled', 'archive']);
            $view->with('propositions', $this->service->received_proposition());
            $view->with('recommendations', $this->service->created_recommendations());
            $view->with('progress', $this->service->progress_recommendations());
            $view->with('cancelled', $this->service->cancelled_recommendations());
            $view->with('archive', $this->service->archive_recommendations());
        }

        if (user_role('designer')) {
            $view->with('badge_names', ['projects', 'progress', 'cancelled', 'archive']);
            $view->with('projects', $this->service->created_projects());
            $view->with('progress', $this->service->progress_projects());
            $view->with('cancelled', $this->service->cancelled_projects());
            $view->with('archive', $this->service->archive_projects());
        }

        if (user_role('montage_firm')) {
            $view->with('badge_names', ['montages', 'progress', 'cancelled', 'archive']);
            $view->with('montages', $this->service->created_montages());
            $view->with('progress', $this->service->progress_montages());
            $view->with('cancelled', $this->service->cancelled_montages());
            $view->with('archive', $this->service->archive_montages());
        }
    }
}
