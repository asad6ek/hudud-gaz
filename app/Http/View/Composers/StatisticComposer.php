<?php


namespace App\Http\View\Composers;


use App\Services\CRUD\PropositionService;
use Illuminate\View\View;

class StatisticComposer
{

    private $service;

    public function __construct(PropositionService $service)
    {
        $this->service = $service;
    }


    public function compose(View $view)
    {
        $view->with('in_today', $this->service->inToday());
        $view->with('in_progress', $this->service->inProgress());
        $view->with('in_late', $this->service->inLate());
        $view->with('in_year', $this->service->inYear());
    }
}
