<?php

namespace App\Interfaces;


use Illuminate\Http\Request;

interface ICrudService
{
    public function list($data = []);

    public function create($data);

    public function update($data, $model);

    public function delete($id);

    public function find($id);
}
