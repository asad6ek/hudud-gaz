<?php

namespace App\Models;


use App\View\Interfaces\IFormInputable;
use App\View\Traits\FormInputAttributeLabelTrait;
use Carbon\Carbon;
use DateTime;
use Illuminate\Database\Eloquent\Model;
use App\QueryFilter\Traits\FilterableTrait;
use Illuminate\Support\Arr;

/**
 * App\Models\BaseModel
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BaseModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BaseModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BaseModel query()
 */
abstract class BaseModel extends Model implements IFormInputable
{
    use FormInputAttributeLabelTrait;
    use FilterableTrait;


    public function created_at()
    {
        return $this->created_at->format('d.m.Y H:i');
    }


    public static function localeName()
    {
        return [
            'uzk' => 'Ўзбекча',
            'uzl' => 'O\'zbekcha',
            'ru' => 'Русский',
        ];
    }

    public static function getSuccessStatus()
    {

        return [21, 22];
    }

    public static function getStatusName($key)
    {
        $status = Status::query()->first($key);
        return $status->name;
    }

    public static function getRejectedStatus()
    {

        return [10];
    }

    public static function regions(): array
    {
        return [
            1 => __('table.region.urgench_city'),
            2 => __('table.region.urgench_district'),
            3 => __('table.region.khiva_city'),
            4 => __('table.region.khiva_district'),
            5 => __('table.region.khonqa'),
            6 => __('table.region.bagat'),
            7 => __('table.region.yangiariq'),
            8 => __('table.region.yangibozor'),
            9 => __('table.region.khazorasp'),
            10 => __('table.region.shavat'),
            11 => __('table.region.gurlan'),
            12 => __('table.region.tuproqqala'),
            13 => __('table.region.kushkupir'),
        ];
    }

    public function month($n)
    {
        $arr = [
            'январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь'
        ];
        return $arr[$n - 1];
    }


    public function percentage($startDate, $endDate)
    {
        $start = new DateTime($startDate);
        $days = $start->diff((new DateTime($endDate)), true)->days;
        $sundays = intval($days / 7) + ($start->format('N') + $days % 7 >= 7);
        $workDays = round((strtotime($endDate) - strtotime($startDate)) / (24 * 60 * 60), 2) - $sundays;
        if (3 - $workDays <= 0) $percent = 101;
        else $percent = round((3 - $workDays) / 3, 2) * 100;
        return $percent;
    }

    public function percent($proposition)
    {
        $general = $proposition->getStatus->minutes;
        if ($proposition->delay_s == 1) $data['percent'] = 101;
        elseif (isset($general) and $general != 0) $data['percent'] = round($proposition['minutes'] * 100 / $general);
        else {
            $data['percent'] = 100;
            $data['text'] = '-';
            return $data;
        }
        $data['time'] = Carbon::create()->minutes($proposition['minutes']);
        $data['day'] = Carbon::create()->diffInDays($data['time']);
        $data['text'] = $this->timeText($data['day'], $data['time']);
        return $data;
    }

    public function timeText($day, $time)
    {
        $text = '';
        if ($day > 0) $text .= $day . ' ' . __('table.day') . ' ';
        if ($time->hour > 0) $text .= $time->hour . ' ' . __('table.hours') . ' ';
        if ($time->minute > 0) $text .= $time->minute . ' ' . __('table.minute');
        return $text;
    }

    public function colorForPercent($percent)
    {
        if ($percent == 0 || $percent == 101) $color = '255, 0, 0';
        elseif ($percent > 0 && $percent <= 25) $color = '255, 125, 0';
        elseif ($percent > 25 && $percent <= 50) $color = '255, 255, 0';
        elseif ($percent > 50 && $percent <= 75) $color = '130, 255, 0';
        else $color = '0, 255, 0';

        return $color;
    }

}
