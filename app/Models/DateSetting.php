<?php


namespace App\Models;

/**
 * Class DateSetting
 * @package App\Models
 * @property int    $id
 * @property int    $type
 * @property string $from
 * @property string $to
 */
class DateSetting extends BaseModel
{
    public const HOLIDAYS = 1;
    public const WORKING_DAYS = 2;

    protected $fillable = ['name', 'from', 'to', 'type'];

    public function getTypes(): array
    {
        return [
            self::HOLIDAYS => __('table.date_settings.holidays'),
            self::WORKING_DAYS => __('table.date_settings.working_days')
        ];
    }

}
