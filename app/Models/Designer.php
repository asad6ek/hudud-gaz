<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


/**
 * @property mixed organization
 */

class Designer extends Model {

    use HasFactory;
    protected $fillable = ['organization', 'leader', 'address', 'phone_number','date_reg', 'date_end', 'document'];

    public function users()
    {
        return $this->morphMany(User::class, 'firm');
    }

    public function getNameAttribute()
    {
        return $this->organization;
    }
}
