<?php

namespace App\Models;


use App\Filters\EquipmentFilter;
use App\QueryFilter\Traits\FilterableTrait;
use App\View\Traits\FormInputAttributeLabelTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;
use TCG\Voyager\Traits\VoyagerUser;
/**
 * Class Proposition
 * @package App\Models
 * @property int $id
 * @property  string $name
 * @property  $children
 */
class Equipment extends BaseModel
{
    use HasFactory,Notifiable;


    use FormInputAttributeLabelTrait;
    use FilterableTrait;
    use VoyagerUser;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    protected $table = 'equipments';

    protected $queryFilterClass = EquipmentFilter::class;


    public function children()
    {
        return $this->hasMany(EquipmentType::class);
    }


}
