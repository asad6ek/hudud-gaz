<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class EquipmentType extends BaseModel
{
    use HasFactory;

    protected $fillable=['equipment_id','type', 'order'];

    protected $table = 'equipments_types';

    public function equipment()
    {
        return $this->belongsTo(Equipment::class);
    }
}
