<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class EventStatus
 * @package App\Models
 * @property int $prop_id
 * @property string $delay_minutes
 * @property int $status
 * @mixin \Eloquent
 * @property-read User $user_id
 * @property-read Proposition $proposition
 *
 */
class EventStatus extends Model
{
    use HasFactory;

    protected $fillable = ['prop_id', 'user_id','status','delay_minutes'];
    protected $table = 'event_status';

}
