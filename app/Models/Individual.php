<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 *  App\Models\Individual
 * @property $full_name
 * @property $tel_number
 * @property $pass_id
 * @property $tin
 * @property  int $id
 *
 * @property-read Proposition $propositions
 *
 */
class Individual extends Model
{
    use HasFactory;

    protected $fillable = ['tin', 'full_name', 'tel_number', 'pass_id', 'tin'];

    public function propositions()
    {
        return $this->morphMany(Proposition::class, 'applicantable');
    }

    public function getNameAttribute()
    {
        return $this->full_name;
    }
}
