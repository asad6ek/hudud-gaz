<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class LegalEntity
 * @property $legal_entity_tin
 * @property $legal_entity_name
 * @property $email
 * @property $leader_name
 * @property $leader_tin
 * @property $tel_number
 *
 * @mixin \Eloquent
 * @property-read Proposition $propositions
 *
 */
class LegalEntity extends Model
{
    use HasFactory;

    protected $fillable = [
        'legal_entity_tin',
        'legal_entity_name',
        'email',
        'leader_name',
        'leader_tin',
        'tel_number',
        'created_at'
    ];

    public function propositions()
    {
        return $this->morphMany(Proposition::class, 'applicantable');
    }

    public function getNameAttribute()
    {
        return $this->legal_entity_name;
    }

    public function getTinAttribute()
    {
        return $this->legal_entity_tin;
    }
}
