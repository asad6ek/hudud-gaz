<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property Proposition proposition
 */
class License extends Model {
    use HasFactory;

    protected $fillable = ['prop_id', 'file', 'district', 'status'];

    public function proposition() {
        return $this->belongsTo(Proposition::class, 'prop_id');
    }
}
