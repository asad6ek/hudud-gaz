<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Montage
 * @package App\Models
 * @property  string $path
 * @property int $user_id
 * @property int $tech_id
 */
class Montage extends Model
{
    protected $fillable = ['path', 'tech_id','comment'];
    use HasFactory;
}
