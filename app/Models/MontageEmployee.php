<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MontageEmployee extends Model {
    use HasFactory;

    protected $fillable = ['firm_id', 'statement_number', 'first_name', 'second_name', 'last_name', 'date_contract',
        'date_contract_end', 'diploma_number', 'passport_series', 'specialization', 'function', 'experience', 'path'];
//
//    public function montage_firm() {
//        return $this->belongsTo(MontageFirm::class, 'firm_id');
//    }
}
