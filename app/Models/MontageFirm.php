<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property mixed short_name
 */
class MontageFirm extends BaseModel
{
    protected $fillable = ['record_number', 'reg_number', 'full_name', 'shortname', 'leader',
        'region', 'address', 'taxpayer_stir', 'legal_form', 'given_by',
        'date_created', 'date_expired', 'permission_to', 'implement_for', 'path'];
    use HasFactory;

    /**
     * @return HasMany
     */
    public function getEmployees(): HasMany {
        return $this->hasMany(MontageEmployee::class, 'firm_id');
    }

    public function users()
    {
        return $this->morphMany(User::class, 'firm');
    }

    public function district($region = false): string {
        if ($region)
            return $this->getAttribute('region');
        return BaseModel::regions()[$this->getAttribute('region')];
    }

    public function getNameAttribute()
    {
        return $this->shortname;
    }
}
