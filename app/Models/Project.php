<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends BaseModel
{
    use HasFactory;

    protected $fillable = ['designer_id','tech_id','path','comment'];

    public function designer(){
        return $this->belongsTo(User::class,'id','designer_id');
    }
    public function tech_condition(){
        return $this->belongsTo(TechCondition::class,'tech_id');
    }

}
