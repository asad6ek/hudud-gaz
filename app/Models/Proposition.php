<?php

namespace App\Models;

use App\Filters\PropositionFilter;
use App\Traits\ModelListTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Arr;

/**
 * Class Proposition
 * @package App\Models
 * @property int $id
 * @property  int $number
 * @property  string $path
 * @property  int $region_id
 * @property  int $type
 * @property  $status
 * @property  $region
 * @property  $activity_type
 * @function Recommendation $recommendation
 * @property-read  $applicantable
 * @property-read Status $getStatus
 * @property int $minutes
 * @property int delay_s
 * @property mixed recommendation
 */
class Proposition extends BaseModel
{

    use SoftDeletes, ModelListTrait, HasFactory;

    protected $fillable = ['id', 'customer_id', 'number', 'path', 'region_id', 'building_type', 'status', 'delay_s', 'activity_type', 'applicantable_type', 'applicantable_id', 'minutes'];

    protected $queryFilterClass = PropositionFilter::class;

    public static function activityTypes()
    {
        return Activity::query()->where('id', '>', 1)->pluck('activity_name', 'id');
    }

    public function getBuildingTypeAttribute($value)
    {

        $building_type = [
            1 => __('table.propositions.residential_building'),
            2 => __('table.propositions.non_residential_building'),
        ];
        return $building_type[$value];
    }

    public function getRegions()
    {
        return Region::query()->pluck('org_name', 'id');
    }

    public function getRegion()
    {
        return Arr::get(self::regions(), $this->region_id);
    }

    public static function getRegionKey($key)
    {
        return Arr::get(self::regions(), $key);
    }

    public function applicantable()
    {
        return $this->morphTo();
    }

    public function getStatus()
    {
        return $this->hasOne(Status::class, 'id', 'status');
    }


    public function isLegalEntity(): bool
    {
        return !is_null($this->applicantable_type) && isset($this->applicantable->legal_entity_tin);
    }

    public function activityName()
    {
        return $this->belongsTo(Activity::class, 'activity_type');
    }

    public function getPercentColorAttribute()
    {
        $startDate = $this->created_at();
        $endDate = now()->toDateTimeString();
        return $this->percentage($startDate, $endDate);
    }

    public function getColorAttribute()
    {
        return $this->colorForPercent($this->getPercentColorAttribute());
    }

    public function timeDifference()
    {
        $start = Carbon::create($this->created_at())->addHours(2);
        $end = Carbon::now();

        return $start < $end ? $start->diff($end)->format('%H:%I:%S') : null;
    }

    public function recommendation()
    {
        return $this->hasOne(Recommendation::class, 'prop_id');
    }

    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    public function tech_condition()
    {
        return $this->hasOne(TechCondition::class, 'prop_id');
    }

    public function eventStatus()
    {
        return $this->hasMany(EventStatus::class, 'prop_id');
    }

    public function equipments()
    {
        return $this->hasMany(TechEquipment::class, 'prop_id');
    }
}
