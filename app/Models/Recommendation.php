<?php

namespace App\Models;

use DateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

/**
 * Class Proposition
 * @package App\Models
 * @property int $id
 * @property  int $prop_id
 * @property  string $path
 * @property  int $region_id
 * @property  int $type
 * @property  $created_at
 * @property  Proposition $proposition
 * @property Status $status
 */

class Recommendation extends BaseModel
{
    use HasFactory;

    protected $additional = "9. Tabiiy gaz ishlatish maqsadi. \n 10. Kelishuv shartlari";
//    protected $casts = ['properties' => 'array'];

    protected $fillable = ['prop_id', 'path', 'address', 'gas_network', 'pipe_type', 'access_point', 'under_len', 'above_len', 'pipe_size', 'pipe_size2', 'depth', 'grc',
        'avg_winter', 'avg_summer', 'capacity', 'real_capacity', 'gas_consumption', 'description', 'additional', 'comment', 'gas_meter', 'status', 'equipments'];

    public function getRegionAttribute()
    {
        return $this->proposition->getRegion();
    }

    public function proposition()
    {
        return $this->belongsTo(Proposition::class, 'prop_id');
    }

    public function getApplicantAttribute()
    {
        return $this->proposition->applicantable;
    }

    public function getPercentColorAttribute()
    {
        $startDate = $this->created_at;
        $endDate = now()->toDateTimeString();

        return $this->percentage($startDate, $endDate);
    }

    public function getColorAttribute()
    {
        return $this->colorForPercent($this->getPercentColorAttribute());
    }

    public static function getNextId()
    {
        $statement = DB::select("show table status like 'recommendations'");
        return $statement[0]->Auto_increment;
    }

    public function equipments()
    {
        return $this->belongsToMany(EquipmentType::class, 'tech_equipments', 'prop_id', 'type_id')
            ->withPivot('note');
//        return $this->proposition->equipments;
    }

    public function getEquipments() {
        $equipments = json_decode($this->getAttribute('equipments'));
        if (!$equipments)
            return [];
        foreach ($equipments as $equipment) {
            $equipment->equipment = Equipment::query()->find($equipment->equipment)->getAttribute('name');
            $equipment->type = EquipmentType::query()->find($equipment->type)->getAttribute('type');
        }

        return $equipments;
    }

    public function getGasMeters() {
        $elements = json_decode($this->getAttribute('gas_meter'));
        if (!$elements)
            return [];

        foreach ($elements as $element) {
            $element->type = EquipmentType::query()->find($element->type)->getAttribute('type');
        }

        return $elements;
    }
}
