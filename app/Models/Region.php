<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Arr;

/**
 * @property int $id
 * @property mixed $org_name
 * @property mixed $region_name
 */

class Region extends BaseModel {
    use HasFactory;
    use Notifiable;

    protected $fillable = ['org_number', 'lead_engineer', 'section_leader', 'region_name', 'org_name', 'address_latin', 'address', 'email', 'phone', 'fax'];

    public function getRegionNameAttribute($value)
    {
        return Arr::get(self::regions(), $value);
    }

    public function users()
    {
        return $this->morphMany(User::class, 'firm');
    }

    public function getNameAttribute()
    {
        return $this->org_name;
    }

    public function propositions()
    {
        return $this->hasMany(Proposition::class, 'region_id');
    }

    public function getRegion()
    {
        return Arr::get(self::regions(), $this->region_name);
    }
}
