<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Status
 * @property int $minutes
 * @package App\Models
 */
class Status extends BaseModel
{
    use HasFactory;

    public static $CANCELLED_STATUS = 10;
    public static $DONE_STATUS = 22;

    protected $guarded = [];
}
