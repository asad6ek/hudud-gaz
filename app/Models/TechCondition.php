<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

/**
 * Class Proposition
 * @package App\Models
 * @property int $id
 * @property  int $prop_id
 * @property  int $designer_id
 * @property  int $firm_id
 * @property  string $path
 * @property  string $qrcode
 * @property  mixed $designer
 * @property-read Montage $montage
 * @property-read Project $project
 * @property-read Proposition $proposition
 * @property-read Status $status
 */
class TechCondition extends BaseModel
{
    use HasFactory, SoftDeletes;

    protected $fillable = ['prop_id', 'path', 'qrcode', 'designer_id', 'firm_id'];

    public function proposition(): BelongsTo
    {
        return $this->belongsTo(Proposition::class, 'prop_id');
    }

    public function project(): HasOne
    {
        return $this->hasOne(Project::class, 'tech_id', 'id');
    }

    public function montage_firm(): HasOne
    {
        return $this->hasOne(MontageFirm::class, 'id', 'firm_id');
    }

    public function montage(): HasOne
    {
        return $this->hasOne(Montage::class, 'tech_id', 'id');
    }

    public function designer(): BelongsTo
    {
        return $this->belongsTo(Designer::class);
    }

    public function scopeDesigner($query, $designer_id)
    {
        return $query->where('designer_id', $designer_id);
    }

    public function scopeMontageFirm($query) {
        return $query->where('firm_id', auth()->user()->firm_id);
    }

    public function scopeHasPropositions($query, $designer_id)
    {
        $query->whereHas('proposition', function ($q) {
            $q->where('status', 11);
        });
    }

    public static function getNextId()
    {
        $statement = DB::select("show table status like 'tech_conditions'");
        return $statement[0]->Auto_increment;
    }
}
