<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class TechEquipment extends BaseModel
{
    use HasFactory;
    protected $fillable = ['prop_id', 'type_id', 'number', 'note'];
    protected $table = 'tech_equipments';

    public function type(): BelongsTo {
        return $this->belongsTo(EquipmentType::class, 'type_id');
    }
}
