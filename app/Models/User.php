<?php

namespace App\Models;

use TCG\Voyager\Models\Role;
use App\Filters\EmployeesFilter;
use TCG\Voyager\Traits\VoyagerUser;
use Illuminate\Notifications\Notifiable;
use App\QueryFilter\Traits\FilterableTrait;
use App\View\Traits\FormInputAttributeLabelTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class User
 * @package App\Models
 * @property-read Role $role
 * @property int $id
 * @property string $name
 * @property string $locale
 * @property  int $firm_id
 * @property  mixed $firm
 * @property mixed role_id
 *
 */
class User extends \TCG\Voyager\Models\User
{
    use FormInputAttributeLabelTrait;
    use FilterableTrait;
    use VoyagerUser;
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];

    protected $table = 'users';

    protected $queryFilterClass = EmployeesFilter::class;
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function projects()
    {
        return $this->hasMany(Project::class,'designer_id');
    }

    public function firm()
    {
        return $this->morphTo();
    }

    public function getTypeAttribute()
    {
        if (in_array($this->role_id, [6,7,8]) and $this->firm_id != null) {
            $firm = $this->firm;
            if (isset($firm)) return $firm->name;
        }
        return "";
    }
}
