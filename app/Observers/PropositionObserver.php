<?php

namespace App\Observers;

use App\Models\EventStatus;
use App\Models\Proposition;
use App\Models\Status;

class PropositionObserver
{
    /**
     * Handle the Proposition "created" event.
     *
     * @param Proposition $proposition
     * @return void
     */
    public function created(Proposition $proposition)
    {
        $status = Status::query()->where('id', 1)->first();
        $model = Proposition::find($proposition->id);
        $data = ['minutes' => $status->minutes];
        $this->query($proposition);
        $model->update($data);
    }

    /**
     * Handle the Proposition "updated" event.
     *
     * @param Proposition $proposition
     * @return void
     */
    public function updated(Proposition $proposition)
    {
        if ($proposition->status != $proposition->getOriginal('status')) {
            $model = Proposition::find($proposition->id);
            $data = ['minutes' => $proposition->getStatus->minutes];
            if ($proposition->delay_s == 1)
                $data['delay_s'] = 0;
            $this->query($proposition);
            $model->update($data);
        }
    }

    /**
     * Handle the Proposition "deleted" event.
     *
     * @param Proposition $proposition
     * @return void
     */
    public function deleted(Proposition $proposition)
    {
    }

    /**
     * Handle the Proposition "restored" event.
     *
     * @param Proposition $proposition
     * @return void
     */
    public function restored(Proposition $proposition)
    {
        //
    }

    /**
     * Handle the Proposition "force deleted" event.
     *
     * @param Proposition $proposition
     * @return void
     */
    public function forceDeleted(Proposition $proposition)
    {
        //
    }

    protected function query(Proposition $proposition)
    {

        $user = \auth()->user();
        $data = [
            'prop_id' => $proposition->id,
            'user_id' => $user->id ?? null,
            'status' => $proposition->status ?? 1,
        ];
        if ($proposition->delay_s == 1)
            $data['delay_minutes'] = $proposition->minutes;
        EventStatus::query()->create($data);
    }

}
