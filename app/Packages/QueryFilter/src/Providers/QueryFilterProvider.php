<?php

namespace App\QueryFilter\Providers;



use Illuminate\Support\ServiceProvider;
use App\QueryFilter\Commands\FilterGeneratorCommand;
use App\QueryFilter\Commands\FilterHolderGeneratorCommand;

class QueryFilterProvider extends ServiceProvider
{
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
               FilterGeneratorCommand::class,
                FilterHolderGeneratorCommand::class,
            ]);
        }
    }
}
