<?php

namespace App\QueryFilter\Traits;


use Illuminate\Database\Eloquent\Builder;
use App\QueryFilter\QueryFilter;

/**
 * Trait FilterableTrait
 * @package App\Packages\QueryFilter\src\Traits
 * @method static Builder filter($value)
 */
trait FilterableTrait
{
    public function scopeFilter(Builder $builder, $filter)
    {
        if ($filter instanceof QueryFilter) {
            return $filter->apply($builder);
        }
        $class = null;

        if (method_exists($this, 'queryFilterClass')) {
            $class = $this->queryFilterClass();
        } elseif (property_exists($this, 'queryFilterClass')) {
            $class = $this->queryFilterClass;
        }


        if ($class) {
            /**
             * @var $queryFilter QueryFilter
             */
            $queryFilter = new $class($filter);

            return $queryFilter->apply($builder);
        }

        return $builder;
    }
}
