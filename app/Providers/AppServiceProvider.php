<?php

namespace App\Providers;

use App\Models\Proposition;
use App\Observers\PropositionObserver;
use Illuminate\Support\ServiceProvider;
use TCG\Voyager\Facades\Voyager;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Proposition::observe(PropositionObserver::class);
    }
}
