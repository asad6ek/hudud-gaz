<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use TCG\Voyager\Models\Permission;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {

        //// Proposition actions
        $models = Permission::all();
        foreach ($models as $model){
        Gate::define($model->key, function ($user) use ($model) {
            return $user->hasPermission($model->key);
        });}


/*
        Gate::define('browse_propositions', function ($user) {
            return $user->hasPermission('browse_propositions');
        });

        Gate::define('edit_propositions', function ($user) {
            return $user->hasPermission('edit_propositions');
        });
        Gate::define('delete_propositions', function ($user) {
            return $user->hasPermission('delete_propositions');
        });
        Gate::define('add_propositions', function ($user) {
            return $user->hasPermission('add_propositions');
        });

        //// User actions

        Gate::define('browse_users', function ($user) {
            return $user->hasPermission('browse_users');
        });
        Gate::define('add_users', function ($user) {
            return $user->hasPermission('add_users');
        });
        Gate::define('edit_users', function ($user) {
            return $user->hasPermission('edit_users');
        });
        Gate::define('delete_users', function ($user) {
            return $user->hasPermission('edit_users');
        });

        //// Recommendation actions

        Gate::define('browse_recommendations', function ($user) {
            return $user->hasPermission('browse_recommendations');
        });
        Gate::define('add_recommendations', function ($user) {
            return $user->hasPermission('add_recommendations');
        });
        Gate::define('delete_recommendations', function ($user) {
            return $user->hasPermission('delete_recommendations');
        });*/

        $this->registerPolicies();

        //
    }
}
