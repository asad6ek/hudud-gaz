<?php

namespace App\Providers;

use App\Http\View\Composers\CancelsComposer;
use App\Http\View\Composers\StatisticComposer;
use App\Models\Proposition;
use App\Models\Recommendation;
use App\Services\CRUD\RecommendationService;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(['voyager::dashboard.navbar', 'voyager::statistics.index'], StatisticComposer::class);

        View::composer('voyager::dashboard.second_navbar', CancelsComposer::class);
    }
}
