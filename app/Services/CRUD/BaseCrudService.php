<?php

namespace App\Services\CRUD;


use App\Exceptions\InvalidArgumentException;
use App\Interfaces\ICrudService;
use App\Models\BaseModel;
use App\Models\TechCondition;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\ModelNotFoundException;

abstract class BaseCrudService implements ICrudService
{

    public function list($data = [])
    {
        return $this->query()
            ->filter($data)
            ->paginate();
    }

    public function filterData($data = [])
    {
        return $this->query()
            ->filter($data);
    }

    public function create($data)
    {
        $class = $this->getModelClass();
        $model = new $class();
        $model->fill($data);
        $model->save();
        return $model;
    }

    public function update($data, $model)
    {
        $model->fill($data);
        $model->update();

        return $model;
    }

    public function delete($id)
    {
        $model = $this->find($id);
        return $model->delete();
    }

    /**
     * @param $id
     * @return mixed
     * @throws InvalidArgumentException
     * @throws ModelNotFoundException
     */
    public function find($id)
    {
        $class = $this->getModelClass();

        return $class::findOrFail($id);
    }

    /**
     * @return Builder
     * @throws InvalidArgumentException
     */
    protected function query()
    {
        $class = $this->getModelClass();

        return $class::query()
            ->orderBy('id', 'desc');
    }

    public function qrcode_check($qrcode, $status)
    {
        return TechCondition::query()->where('qrcode', $qrcode)
            ->whereHas('proposition', function ($query) use ($status) {
                $query->where('status', $status);
            })
            ->first();
    }

    /**
     * @return BaseModel | string
     * @throws InvalidArgumentException
     */
    protected function getModelClass()
    {
        if (property_exists($this, 'modelClass')) {
            return $this->modelClass;
        }

        throw new InvalidArgumentException(get_class($this) . ' modelClass property not implemented');
    }

}
