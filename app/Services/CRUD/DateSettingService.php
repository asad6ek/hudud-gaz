<?php


namespace App\Services\CRUD;


use App\Models\DateSetting;
use Carbon\Carbon;

class DateSettingService extends BaseCrudService
{
    protected $modelClass = DateSetting::class;

    public function create($data)
    {
        $date = explode('-', $data['from_to']);
        $data['from'] = Carbon::create($date[0])->format('Y-m-d H:i');
        $data['to'] = Carbon::create($date[1])->format('Y-m-d H:i');
        return parent::create($data);
    }


}
