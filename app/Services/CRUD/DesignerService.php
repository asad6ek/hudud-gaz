<?php


namespace App\Services\CRUD;


use App\Models\Designer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class DesignerService extends BaseCrudService {
    protected $modelClass = Designer::class;

    private $request;

    public function __construct(Request $request) {
        $this->request = $request;
    }

    /**
     * @throws \App\Exceptions\InvalidArgumentException
     */
    public function create($data) {
        $class = $this->getModelClass();
        $model = new $class();

        if (isset($data['document'])) {
            $data['document'] = $this->createPath();
        }
        $model->fill($data);
        $model->save();
    }

    public function update($data, $model) {
        if (isset($data['document'])) {
            $this->deletePath($model->document);
            $data['document'] = $this->createPath();
        }

        $model->fill($data);
        $model->save();
        return $model;
    }

    /**
     * @throws \App\Exceptions\InvalidArgumentException
     */
    public function delete($id) {
        $model = $this->find($id);
        $this->deletePath($model->document);
        return $model->delete();
    }

    private function deletePath($path) {
        File::delete(public_path('../storage/app/public/designer/' . $path));
    }

    private function createPath(): string {
        $fileName = time() . '.' . $this->request->file('document')->extension();
        $this->request->file('document')->move(public_path('../storage/app/public/designer'), $fileName);
        return $fileName;
    }
}
