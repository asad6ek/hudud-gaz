<?php


namespace App\Services\CRUD;


use App\Models\Designer;
use App\Models\MontageFirm;
use App\Models\Region;
use App\Models\User;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;

class EmployeesService extends BaseCrudService
{

    protected $modelClass = User::class;

    public function create($data) {

        $arr = [
            'name' => $data['name'],
            'password' => $this->hashingPassword($data['password']),
            'email' => $data['email'],
            'lastname' => $data['lastname'],
            'patronymic' => $data['patronymic'],
            'position' => $data['position'],
            'role_id' => $data['role_id'],
            'locale' => $data['locale'],
            'avatar' => '',
        ];

        $class = $this->getModelClass();
        $model = new $class();
        $model->fill($arr);
        if ($data['role_id'] == 6)
            Region::query()->where('id', $data['firm_id'])->firstOrFail()->users()->save($model);
        elseif ($data['role_id'] == 7)
            Designer::query()->where('id', $data['firm_id'])->firstOrFail()->users()->save($model);
        elseif ($data['role_id'] == 8)
            MontageFirm::query()->where('id', $data['firm_id'])->firstOrFail()->users()->save($model);
        else {

            $model->save();
        }
    }

    public function update($data, $model)
    {
        if (isset($data['old_password'])) {
            $model->update(['password' => $this->hashed($data['new_password'])]);
            return $model;
        }


        $arr = [
            'name' => $data['name'],
            'email' => $data['email'],
            'lastname' => $data['lastname'],
            'patronymic' => $data['patronymic'],
            'position' => $data['position'],
            'role_id' => $data['role_id'],
            'locale' => $data['locale']
        ];

        if (isset($data['avatar'])) {
            $this->deleteFile($model->avatar);
            $arr['avatar'] = $this->createFile($data['avatar']);
        }
        if (isset($data['password'])) {
            $arr['password'] = $this->hashingPassword($data['password']);
        }
        $model->fill($arr);
        if ((int)$data['firm_id'] ===0 )
            $model->save();
        elseif ($data['role_id'] == 6)
            $model = Region::query()->where('id', $data['firm_id'])->firstOrFail()->users()->save($model);
        elseif ($data['role_id'] == 7)
            $model = Designer::query()->where('id', $data['firm_id'])->firstOrFail()->users()->save($model);
        elseif ($data['role_id'] == 8)
            $model = MontageFirm::query()->where('id', $data['firm_id'])->firstOrFail()->users()->save($model);
        else {

            $model->save();
        }
        return $model;
    }


    /**
     * @param string $password
     * @return string
     */
    public function hashingPassword(string $password): string
    {
        return Hash::make($password);
    }

    private function createFile($file) {
        $name = time() . '.' . $file->extension();
        $file->storeAs('public/users', $name);
        return $name;
    }

    private function deleteFile($path) {
        File::delete('storage/users/' . $path);
    }

    private function hashed($pass) {
        return Hash::make($pass);
    }
}
