<?php

namespace App\Services\CRUD;


use App\Models\Equipment;
use App\Models\EquipmentType;
use Illuminate\Database\Eloquent\Model;

class EquipmentService extends BaseCrudService
{
    protected $modelClass = Equipment::class;

    public function create($data)
    {
        $arr = [
            'name' => $data['name'],

        ];

        $class = $this->getModelClass();
        $model = new $class();
        $model->fill($arr);
        $model->save();

    }

    public function update($data, $model)
    {

        $arr = [
            'name' => $data['name'],

        ];

        $model->fill($arr);
        $model->save();


        return $model;
    }

    public function save_types($data, $equipment)
    {
        $equipment_types = new EquipmentType();
        $equipment_types -> fill(
            [
                'type'=>$data['type'],
                'equipment_id'=>$equipment->id,
                'order'=>intval($data['order']),
            ]
        )->save();
    }

    public function update_types($data, $model)
    {
        $model -> fill(
            [
                'type'=>$data['type'],
                'order'=>intval($data['order'])
            ]
        );
        $model->save();
    }

    public function delete_type($model){
        $model->delete();
    }
}
