<?php


namespace App\Services\CRUD;


use App\Models\MontageEmployee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class MontageEmployeeService extends BaseCrudService {

    protected $modelClass = MontageEmployee::class;

    /**
     * @throws \App\Exceptions\InvalidArgumentException
     */
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function create($data) {
        $class = $this->getModelClass();
        $model = new $class();

        $data["path"] = $this->createPath();
        $model->fill($data);
        $model->save();
    }

    public function update($data, $model) {
        $new_data = $data;
        if (isset($new_data['path'])) {
            $this->deletePath($model->path);
            $new_data['path'] = $this->createPath();
        }

        $model->fill($new_data);
        $model->save();

        return $model;
    }

    public function delete($id) {
        $model = $this->find($id);
        $this->deletePath($model->path);
        return $model->delete();
    }

    private function deletePath($path) {
        File::delete(public_path('../storage/app/public/montage_employees/' . $path));
    }

    private function createPath(): string {
        $fileName = time() . '.' . $this->request->file('path')->extension();
        $this->request->file('path')->move(public_path('../storage/app/public/montage_employees'), $fileName);
        return $fileName;
    }
}
