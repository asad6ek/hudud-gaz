<?php


namespace App\Services\CRUD;


use App\Exceptions\InvalidArgumentException;
use App\Models\MontageFirm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class MontageFirmService extends BaseCrudService
{
    protected $modelClass = MontageFirm::class;

    /**
     * @var Request
     */
    private Request $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @throws InvalidArgumentException
     */
    public function create($data) {
        $class = $this->getModelClass();
        $model = new $class();

        if (isset($data['path'])) {
            $data["path"] = $this->createPath();
        }
        $model->fill($data);
        $model->save();
    }

    public function update($data, $model) {
        if (isset($data['path'])) {
            $this->deletePath($model->path);
            $data["path"] = $this->createPath();
        }

        $model->fill($data);
        $model->save();
        return $model;
    }

    public function delete($id) {
        $model = $this->find($id);
        $this->deletePath($model->path);
        return $model->delete();
    }

    private function deletePath($path) {
        File::delete(public_path('../storage/app/public/montage_firms/' . $path));
    }

    private function createPath(): string {
        $fileName = time() . '.' . $this->request->file('path')->extension();
        $this->request->file('path')->move(public_path('../storage/app/public/montage_firms'), $fileName);
        return $fileName;
    }
}
