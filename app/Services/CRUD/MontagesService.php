<?php


namespace App\Services\CRUD;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Models\Montage;
use App\Models\TechCondition;
use App\Models\License;
use App\Models\EquipmentType;
use Barryvdh\DomPDF\Facade as PDF;

class MontagesService extends BaseCrudService {

    protected $modelClass = TechCondition::class;

    public function upload($data, $tech) {
        if (user_role('engineer')) {
            return $this->generate_license($tech);
        }

        $status = ['status' => 18];
        $montage = Montage::query()->where('tech_id', $tech->id)->first();
        $fileName = time() . '.' . $data->montage->extension();
        $data->montage->move(public_path('../storage/app/public/montages'), $fileName);
        if (!is_null($montage)) {
            File::delete('storage/montages/' . $montage->getAttribute('path'));
            $montage->update(['path' => $fileName]);
        } else {
            $montage = new Montage();
            $montage->fill([
                'path' => $fileName,
                'tech_id' => $tech->id
            ])->save();
        }
        return $this->update($status, $tech->proposition);
    }

    public function generate_license($tech) {
        $proposition = $tech->proposition;
        $status['status'] = 21;
        $this->update($status, $proposition);

        $applicant = $proposition->applicantable;
        $recommendation = $proposition->recommendation;
        $designer = $tech->designer;
        $montage_firm = $tech->montage_firm;
        $equips = $proposition->equipments;
        $gas_meters = $this->gas_meters($recommendation->gas_meter);
        $data['region_name'] = get_shareholder_name();
        view()->share(['proposition' => $proposition, 'applicant' => $applicant, 'recommendation' => $recommendation,
            'designer' => $designer, 'montage_firm' => $montage_firm, 'equips' => $equips, 'data' => $data,
            'gas_meters' => $gas_meters]);

        $file = time() . '.pdf';
        $license = new License();
        $license->fill([
            'prop_id' => $proposition->id,
            'file' => $file,
            'district' => $montage_firm->district(true)
        ]);
        $license->save();
        if ($proposition->applicantable_type == 'App\Models\LegalEntity') {
            File::makeDirectory('public/licenses', 0777, true, true);
            PDF::loadView('voyager::license')->save('storage/licenses/' . $file);
            return redirect(Storage::url('public/licenses/' . $file));
        }

        PDF::loadView('voyager::license')->save('storage/licenses/' . $file);
        return redirect(Storage::url('public/licenses/' . $file));
    }

    private function gas_meters($data) {
        $elements = json_decode($data);
        if (!$elements)
            return [];
        foreach ($elements as $element) {
            $element->type = EquipmentType::query()->find($element->type)->getAttribute('type');
        }

        return $elements;
    }
}
