<?php


namespace App\Services\CRUD;


use App\Models\Project;
use App\Models\TechCondition;
use Illuminate\Support\Facades\File;

class ProjectService extends BaseCrudService
{


    protected $modelClass = Project::class;

    public function create($data)
    {
        $arr = [
            'tech_id' => intval($data['tech_id']),
            'path' => $this->createPath($data['path'])
        ];

        $class = $this->getModelClass();
        $model = new $class();
        $model->fill($arr);
        $model->save();
    }

    public function update($data, $model)
    {
        $arr = [
            'tech_id' => intval($data['tech_id']),
            'path' => $data['path'],
            'comment' => $data['comment']
        ];
        $model->fill($arr);
        $model->save();
        return $model;
    }

    public function update_status($data, $model)
    {
        return parent::update($data, $model);
    }

    public function upload($data, TechCondition $tech)
    {
        $project = Project::query()->where('tech_id', $tech->id)->first();
        $fileName = time() . '.' . $data->project->extension();
        $data->project->move(public_path('../storage/app/public/projects'), $fileName);
        if (!is_null($project)) {
            File::delete('storage/projects/' . $project->path);
            $project->update(['path' => $fileName]);
        } else {
            $project = new Project();
            $project->fill([
                'path' => $fileName,
                'tech_id' => $tech->id
            ])->save();
        }

        $proposition = $tech->proposition;
        $status = ['status' => 12];
        if (user_role('engineer'))
            $status['status'] = 16;
        $proposition->update($status);
    }

    public function getTechs($status, $user_id)
    {

    }


    private function createPath($file): string
    {
        $fileName = time() . '.' . $file->extension();
        $file->storeAs('public/projects', $fileName);
        return $fileName;
    }
}
