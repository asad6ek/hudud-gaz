<?php


namespace App\Services\CRUD;


use App\Models\Individual;
use App\Models\LegalEntity;
use App\Models\Proposition;
use App\Models\TechCondition;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class PropositionService extends BaseCrudService
{

    protected $modelClass = Proposition::class;
    /**
     * @var Request
     */
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function create($data)
    {
        $fileName = $this->createPath();

        $data['path'] = $fileName;
        $class = $this->getModelClass();
        $model = new $class();
        $model->fill($data)->save();

        return $model;
    }


    public function update($data, $model)
    {

        if (user_role('region')) {

            if ((int)$data['type'] === 1) {
                unset($data['activity_type']);
                $applicant = Individual::query()->firstOrCreate(['tin' => $data['tin']]);
                $applicant->fill($data)->save();
                $model->fill([
                    'applicantable_type' => "App\Models\Individual",
                    'applicantable_id' => $applicant->id
                ]);

            } elseif ((int)$data['type'] === 2) {
                $applicant = LegalEntity::query()->firstOrCreate(['legal_entity_tin' => $data['legal_entity_tin']]);
                $applicant->fill($data)->save();

                $model->fill([
                    'applicantable_type' => "App\Models\LegalEntity",
                    'applicantable_id' => $applicant->id
                ]);
            }
        }

        $fileName = $model->path;
        if (isset($data['path'])) {
            $this->deletePath($model->path);
            $fileName = $this->createPath();
        }

        $data['file_name'] = $fileName;
        $model->fill($data)->save();

        return $model;
    }

    public function delete($id)
    {
        $model = $this->find($id);
        $this->deletePath($model->path);
        return $model->delete();
    }

    private function deletePath($path)
    {
        File::delete(public_path('../storage/app/public/logos/' . $path));
    }

    public function createPath(): string
    {
        $fileName = time() . '.' . $this->request->path->extension();
        $this->request->path->move(public_path('../storage/app/public/propositions'), $fileName);
        return $fileName;
    }


    public function inToday()
    {
        return Proposition::query()->whereDate('created_at', today())->count();
    }

    public function inProgress()
    {
        return Proposition::query()->whereNotIn('status', [9, 21])->count();
    }

    public function inLate()
    {
        return Proposition::query()->where('delay_s', 1)->count();
    }

    public function inYear()
    {
        return Proposition::query()->whereIn('status', [9, 21])->where('created_at', '>', Carbon::now()->firstOfYear())->count();
    }

    /* Region menu */
    public function received_proposition() {
        return $this->reg([1, 2]);
    }

    public function progress_recommendations() {
        return $this->reg([4, 5]);
    }

    public function created_recommendations() {
        return $this->reg([3]);
    }

    public function cancelled_recommendations()
    {
        return $this->reg([6]);
    }

    public function archive_recommendations()
    {
        return $this->reg([10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21]);
    }

    private function reg($status) {
        return Proposition::query()
            ->where('region_id', auth()->user()->firm_id)
            ->whereIn('status', $status)->count();
    }

    /* Designer menu */
    public function created_projects()
    {
        return $this->designer([11, 11]);
    }

    public function progress_projects()
    {
        return $this->designer([12, 13]);
    }

    public function cancelled_projects()
    {
        return $this->designer([14, 14]);
    }

    public function archive_projects() {
        return $this->designer([15, 22]);
    }

    private function designer($status) {
        return TechCondition::query()
            ->where('designer_id', auth()->user()->firm_id)
            ->whereHas('proposition', function($query) use ($status) {
                return $query->whereBetween('status', $status);
            })->count();
    }

    /* Installer menu */
    public function created_montages() {
        return $this->installer([17, 17]);
    }

    public function progress_montages() {
        return $this->installer([18, 18]);
    }

    public function cancelled_montages()
    {
        return $this->installer([20, 20]);
    }

    public function archive_montages() {
        return $this->installer([19, 22]);
    }

    private function installer($status) {
        return TechCondition::query()
            ->where('firm_id', auth()->user()->firm_id)
            ->whereHas('proposition', function($query) use ($status) {
                return $query->whereBetween('status', $status);
            })->count();
    }
}
