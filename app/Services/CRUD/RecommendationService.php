<?php


namespace App\Services\CRUD;

use App\Models\Recommendation;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Request;

class RecommendationService extends BaseCrudService
{

    protected $modelClass = Recommendation::class;
    /**
     * @var Request
     */
    private $request;
    /**
     * @var StatusService
     */
    private $service;

    public function __construct(Request $request, StatusService $service)
    {
        $this->request = $request;
        $this->service = $service;
    }

    public function create($data)
    {
        $recommendation = new Recommendation();
        $path = uniqid(rand(), false) . '.pdf';
        $recommendation->fill([
            $data['rec_id'] = (Recommendation::query()->get(['id'])->last()->id ?? 0) + 1,
            'path' => $path,
            'prop_id' => $data['prop_id'],
            'equipments' => $data['equipments'] ?? null,
            'gas_meter' => $data['gas_meter'] ?? null,
            'gas_network' => $data['gas_network'] ?? null,
        ]);

        if ($data['type'] == 'success') $this->save_success($recommendation, $data);
        elseif($data['type'] == 'fail') $this->save_fail($recommendation, $data);


        $recommendation->save();
        return redirect()->route('admin.recommendations.index');
    }

    public function save_success($recommendation, $data)
    {
        if ($data['length_type'] == 1) {
            $data['key'] = "Ер ости узунлиги";
            $key = "under_len";
        } else {
            $data['key'] = "Ер усти узунлиги";
            $key = "above_len";
        }

        $data['json'] = $recommendation->getEquipments();

        $recommendation->fill([
            'address' => $data['address'],
            'status' => 1,
            'access_point' => $data['access_point'],
            $key => $data['length'],
            'pipe_size' => $data['pipe_size'],
            'depth' => $data['depth'],
            'avg_winter' => $data['avg_winter'],
            'avg_summer' => $data['avg_summer'],
            'capacity' => $data['capacity'],
            'real_capacity' => $data['real_capacity'],
            'grc' => $data['grc'],
            'gas_consumption' => $data['gas_consumption'],
            'additional' => $data['additional'],
        ]);
        $this->createPDF($recommendation, $data, 'success');
    }

    public function save_fail($recommendation, $data)
    {
        $recommendation->fill([
            'description' => $data['description'],
            'additional' => $data['additional'],
            'status' => 0,
        ]);
        $this->createPDF($recommendation, $data);
    }

    public function createPDF($recommendation, $data, $type = 'fail', $status = 3) {
        $proposition = $recommendation->proposition;
        $this->update_status(['status' => $status], $proposition);
        $region = $proposition->region;
        $data['general'] = get_general_settings();
        $data['branch_name'] = get_branch_name();
        $data['shareholder_name'] = get_shareholder_name();
        $data['region'] = $region;
        $data['org_name'] = $region->org_name;
        $data['region_name'] = $region->region_name;
        $data['region_leader'] = $region->section_leader;
        $data['region_engineer'] = $region->lead_engineer;
        $data['proposition'] = $proposition;

        if ($type == 'success')
        {
            $data['number'] = $proposition->number;
            $data['customer'] = $proposition->applicantable->name;
            $data['building_type'] = $proposition->building_type;
            $data['activity_type'] = $proposition->activityName;

            $data['full_len'] = $data['length'];
            PDF::loadView('voyager::recommendations.pdf.success', ['data' => $data])->save('storage/recommendations/' . $recommendation->path);
        }
        else{
            PDF::loadView('voyager::recommendations.pdf.fail', ['data' => $data])->save('storage/recommendations/' . $recommendation->path);
        }
    }

    public function upload($data, $recommendation)
    {
        if ($data->hasFile('recommendation'))
        {
            try {
                File::delete('storage/recommendations/'. $recommendation->path);
                $pdf = $data->file('recommendation');
                $pdf->storeAs('public/recommendations', $recommendation->path);
            } catch (\Exception $e){
                return $e;
            }
        }
        $this->service->update(['status' => 4], $recommendation->proposition);
    }

    public function update($data, $model)
    {
        $data['status'] = 0;
        if (isset($data['length_type'])) {
            if ($data['length_type'] == 1) {
                $data['key'] = "Ер ости узунлиги";
                $data['under_line'] = "under_line";
            }
            else {
                $data['key'] = "above_len";
                $data['key'] = "Ер усти узунлиги";
            }
            $data['status'] = 1;
        }
        $data['rec_id'] = $model->id;
        $data['json'] = $model->getEquipments();
        $this->createPDF($model, $data, $data['type']);
        return parent::update($data, $model);
    }

    public function update_status($data, $model)
    {
        return parent::update($data, $model);
    }
}
