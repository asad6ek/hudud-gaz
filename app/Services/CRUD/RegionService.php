<?php


namespace App\Services\CRUD;


use App\Models\Region;

class RegionService extends BaseCrudService {

    protected $modelClass = Region::class;

    /**
     * @throws \App\Exceptions\InvalidArgumentException
     */
    public function create($data) {
        $class = $this->getModelClass();
        $model = new $class();
        $model->fill($data);
        $model->save();
    }

    public function update($data, $model) {
        $model->fill($data);
        $model->save();

        return $model;
    }
}
