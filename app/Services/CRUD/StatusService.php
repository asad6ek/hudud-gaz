<?php


namespace App\Services\CRUD;


use App\Models\EventStatus;
use App\Models\Status;

class StatusService extends BaseCrudService
{

    protected $modelClass = Status::class;

    public function all()
    {
        return Status::all();
    }

    public function update($data, $model)
    {
//        if (isset($data['status']))
//            EventStatus::query()->insert(['prop_id' => $model->id, 'user_id' => auth()->id(), 'status' => $data['status']]);
        return parent::update($data, $model); // TODO: Change the autogenerated stub
    }
}
