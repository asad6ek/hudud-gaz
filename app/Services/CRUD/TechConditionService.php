<?php


namespace App\Services\CRUD;

use App\Models\Proposition;
use App\Models\TechCondition;
use App\Models\TechEquipment;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\File;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class TechConditionService extends BaseCrudService
{

    protected $modelClass = Proposition::class;
    /**
     * @var Request
     */
    private $request;
    /**
     * @var StatusService
     */
    private $service;

    public function __construct(Request $request, StatusService $service)
    {
        $this->request = $request;
        $this->service = $service;
    }

    public function create($data)
    {
        $tech_condition = new TechCondition();
        $path = uniqid(rand(), true) . '.pdf';
        $qrcode = $this->qrcode_generate();
        $tech_condition->fill([
            'path' => $path,
            'prop_id' => $data['prop_id'],
            'qrcode' => $qrcode
        ]);
        $proposition = $tech_condition->proposition;
        $region = $proposition->region;

        $data['general'] = get_general_settings();
        $data['region'] = $region;
        $data['proposition'] = $proposition;
        $data['region_engineer'] = $region->lead_engineer;
        $data['org_name'] = $region->org_name;
        $data['month'] = $proposition->month(date('n'));
        $data['id'] = (TechCondition::query()->get(['id'])->last()->id ?? 0) + 1;
        $this->service->update(['status' => 8], $proposition);

        QrCode::format('svg')->size(200)->errorCorrection('H')->generate($qrcode, 'storage/tech_conditions/qrcode.svg');
        if ($data['type'])
            PDF::loadView('voyager::tech_conditions.pdf.success', ['data' => $data])->save('storage/tech_conditions/' . $path);
        else
            PDF::loadView('voyager::tech_conditions.pdf.fail', ['data' => $data])->save('storage/tech_conditions/' . $path);

        $tech_condition->save();
        $this->techEquipments($proposition->recommendation->equipments, $data['prop_id']);
        return redirect()->route('admin.tech_conditions.index');
    }

    public function upload_pdf($data, $tech_condition)
    {
        if ($data->hasFile('file'))
        {
            File::delete('storage/tech_conditions/'. $tech_condition->path);
            $pdf = $data->file('file');
            $pdf->storeAs('public/tech_conditions', $tech_condition->path);
            $this->service->update(['status' => 9], $tech_condition->proposition);
        }
    }

    public function update($data, $model)
    {
        $fileName = $model->path;
        if (isset($data['path'])) {
            $this->deletePath($model->path);
            $fileName = $this->createPath();
        }
        $arr = [
            'path' => $fileName,
            'prop_id' => $data['prop_id'],
            'qrcode' => $data['qrcode'],
        ];
        $model->fill($arr);
        $model->save();
        return $model;
    }

    public function update_status($data, $model)
    {
        return parent::update($data, $model);
    }

    public function delete($id)
    {
        $model = $this->find($id);
        $this->deletePath($model->path);
        return $model->delete();
    }

    private function deletePath($path)
    {
        File::delete(public_path('../storage/app/public/tech_conditions/' . $path));
    }

    public function createPath(): string
    {
        $fileName = time() . '.' . $this->request->path->extension();
        $this->request->path->move(public_path('../storage/app/public/tech_conditions'), $fileName);
        return $fileName;
    }

    public function qrcode_generate(){
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $pin = mt_rand(1000000, 9999999)
            . mt_rand(1000000, 9999999)
            . $characters[rand(0, strlen($characters) - 1)];

        return str_shuffle($pin);
    }

    private function techEquipments($equipments, $id) {
        $equips = json_decode($equipments, true);
        if (!$equips)
            return;

        foreach ($equips as $equip) {
            TechEquipment::query()->create([
                'prop_id' => $id,
                'type_id' => $equip['type'],
                'number' => $equip['number'],
                'note' => $equip['note']
            ]);
        }
    }
}
