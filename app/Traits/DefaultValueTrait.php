<?php


namespace App\Traits;


use Illuminate\Database\Eloquent\Model;

trait DefaultValueTrait
{
    public static function bootDefaultValueTrait()
    {
        static::saving(function ($model) {
            $values = $model->getDefaultValueList();

            foreach ($model->attributes as $attr => $value) {
                if (isset($values[$attr]) && $value === null) {

                    $model->setDefaultValueToNull($attr, $values);
                }
            }
        });
    }

    protected function setDefaultValueToNull($attribute, array $values = [])
    {
        if (isset($values[$attribute])) {
            $this->{$attribute} = $values[$attribute];
        }
    }

    protected function getDefaultValueList()
    {
        if (method_exists($this, 'getDefaultValues')) {
            return $this->getDefaultValues();
        } elseif (property_exists($this, 'defaultValues')) {
            return $this->defaultValues;
        }

        return [];
    }
}
