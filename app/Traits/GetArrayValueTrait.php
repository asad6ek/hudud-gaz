<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-07-23
 * Time: 15:03
 */

namespace App\Traits;


use Illuminate\Support\Arr;

trait GetArrayValueTrait
{
    protected function getValue($array, $key, $default = null)
    {
        return Arr::get($array, $key, $default);
    }
}
