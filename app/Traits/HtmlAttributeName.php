<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-06-09
 * Time: 11:26
 */

namespace App\Traits;


use InvalidArgumentException;

trait HtmlAttributeName
{
    protected static $attributeRegex = '/(^|.*\])([\w\.\+]+)(\[.*|$)/u';

    public static function getAttributeName($attribute)
    {
        if (preg_match(static::$attributeRegex, $attribute, $matches)) {
            return $matches[2];
        }

        throw new InvalidArgumentException('Attribute name must contain word characters only.');
    }
}
