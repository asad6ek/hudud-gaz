<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-06-17
 * Time: 18:44
 */

namespace App\Traits;


trait ShopSetterGetterTrait
{
    protected $shop_id;

    /**
     * @return mixed
     */
    public function getShopId()
    {
        return $this->shop_id ?: \Shop::id();
    }

    /**
     * @param mixed $shop_id
     * @return self
     */
    public function setShopId($shop_id)
    {
        $this->shop_id = $shop_id;
        return $this;
    }
}
