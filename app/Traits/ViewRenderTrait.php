<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-06-26
 * Time: 14:47
 */

namespace App\Traits;


use Illuminate\Support\Facades\View;

trait ViewRenderTrait
{
    public function getViewPrefix()
    {
        if (property_exists($this, 'view_prefix')) {
            return $this->view_prefix;
        }
        return false;
    }

    public function render($view, $data = [])
    {
        if (($viewPrefix = $this->getViewPrefix()) !== false) {
            return $this->renderStatic($viewPrefix . '.' . $view, $data);
        }

        return $this->renderDynamic($view, $data);
    }

    public function renderDynamic($view, $data = [])
    {
        return View::first([
            $this->getViewDynamicPrefix() . '.' . $view,
            'common.admin-side.' . $view
        ], $data);
    }

    public function renderStatic($view, $data = [])
    {
        return View::make($view, $data);
    }

    protected function getViewDynamicPrefix()
    {
        $data = explode('\\', get_class($this));

        return strtolower($data[count($data) - 2]);
    }


}
