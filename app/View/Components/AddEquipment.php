<?php

namespace App\View\Components;

use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class AddEquipment extends Component
{
    public $componentIndex;
    public $equipments;

    /**
     * Create a new component instance.
     *
     * @param $equipments
     * @param $componentIndex
     */
    public function __construct($equipments, $componentIndex)
    {
        $this->equipments = $equipments;
        $this->componentIndex = $componentIndex;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return View|string
     */
    public function render()
    {
        return view('components.add-equipment');
    }
}
