<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Errors extends Component
{
    /**
     * @var array
     */
    public $errors;

    /**
     * Create a new component instance.
     *
     * @param array $errors
     */
    public function __construct($errors = [])
    {

        $this->errors = $errors;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.errors');
    }
}
