<?php

namespace App\View\Components\Html;

use App\View\Traits\ViewComponentOptionAttributeResolverTrait;
use Illuminate\View\Component;

/**
 * Class Tag
 * @package App\View\Components\Html
 *
 * @usage
 *
 * <x-html.tag type="label" :options="$labelOptions" />
 */
class Tag extends Component
{
    use ViewComponentOptionAttributeResolverTrait;

    /**
     * @var string
     */
    protected $type;
    /**
     * @var array
     */
    protected $options;

    /**
     * Create a new component instance.
     *
     * @param string $type
     * @param array $options
     */
    public function __construct($type = '', $options = [])
    {
        $this->type = $type;
        $this->options = $options;


    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.html.tag', $this->data());
    }

    public function type()
    {

        return $this->type ?: ($this->paramOptions()['type'] ?? 'div');
    }

    public function options()
    {
        $options = $this->paramOptions();
        foreach ($this->excludedArrProperties() as $property) {
            if (array_key_exists($property, $options)) {
                unset($options[$property]);
            }
        }

        return $options;
    }

    public function hasContent()
    {
        return (boolean) ($this->paramOptions()['content'] ?? false);
    }

    public function content()
    {
        return $this->paramOptions()['content'] ?? '';
    }

    public function excludedArrProperties()
    {
        return ['content', 'type',];
    }

}
