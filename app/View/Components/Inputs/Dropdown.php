<?php

namespace App\View\Components\Inputs;

use App\View\AttributeLister;
use App\View\Traits\ExcludedOptionsTrait;
use Illuminate\View\Component;

class Dropdown extends Component
{
    use ExcludedOptionsTrait;

    private $name;
    private $value;
    /**
     * @var array
     */
    protected $items;

    /**
     * Create a new component instance.
     *
     * @param $name
     * @param $value
     * @param array $options
     */
    public function __construct($name = '', $value = '', $items = [], $options = [])
    {
        $this->name = $name;
        $this->value = $value;
        $this->options = $options;
        $this->items = $items;

    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.inputs.dropdown', $this->data());
    }

    public function name()
    {
        return $this->name ?: $this->paramOptions()['name'] ?? '';
    }

    public function value()
    {
        $value = $this->value ?: $this->paramOptions()['value'] ?? '';


        return static::resolveViewDataByValue($value);
    }

    public function items()
    {
        return $this->resolveViewDataByAttribute('items');

    }


    public function excludedOptionProperties()
    {
        return ['items', 'value', 'name'];
    }

    public static function arrToOption($options)
    {
        return [];
    }

    public function isSelectedOption($optionValue)
    {
        $value = $this->value();
        if (is_array($value)) {
            return in_array($optionValue, $value);
        }

        if (is_int($optionValue) && trim($value) !=='') {

            return $optionValue === (int)$value;
        }



        return $optionValue === $value;

    }


}
