<?php

namespace App\View\Components\Inputs;

use App\View\Interfaces\IFormInputable;

class FormCheckbox extends AbstractFormInput
{
    /**
     * @var int
     */
    private $value;

    public function __construct(IFormInputable $model, $attribute, $value = 1, array $options = [])
    {
        parent::__construct($model, $attribute, $options);
        $this->value = $value;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.inputs.form-checkbox');
    }

    public function checkedValue()
    {
        return $this->value;
    }

    public function options()
    {
        $options = $this->excludedOptions();

        if (!array_key_exists('class', $options)) {
            $options['class'] = 'switch-icon';
        }

        return $options;
    }
}
