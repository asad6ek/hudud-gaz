<?php

namespace App\View\Components\Inputs;

use App\View\Interfaces\IFormInputable;
use App\View\ViewIdProvider;

class FormDropDown extends AbstractFormInput
{

    protected $items;

    public function __construct($items, IFormInputable $model, $attribute, array $options = [])
    {
        parent::__construct($model, $attribute, $options);
        $this->items = $items;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.inputs.form-drop-down', $this->data());
    }

    public function options()
    {
        $options = $this->excludedOptions();

        if (empty($options['id'])) {
            $options['id'] = 'w-'.ViewIdProvider::getNewId();
        }

        if (empty($options['class'])) {
            $options['class'] = 'form-control';
        }

        return $options;
    }

    public function items()
    {
        return $this->items;
    }


    public function excludedOptionProperties()
    {
        return [
            'value', 'label', 'attribute', 'labelOptions', 'divOptions'
        ];
    }

}
