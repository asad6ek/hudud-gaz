<?php

namespace App\View\Components\Inputs;



/**
 * Class FormInput
 * @package App\View\Components\Inputs
 *
 * @usage
 *
 * model  is a object which implements IFormInputableInterface
 *
 * <x-inputs.form-input :model="$model" attribute="name" />
 *
 */
class FormPassword extends FormInput
{

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {

        return view('components.inputs.form-input', $this->data());
    }

    public function options()
    {
        $options = parent::options();

        $options['type'] = 'password';
        return $options;
    }

}
