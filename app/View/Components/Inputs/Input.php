<?php

namespace App\View\Components\Inputs;

use App\View\Traits\ViewComponentOptionAttributeResolverTrait;
use App\View\ViewIdProvider;
use Illuminate\View\Component;

/**
 * Class Input
 * @package App\View\Components\Inputs
 *
 * @usage
 *
 *  <x-inputs.input :options="$inputOptions" :value="$inputValue" :name="$inputName" />
 */
class Input extends Component
{
    use ViewComponentOptionAttributeResolverTrait;

    /**
     * @var string
     */
    private $name;
    /**
     * @var null
     */
    private $value;


    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($name, $value = null, $options = [])
    {

        $this->name = $name;
        $this->value = $value;
        $this->options = $options;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.inputs.input', [
            'name' => $this->name,
            'value' => $this->value,
            'options' => $this->options(),
            'class' => $this->inputClasses(),
            'type' => $this->inputType(),
            'id' => $this->ID(),
        ]);
    }

    public function inputType()
    {
        if (!empty($this->paramOptions()['type']) && in_array($this->paramOptions()['type'], self::validInputTypes())) {
            return $this->paramOptions()['type'];
        }

        return 'text';
    }

    public static function validInputTypes()
    {
        return [
            'text',
            'email',
            'radio',
            'checkbox',
        ];
    }

    public function inputClasses()
    {
        return $this->paramOptions()['class'] ?? 'form-control';
    }

    public function options()
    {
        $excludedOptions = $this->exlcludedOptions();
        $options = $this->paramOptions();
        foreach ($excludedOptions as $option) {
            if (!array_key_exists($option, $options)) {
                unset($options[$option]);
            }
        }

        return $options;
    }

    protected function exlcludedOptions()
    {
        return ['name', 'type', 'value', 'class'];
    }

    public function ID()
    {
        return $this->paramOptions()['id'] ?? ViewIdProvider::getNewId();
    }

}
