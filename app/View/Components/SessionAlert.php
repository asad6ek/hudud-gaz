<?php

namespace App\View\Components;

use App\View\Traits\ViewComponentOptionAttributeResolverTrait;
use Illuminate\View\Component;

class SessionAlert extends Component
{
    use ViewComponentOptionAttributeResolverTrait;

    protected $alertMessagesTypes = ['success', 'error', 'info', 'danger'];

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.session-alert', [
            'messages' => $this->getMessages(),
        ]);
    }


    public function getMessages()
    {
        return collect($this->getBasicMessage())
            ->mergeRecursive($this->getHelperMessage());
    }

    public function getBasicMessage()
    {
        $messages = [];
        foreach ($this->alertMessagesTypes as $messagesType) {
            if (!session()->has($messagesType)) {
                continue;
            }

            $message = session()->get($messagesType);

            $messages[$messagesType][] = $message;
        }

        return $messages;
    }

    public function getHelperMessage()
    {
        if (session()->has('alert-type')) {
            $arr [session()->get('alert-type')] = [session()->get('alert-message')];

            return $arr;
        }
        return [];
    }

}
