<?php

namespace App\View\Components;

use App\Models\StatusList;
use Illuminate\Support\Arr;
use Illuminate\View\Component;

class Status extends Component
{
    public $status;

    /**
     * Create a new component instance.
     *
     * @param $status
     */
    public function __construct($status)
    {
        $this->status = $status;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.status-component', $this->data());
    }

    public function statusClass()
    {
        return Arr::get(StatusList::classList(), $this->status);
    }

    public function statusText()
    {
        return Arr::get(StatusList::activeInactive(), $this->status);
    }

}
