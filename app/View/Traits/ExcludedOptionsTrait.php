<?php

namespace App\View\Traits;


trait ExcludedOptionsTrait
{
    use ViewComponentOptionAttributeResolverTrait;

    public function excludedOptions()
    {
        $options = $this->paramOptions();
        $excludedOptions = [];

        if (method_exists($this, 'excludedOptionProperties')) {
            $excludedOptions = $this->excludedOptionProperties();
        } elseif (property_exists($this, 'excludedOptionProperties')) {
            $excludedOptions = $this->excludedOptionProperties;
        }

        if (!$excludedOptions) {
            return $options;
        }

        foreach ($excludedOptions as $property) {
            if (array_key_exists($property, $options)) {
                unset($options[$property]);
            }
        }

        return $options;
    }

    public function options()
    {
        return $this->excludedOptions();
    }

}
