<?php

namespace App\View;


class ViewIdProvider
{
    static protected $currentId = 0;

    public static function getNewId()
    {
        return self::$currentId++;
    }

}
