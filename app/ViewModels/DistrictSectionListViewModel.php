<?php


namespace App\ViewModels;


use App\Models\Activity;
use App\Models\Proposition;
use App\Models\Region;
use App\Services\CRUD\SectionsService;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Spatie\ViewModels\ViewModel;

class DistrictSectionListViewModel extends ViewModel
{

    /**
     * @var SectionsService
     */
    private $service;
    /**
     * @var Request
     */
    private $request;

    public function __construct(SectionsService $service, Request $request) {
        $this->service = $service;
        $this->request = $request;
    }

    public function models() {
        $models = Region::query()->with('propositions');

        if (isset($this->request->section) and (int)$this->request->section !== 0) {
            $section = (int)$this->request->section;
            $models->whereHas('propositions', function ($query) use ($section) {
                $query->whereIn('status', $section === 1 ? Proposition::getSuccessStatus() : Proposition::getRejectedStatus());
            });
        }
        return $models->get();
    }

    public function section() {
        return (isset($this->request->section) and (int)$this->request->section !== 0) ? (int)$this->request->section : 0;
    }

    public function activityTypes() {
        return Activity::query()->pluck('activity_name', 'id');
    }

    }
