<?php


namespace App\ViewModels;


use App\Filters\Holders\EmployeesFilterHolder;
use App\Models\BaseModel;
use App\Models\Proposition;
use App\Models\User;
use App\Services\CRUD\BaseCrudService;
use App\Services\CRUD\EmployeesService;
use Illuminate\Http\Request;
use Spatie\ViewModels\ViewModel;
use Symfony\Component\HttpFoundation\Response;
use TCG\Voyager\Models\Role;

class EmployeesListViewModel extends ViewModel
{
    /**
     * @var BaseCrudService
     */
    private $service;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var User
     */
    private $user;
    public function __construct(EmployeesService $service, Request $request, $user)
    {
        $this->request = $request;
        $this->service = $service;
        $this->user = $user;

    }

    public function models()
    {
        return User::query()->with('role')->orderBy('id', 'desc')->get();
//            $this->service->filterData($this->filterDto())->with('role')->get();
    }

    public function locales(){
        return BaseModel::localeName();
    }
    public function filterDto()
    {
        $model = new EmployeesFilterHolder();
        $data = $this->request->all();
        /*
                if (!is_null($this->user->region)) {
                    $data['region_id'] = $this->user->region->region_id;
                }*/

        $model->load($data);

        return $model;
    }


}
