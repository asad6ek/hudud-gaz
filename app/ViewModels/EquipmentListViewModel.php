<?php
namespace App\ViewModels;



use App\Filters\Holders\EquipmentFilterHolder;

use App\Services\CRUD\BaseCrudService;

use App\Services\CRUD\EquipmentService;
use Illuminate\Http\Request;
use Spatie\ViewModels\ViewModel;


class EquipmentListViewModel extends ViewModel
{
    /**
     * @var BaseCrudService
     */
    private $service;

    /**
     * @var Request
     */
    private $request;
    private  $user;

    public function __construct(EquipmentService $service, Request $request, ?\Illuminate\Contracts\Auth\Authenticatable $user)
    {
        $this->request = $request;
        $this->service = $service;
        $this->user = $user;
    }

    public function models()
    {
        return $this->service->list($this->filterDto());
    }
    public function filterDto()
    {
        $model = new EquipmentFilterHolder();
        $data = $this->request->all();
        /*
                if (!is_null($this->user->region)) {
                    $data['region_id'] = $this->user->region->region_id;
                }*/

        $model->load($data);

        return $model;
    }

}
