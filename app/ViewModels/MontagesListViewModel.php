<?php


namespace App\ViewModels;


use App\Services\CRUD\MontagesService;
use Spatie\ViewModels\ViewModel;

class MontagesListViewModel extends ViewModel
{

    private $service;

    public function __construct(MontagesService $service){
        $this->service = $service;
    }

    public function models(){

    }
}
