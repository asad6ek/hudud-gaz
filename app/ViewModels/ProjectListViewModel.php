<?php
namespace App\ViewModels;


use App\Filters\Holders\DesignerFilterHolder;
use App\Models\Proposition;
use App\Models\TechCondition;
use App\Services\CRUD\BaseCrudService;

use App\Services\CRUD\ProjectService;
use Illuminate\Http\Request;
use Spatie\ViewModels\ViewModel;


class ProjectListViewModel extends ViewModel
{
    /**
     * @var
     */
    private $service;

    /**
     * @var Request
     */
    private $request;
    private  $user;

    public function __construct(ProjectService $service, Request $request, ?\Illuminate\Contracts\Auth\Authenticatable $user)
    {
        $this->request = $request;
        $this->service = $service;
        $this->user = $user;
    }

    public function models()
    {
        return TechCondition::query()->with('proposition.applicantable')->where('designer_id',$this->user->id);
    }
    public function filterDto()
    {
        $model = new DesignerFilterHolder();
        $data = $this->request->all();
        /*
                if (!is_null($this->user->region)) {
                    $data['region_id'] = $this->user->region->region_id;
                }*/
        $model->load($data);

        return $model;
    }
    public function cancels()
    {
        return Proposition::query()->with('project')->where('status', 14)->get();
    }
}
