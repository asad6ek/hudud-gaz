<?php


namespace App\ViewModels;


use App\Filters\Holders\PropositionFilterHolder;
use App\Models\User;
use App\Services\CRUD\BaseCrudService;
use App\Services\CRUD\PropositionService;
use Spatie\ViewModels\ViewModel;

class PropositionListViewModel extends ViewModel
{

    /**
     * @var BaseCrudService
     */
    private $service;

    /**
     * @var User
     */
    private $user;

    public function __construct(PropositionService $service, $user)
    {
        $this->service = $service;
        $this->user = $user;
    }

    public function models()
    {
        return $this->service->filterData($this->filterDto())->with('getStatus', 'region')->get();
    }


    public function filterDto()
    {
        $model = new PropositionFilterHolder();

        $data['status'] = [1, 2, 3];
        if ($this->user->role_id == 6) {
            $data =
                [
                    'status' => [1, 2],
                    'region_id' => $this->user->firm_id
                ];
        }
        $model->load($data);

        return $model;
    }
}
