<?php


namespace App\ViewModels;


use App\Filters\Holders\RecommendationFilterHolder;
use App\Models\Proposition;
use App\Models\Recommendation;
use App\Models\User;
use App\Services\CRUD\RecommendationService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\ViewModels\ViewModel;

class RecommendationListViewModel extends ViewModel
{
    /**
     * @var Request
     */
    private $request;
    /**
     * @var RecommendationService
     */
    private $service;
    /**
     * @var User
     */
    private $user;

    public function __construct(RecommendationService $service, Request $request, $user)
    {
        $this->service = $service;
        $this->request = $request;
        $this->user = $user;
    }

    public function models()
    {
        if (user_role('region')) {
            return Proposition::query()->with(['recommendation' => function ($query) {
                $query->select('id', 'path', 'created_at', 'prop_id');
            }, 'region'])->where('status', 3)->where('region_id', $this->user->firm_id)->get();
        }

        return Proposition::query()->with('recommendation')->whereIn('status', [4, 5, 7])->get();
    }


    public function cancels()
    {
        return Proposition::query()->with('recommendation')->where('status', 6)->get();
    }
}
