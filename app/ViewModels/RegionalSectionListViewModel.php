<?php


namespace App\ViewModels;


use App\Models\Activity;
use App\Services\CRUD\RecommendationService;
use App\Services\CRUD\SectionsService;
use Spatie\ViewModels\ViewModel;

class RegionalSectionListViewModel extends ViewModel
{

    /**
     * @var RecommendationService
     */
    private $service;

    public function __construct(SectionsService $service)
    {
        $this->service = $service;
    }

    public function models()
    {
        return  $this->service->filterData()->get();
    }

    public function activities()
    {
        return Activity::query()->pluck('activity_name', 'id');
    }

    public function statuses()
    {
        return \App\Models\Status::all();
    }

}
