<?php


namespace App\ViewModels;


use App\Filters\Holders\PropositionFilterHolder;
use App\Services\CRUD\PropositionService;
use Spatie\ViewModels\ViewModel;

class ReportListViewModel extends ViewModel
{
    /**
     * @var PropositionService
     */
    private $service;

    public function __construct($service, $proposition = null)
    {
        $this->service = $service;
        $this->proposition = $proposition;
    }

    public function models()
    {
        $model = new PropositionFilterHolder();
        $model->load([
            'status' => [22],
        ]);
        return $this->service->filterData($model)->get();
    }
}
