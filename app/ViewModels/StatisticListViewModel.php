<?php


namespace App\ViewModels;


use App\Models\Activity;
use App\Models\Proposition;
use App\Services\CRUD\StatisticService;
use Illuminate\Support\Facades\DB;
use Spatie\ViewModels\ViewModel;

class StatisticListViewModel extends ViewModel
{

    /**
     * @var StatisticService
     */
    private $service;

    public function __construct(StatisticService $service)
    {
        $this->service = $service;
    }

    public function all()
    {
        return Proposition::query()->whereYear('created_at', today()->year)->count();
    }

    public function success()
    {
        return Proposition::query()->where('status', '>', 9)->where('status', '<>', 10)->count();
    }

    public function process()
    {
        return Proposition::query()->whereNotIn('status', [9, 10, 20, 21])->count();
    }

    public function fail()
    {
        return Proposition::query()->where('status', 10)->count();
    }

    public function countsByType()
    {
        return Proposition::query()->select('activity_type', DB::raw('count(*) as count'))->with('activityName')
            ->groupBy('activity_type')->get()->groupBy('activity_type')->toArray();

//        dd(Proposition::query()->select(DB::raw('count(id) as count'), DB::raw('activity_type'))
//            ->groupBy('activity_type')->get()->groupBy('activity_type'));
//        return Proposition::query()->select(DB::raw('count(id) as count'), DB::raw('activity_type'))
//            ->groupBy('activity_type')->get();
    }

    public function points()
    {
        $array = Proposition::query()//->where('status', 21)
            ->select(DB::raw('count(id) as count'), DB::raw('MONTH(created_at) as month'))
            ->groupby('month')
            ->get()->groupBy('month')->toArray();

        $points = array_fill(1, 12, 0);
        foreach ($array as $key => $item) $points[$key] = $item[0]['count'];
//        dd($points);

        return $points;
    }

    public function props_group() {
        return Proposition::query()->get('activity_type')->groupBy('activity_type');
    }

    public function activities() {
        return Activity::query()->pluck('activity_name', 'id');
    }
}
