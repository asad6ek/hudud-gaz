<?php


namespace App\ViewModels;


use App\Services\CRUD\StatusService;
use Illuminate\Http\Request;
use Spatie\ViewModels\ViewModel;

class StatusListViewModel extends ViewModel
{

    /**
     * @var Request
     */
    private $request;
    /**
     * @var StatusService
     */
    private $service;

    public function __construct(StatusService $service, Request $request)
    {
        $this->request = $request;
        $this->service = $service;
    }

    public function models()
    {
        return $this->service->all();
    }
}
