<?php


namespace App\ViewModels;



use App\Filters\Holders\TechConditionFilterHolder;
use App\Models\Proposition;
use Illuminate\Http\Request;
use Spatie\ViewModels\ViewModel;
use Illuminate\Support\Facades\Auth;
use App\Services\CRUD\TechConditionService;

class TechConditionListViewModel extends ViewModel
{
    /**
     * @var Request
     */
    private $request;
    /**
     * @var TechConditionService
     */
    private $service;
    /**
     * @var Auth
     */
    private $user;

    public function __construct(TechConditionService $service, Request $request, $user)
    {
        $this->service = $service;
        $this->request = $request;
        $this->user = $user;
    }

    public function models()
    {
        return Proposition::with('tech_condition')->where('status', 8)->get();
    }

    public function filterDto()
    {
        $model = new TechConditionFilterHolder();
        $data = $this->request->all();

//        dd($this->user->region);
        if (!is_null($this->user->region)) {
            $data['region_id'] = $this->user->region->region_id;
        }
        $model->load($data);
        return $model;
    }

}
