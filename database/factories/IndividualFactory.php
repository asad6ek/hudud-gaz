<?php

namespace Database\Factories;

use App\Models\Individual;
use Illuminate\Database\Eloquent\Factories\Factory;

class IndividualFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Individual::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'full_name' => $this->faker->name('male'),
            'tel_number' => $this->faker->randomNumber(7, true),
            'pass_id' => $this->faker->numerify('AA#######'),
            'tin' => $this->faker->randomNumber(7, true)
        ];
    }
}
