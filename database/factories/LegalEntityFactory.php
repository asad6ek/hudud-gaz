<?php

namespace Database\Factories;

use App\Models\LegalEntity;
use Illuminate\Database\Eloquent\Factories\Factory;

class LegalEntityFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = LegalEntity::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'legal_entity_tin' => $this->faker->randomNumber(7),
            'legal_entity_name' => $this->faker->catchPhrase,
            'email' => $this->faker->safeEmail,
            'leader_name' => $this->faker->name('male'),
            'leader_tin' => $this->faker->randomNumber(6, true),
            'tel_number' => $this->faker->randomNumber(9, true)
        ];
    }
}
