<?php

namespace Database\Factories;

use App\Models\MontageFirm;
use Illuminate\Database\Eloquent\Factories\Factory;

class MontageFirmFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = MontageFirm::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'full_name' => $this->faker->catchPhrase,
            'shortname' => $this->faker->companySuffix,
            'record_number' => $this->faker->randomNumber(3, true),
            'taxpayer_stir' => $this->faker->randomNumber(7, true),
            'legal_form' => $this->faker->text,
            'leader' => $this->faker->name,
            'region' => rand(1, 3),
            'address' => $this->faker->address,
            'reg_number' => $this->faker->randomNumber(4, true),
            'given_by' => $this->faker->firstName,
            'date_created' => now()->subMonth(),
            'date_expired' => now()->addYear(),
            'permission_to' => $this->faker->paragraph,
            'implement_for' => $this->faker->paragraph
        ];
    }
}
