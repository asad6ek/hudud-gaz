<?php

namespace Database\Factories;

use App\Models\Individual;
use App\Models\LegalEntity;
use App\Models\Proposition;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class PropositionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Proposition::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */

    public function definition()
    {
        $applicant = [
            Individual::class,
            LegalEntity::class,
        ];
        $applicantType = $this->faker->randomElement($applicant);
        $applicant = $applicantType::factory()->create();
        return [
            'applicantable_id' => $applicant->id,
            'applicantable_type' => $applicantType,
            'number' => mt_rand(100000, 800000),
            'path' => Str::random(10),
            'region_id' => rand(1, 3),
            'building_type' => mt_rand(1, 2),
            'activity_type' => mt_rand(1, 2),
            'minutes' => rand(10, 60)
        ];
    }
}
