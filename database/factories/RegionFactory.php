<?php

namespace Database\Factories;

use App\Models\Region;
use Illuminate\Database\Eloquent\Factories\Factory;

class RegionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Region::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'org_number' => $this->faker->randomNumber(7, true),
            'lead_engineer' => $this->faker->name('male'),
            'section_leader' => $this->faker->titleMale . $this->faker->name('male'),
            'region_name' => mt_rand(1,13),
            'org_name' => $this->faker->company,
            'address_latin' => $this->faker->address,
            'address' => $this->faker->address,
            'email' => $this->faker->freeEmail,
            'phone' => $this->faker->randomNumber(7, true),
            'fax' => '+' . $this->faker->phoneNumber
        ];
    }
}
