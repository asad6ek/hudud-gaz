<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecommendationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recommendations', function (Blueprint $table) {
            $table->id();
            $table->string('path')->nullable();
            $table->unsignedBigInteger('prop_id')->unique()->index();
            $table->string('address')->nullable();
            $table->string('access_point')->nullable();
            $table->string('pipe_size')->nullable();
            $table->decimal('under_len')->nullable();
            $table->decimal('above_len')->nullable();
            $table->string('avg_winter')->nullable();
            $table->string('avg_summer')->nullable();
            $table->decimal('gas_consumption')->nullable();
            $table->text('additional')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('prop_id')
                ->references('id')
                ->on('propositions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recommendations');
    }
}
