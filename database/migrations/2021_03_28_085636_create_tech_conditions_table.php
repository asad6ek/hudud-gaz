<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTechConditionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tech_conditions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('prop_id')->index();
            $table->string('path');
            $table->string('qrcode')->nullable()->unique();
            $table->bigInteger('designer_id')->unsigned()->nullable();
            $table->bigInteger('firm_id')->unsigned()->nullable();
            $table->softDeletes();
            $table->timestamps();

           $table->foreign('prop_id')
                ->references('id')
                ->on('propositions');


        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tech_conditions');
    }
}
