<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTechEquipmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tech_equipments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('prop_id')->index();
            $table->unsignedBigInteger('type_id')->index();
            $table->integer('number')->default(0);
            $table->string('note')->nullable();
            $table->timestamps();

            $table->foreign('prop_id')->references('id')->on('propositions');
//            $table->foreign('type_id')->references('id')->on('equipments_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tech_equipments');
    }
}
