<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDescriptionToRecommendationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recommendations', function (Blueprint $table) {
            $table->integer('depth')->after('pipe_size')->nullable();
            $table->string('description')->after('gas_consumption')->nullable();
            $table->text('comment')->after('additional')->nullable();
            $table->tinyInteger('status')->after('comment')->default(0);
            $table->string('grc')->after('depth')->nullable();
            $table->decimal('capacity')->nullable()->after('avg_summer');
            $table->decimal('real_capacity')->nullable()->after('capacity');
            $table->json('equipments')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recommendations', function (Blueprint $table) {
            $table->dropColumn('depth');
            $table->dropColumn('grc');
            $table->dropColumn('description');
            $table->dropColumn('comment');
            $table->dropColumn('status');
        });
    }
}
