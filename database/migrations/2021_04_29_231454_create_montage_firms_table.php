<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMontageFirmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('montage_firms', function (Blueprint $table) {
            $table->id();
            $table->string('full_name')->nullable();
            $table->string('shortname');
            $table->string('record_number');
            $table->integer('taxpayer_stir');
            $table->string('legal_form');
            $table->string('leader');
            $table->integer('region');
            $table->string('address');
            $table->string('reg_number');
            $table->string('given_by')->nullable();
            $table->date('date_created');
            $table->date('date_expired');
            $table->string('permission_to', 500);
            $table->string('implement_for', 500);
            $table->string('path')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('montage_firms');
    }
}
