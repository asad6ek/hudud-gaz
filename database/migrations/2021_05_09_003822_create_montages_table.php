<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMontagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('montages', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('tech_id')->unsigned()->index();
            $table->string('path');
            $table->string('comment')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('tech_id')
                ->references('id')
                ->on('tech_conditions')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('montages');
    }
}
