<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddJsonColumnToRecommendationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recommendations', function (Blueprint $table) {
            $table->string('pipe_size2')->after('pipe_size')->nullable();
            $table->longText('gas_meter')->after('comment')->nullable();
            $table->string('gas_network')->after('pipe_size')->nullable();
            $table->tinyInteger('pipe_type')->after('gas_network')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recommendations', function (Blueprint $table) {
            $table->dropColumn('pipe_size2');
            $table->dropColumn('gas_meter');
        });
    }
}
