<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use MenusTableSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        MenuItemsSeeder::class;
        MenusTableSeeder::class;
        // \App\Models\User::factory(10)->create();
    }
}
