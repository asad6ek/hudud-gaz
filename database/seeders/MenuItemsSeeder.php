<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Menu;
use TCG\Voyager\Models\MenuItem;

class MenuItemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu = Menu::where('name', 'admin')->firstOrFail();

        $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menu->id,
            'title'   => 'Arizalar',
            'url'     => '',
            'route'   => 'admin.proposition.index',
        ]);
        if (!$menuItem->exists) {
            $menuItem->fill([
                'target'     => '_self',
                'icon_class' => 'voyager-settings',
                'color'      => null,
                'parent_id'  => null,
                'order'      => 17,
            ])->save();
        }

        $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menu->id,
            'title'   => 'Tavsiyanomalar',
            'url'     => '',
            'route'   => 'admin.recommendations.index',
        ]);
        if (!$menuItem->exists) {
            $menuItem->fill([
                'target'     => '_self',
                'icon_class' => 'voyager-settings',
                'color'      => null,
                'parent_id'  => null,
                'order'      => 18,
            ])->save();
        }
        $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menu->id,
            'title'   => 'Hisobotlar',
            'url'     => '',
            'route'   => 'admin.reports.index',
        ]);
        if (!$menuItem->exists) {
            $menuItem->fill([
                'target'     => '_self',
                'icon_class' => 'voyager-settings',
                'color'      => null,
                'parent_id'  => null,
                'order'      => 18,
            ])->save();
        }

        $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menu->id,
            'title'   => 'Viloyat kesimi',
            'url'     => '',
            'route'   => 'admin.regional_section.index',
        ]);
        if (!$menuItem->exists) {
            $menuItem->fill([
                'target'     => '_self',
                'icon_class' => 'voyager-settings',
                'color'      => null,
                'parent_id'  => 16,
                'order'      => 19,
            ])->save();
        }
        $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menu->id,
            'title'   => 'Tumanlar kesimi',
            'url'     => '',
            'route'   => 'admin.district_section.index',
        ]);
        if (!$menuItem->exists) {
            $menuItem->fill([
                'target'     => '_self',
                'icon_class' => 'voyager-settings',
                'color'      => null,
                'parent_id'  => 16,
                'order'      => 20,
            ])->save();
        }
        $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menu->id,
            'title'   => 'Batafsil',
            'url'     => '',
            'route'   => 'admin.more.index',
        ]);
        if (!$menuItem->exists) {
            $menuItem->fill([
                'target'     => '_self',
                'icon_class' => 'voyager-settings',
                'color'      => null,
                'parent_id'  => 16,
                'order'      => 21,
            ])->save();
        }

        $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menu->id,
            'title'   => 'Arxiv',
            'url'     => '',
            'route'   => 'admin.archive.index',
        ]);
        if (!$menuItem->exists) {
            $menuItem->fill([
                'target'     => '_self',
                'icon_class' => 'voyager-settings',
                'color'      => null,
                'parent_id'  => null,
                'order'      => 22,
            ])->save();
        }
        $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menu->id,
            'title'   => 'Arizalar',
            'url'     => '',
            'route'   => 'admin.propositions.index',
        ]);
        if (!$menuItem->exists) {
            $menuItem->fill([
                'target'     => '_self',
                'icon_class' => 'voyager-settings',
                'color'      => null,
                'parent_id'  => 20,
                'order'      => 22,
            ])->save();
        }

        $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menu->id,
            'title'   => 'Tavsiyanomalar',
            'url'     => '',
            'route'   => 'admin.archive_recommendation.index',
        ]);
        if (!$menuItem->exists) {
            $menuItem->fill([
                'target'     => '_self',
                'icon_class' => 'voyager-settings',
                'color'      => null,
                'parent_id'  => 20,
                'order'      => 23,
            ])->save();
        }
        $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menu->id,
            'title'   => 'Texnik shartlar',
            'url'     => '',
            'route'   => 'admin.archive_tech_condition.index',
        ]);
        if (!$menuItem->exists) {
            $menuItem->fill([
                'target'     => '_self',
                'icon_class' => 'voyager-settings',
                'color'      => null,
                'parent_id'  => 20,
                'order'      => 24,
            ])->save();
        }
        /*==========================================
         * Engineer menus
         */
        $menu_engineer = Menu::where('name', 'engineer')->firstOrFail();

        $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menu_engineer->id,
            'title'   => 'Loyihalar',
            'url'     => '',
            'route'   => 'admin.engineer_projects.index',
        ]);
        if (!$menuItem->exists) {
            $menuItem->fill([
                'target'     => '_self',
                'icon_class' => 'voyager-settings',
                'color'      => null,
                'parent_id'  => null,
                'order'      => 25,
            ])->save();
        }
        $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menu_engineer->id,
            'title'   => 'Montajlar',
            'url'     => '',
            'route'   => 'admin.engineer_montages.index',
        ]);
        if (!$menuItem->exists) {
            $menuItem->fill([
                'target'     => '_self',
                'icon_class' => 'voyager-settings',
                'color'      => null,
                'parent_id'  => null,
                'order'      => 26,
            ])->save();
        }

        $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menu_engineer->id,
            'title'   => 'Hisobotlar',
            'url'     => '',
            'route'   => 'admin.engineer_reports.index',
        ]);
        if (!$menuItem->exists) {
            $menuItem->fill([
                'target'     => '_self',
                'icon_class' => 'voyager-settings',
                'color'      => null,
                'parent_id'  => null,
                'order'      => 27,
            ])->save();
        }


        $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menu_engineer->id,
            'title'   => 'Arxiv',
            'url'     => '',
            'route'   => 'admin.engineer_archive.index',
        ]);
        if (!$menuItem->exists) {
            $menuItem->fill([
                'target'     => '_self',
                'icon_class' => 'voyager-settings',
                'color'      => null,
                'parent_id'  => null,
                'order'      => 28,
            ])->save();
        }

        $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menu_engineer->id,
            'title'   => 'Arxiv',
            'url'     => '',
            'route'   => 'admin.engineer_archive.index',
        ]);
        if (!$menuItem->exists) {
            $menuItem->fill([
                'target'     => '_self',
                'icon_class' => 'voyager-settings',
                'color'      => null,
                'parent_id'  => null,
                'order'      => 29,
            ])->save();
        }

        $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menu_engineer->id,
            'title'   => 'Loyihalar',
            'url'     => '',
            'route'   => 'admin.engineer_archive_projects.index',
        ]);
        if (!$menuItem->exists) {
            $menuItem->fill([
                'target'     => '_self',
                'icon_class' => 'voyager-settings',
                'color'      => null,
                'parent_id'  => 28,
                'order'      => 30,
            ])->save();
        }

        /*===================
        District menus
        */
        $menu_districts = Menu::where('name', 'districts')->firstOrFail();

        $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menu_districts->id,
            'title'   => 'Arizalar',
            'url'     => '',
            'route'   => 'admin.districts_propositions.index',
        ]);
        if (!$menuItem->exists) {
            $menuItem->fill([
                'target'     => '_self',
                'icon_class' => 'voyager-settings',
                'color'      => null,
                'parent_id'  => null,
                'order'      => 31,
            ])->save();
        }

        /*============
        Admin menu items create
        */

        $menu_administrator = Menu::where('name', 'administrator')->firstOrFail();

        $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menu_administrator->id,
            'title'   => 'Hodimlar',
            'url'     => '',
            'route'   => 'admin.employee.index',
        ]);
        if (!$menuItem->exists) {
            $menuItem->fill([
                'target'     => '_self',
                'icon_class' => 'voyager-settings',
                'color'      => null,
                'parent_id'  => null,
                'order'      => 32,
            ])->save();
        }
        $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menu_administrator->id,
            'title'   => 'Jihozlar',
            'url'     => '',
            'route'   => 'admin.equipments.index',
        ]);
        if (!$menuItem->exists) {
            $menuItem->fill([
                'target'     => '_self',
                'icon_class' => 'voyager-settings',
                'color'      => null,
                'parent_id'  => null,
                'order'      => 33,
            ])->save();
        }
        $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menu_administrator->id,
            'title'   => 'Tumanlar',
            'url'     => '',
            'route'   => 'admin.regions.index',
        ]);
        if (!$menuItem->exists) {
            $menuItem->fill([
                'target'     => '_self',
                'icon_class' => 'voyager-settings',
                'color'      => null,
                'parent_id'  => null,
                'order'      => 33,
            ])->save();
        }
        $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menu_administrator->id,
            'title'   => 'Loyihachilar',
            'url'     => '',
            'route'   => 'admin.designers.index',
        ]);
        if (!$menuItem->exists) {
            $menuItem->fill([
                'target'     => '_self',
                'icon_class' => 'voyager-settings',
                'color'      => null,
                'parent_id'  => null,
                'order'      => 34,
            ])->save();
        }
        $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menu_administrator->id,
            'title'   => 'Montajchilar',
            'url'     => '',
            'route'   => 'admin.montage_firms.index',
        ]);
        if (!$menuItem->exists) {
            $menuItem->fill([
                'target'     => '_self',
                'icon_class' => 'voyager-settings',
                'color'      => null,
                'parent_id'  => null,
                'order'      => 35,
            ])->save();
        }


    }
}
