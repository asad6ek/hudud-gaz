<?php


use App\Models\Activity;
use Illuminate\Database\Seeder;
use TCG\Voyager\Traits\Seedable;

class ActivitySeeder extends Seeder
{
    use Seedable;
    protected $seedersPath;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Activity::query()->firstOrCreate(['activity_name' => 'Аҳоли хонадонлари']);
        Activity::query()->firstOrCreate(['activity_name' => 'Оилавий тадбиркорлик']);
        Activity::query()->firstOrCreate(['activity_name' => 'Хусусий бизнес']);
        Activity::query()->firstOrCreate(['activity_name' => 'Якка тадбиркор']);
    }
}
