<?php

use App\Models\Designer;
use App\Models\Equipment;
use App\Models\MontageFirm;
use App\Models\Region;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\File;

class FakeDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Equipment::query()->firstOrCreate(['name' => 'Газ ҳисоблагич']);

        Cache::put('general_settings', [
            "shareholder_name" => "Худуд Газ Таъминот",
            "branch_name" => "ХудудГазХоразм",
            "engineer_name" => "Engineer",
            "helper_name" => "Helper",
            "phone_number" => "+998991234567",
            "reg_num" => "777",
            "address_latin" => "Urgench city",
            "address" => "Urganch city",
            "email_address" => "email@hududgaz.uz",
            "fax" => "011102220",
        ]);


        $path =public_path('storage/propositions');
        if (!file_exists($path)) {
            File::makeDirectory($path, $mode = 0777, true, true);
        }

        $path =public_path('storage/recommendations');
        if (!file_exists($path)) {
            File::makeDirectory($path, $mode = 0777, true, true);
        }

        $path =public_path('storage/tech_conditions');
        if (!file_exists($path)) {
            File::makeDirectory($path, $mode = 0777, true, true);
        }

        $path =public_path('storage/projects');
        if (!file_exists($path)) {
            File::makeDirectory($path, $mode = 0777, true, true);
        }

        $path =public_path('storage/montages');
        if (!file_exists($path)) {
            File::makeDirectory($path, $mode = 0777, true, true);
        }

        $path = public_path('storage/licenses');
        if (!file_exists($path)) {
            File::makeDirectory($path, $mode = 0777, true, true);
        }

        $path =public_path('storage/designer');
        if (!file_exists($path)) {
            File::makeDirectory($path, $mode = 0777, true, true);
        }

        $path =public_path('storage/montage_firms');
        if (!file_exists($path)) {
            File::makeDirectory($path, $mode = 0777, true, true);
        }
    }
}
