<?php

use App\Models\Equipment;
use App\Models\EquipmentType;
use Illuminate\Database\Seeder;

class HelperSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $this->insertEquipments();
        $this->insertEquipmentTypes();
    }

    private function insertEquipments() {
        foreach (['Gaz qazoni', "Jo\u{2018}mrak", 'Quvur'] as $equip) {
            Equipment::query()->create([
                'name' => $equip
            ]);
        }
    }

    private function insertEquipmentTypes() {
        foreach (['D11', "D12"] as $type) {
            EquipmentType::query()->create([
                'type' => $type,
                'equipment_id' => 1,
                'order' => 1
            ]);
        }
    }
}
