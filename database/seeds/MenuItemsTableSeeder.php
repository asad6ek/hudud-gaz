<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Menu;
use TCG\Voyager\Models\MenuItem;

class MenuItemsTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @param $menu_id
     * @param $title
     * @param $route
     * @param string $url
     * @return MenuItem
     */

    protected function creator($menu_id, $title, $route = '', $url = ''): MenuItem
    {
        return MenuItem::firstOrNew([
            'menu_id' => $menu_id,
            'title' => $title,
            'url' => $url,
            'route' => $route,
        ]);
    }

    public function filler($item, $icon_class, $order, $parent_id = null, $color = null, $target = '_self')
    {
        $item->fill([
            'target' => $target,
            'icon_class' => $icon_class,
            'color' => $color,
            'parent_id' => $parent_id,
            'order' => $order,
        ])->save();
    }

    public function run()
    {

        /*============
            Admin menu items create
        */

        $menu = Menu::where('name', 'admin')->firstOrFail();

        $menuItem = $this->creator($menu->id, __('voyager::seeders.menu_items.roles'), 'voyager.roles.index');
        if (!$menuItem->exists) $this->filler($menuItem, 'voyager-lock', 2);

        $menuItem = $this->creator($menu->id, __('voyager::seeders.menu_items.users'), 'voyager.users.index');
        if (!$menuItem->exists) $this->filler($menuItem, 'voyager-person', 3);

        $toolsMenuItem = $this->creator($menu->id, __('voyager::seeders.menu_items.tools'));
        if (!$toolsMenuItem->exists) $this->filler($toolsMenuItem, 'voyager-tools', 5);

        $menuItem = $this->creator($menu->id, __('voyager::seeders.menu_items.menu_builder'), 'voyager.menus.index');
        if (!$menuItem->exists) $this->filler($menuItem, 'voyager-list', 6, $toolsMenuItem->id);

        $menuItem = $this->creator($menu->id, __('voyager::seeders.menu_items.database'), 'voyager.database.index');
        if (!$menuItem->exists) $this->filler($menuItem, 'voyager-data', 7, $toolsMenuItem->id);

        $menuItem = $this->creator($menu->id, __('voyager::seeders.menu_items.bread'), 'voyager.bread.index');
        if (!$menuItem->exists) $this->filler($menuItem, 'voyager-bread', 9, $toolsMenuItem->id);

        $menuItem = $this->creator($menu->id, __('voyager::seeders.menu_items.settings'), 'voyager.settings.index');
        if (!$menuItem->exists) $this->filler($menuItem, 'voyager-settings', 10);

        /*============
            Technic menu items create
        */

        $menu = Menu::where('name', 'technic')->firstOrFail();

        $menuItem = $this->creator($menu->id, 'menu_items.dashboard', 'technic.dashboard');
        if (!$menuItem->exists) $this->filler($menuItem, 'nav-icon fas fa-tachometer-alt', 10);

        $menuItem = $this->creator($menu->id, 'menu_items.propositions', 'admin.propositions.index');
        if (!$menuItem->exists) $this->filler($menuItem, 'nav-icon fas fa-file-alt', 11);

        $menuItem = $this->creator($menu->id, 'menu_items.recommendations', 'admin.recommendations.index');
        if (!$menuItem->exists) $this->filler($menuItem, 'nav-icon fas fa-paste', 12);

        $menuItem = $this->creator($menu->id, 'menu_items.tech_conditions', 'admin.tech_conditions.index');
        if (!$menuItem->exists) $this->filler($menuItem, 'nav-icon fas fa-paste', 36);

        $reportsMenuItem = $this->creator($menu->id, 'menu_items.reports', 'admin.reports.index');
        if (!$reportsMenuItem->exists) $this->filler($reportsMenuItem, 'nav-icon fas fa-chart-line', 13);

        $menuItem = $this->creator($menu->id, 'menu_items.regions', 'admin.regional_section.index');
        if (!$menuItem->exists) $this->filler($menuItem, 'voyager-settings', 14, $reportsMenuItem->id);

        $menuItem = $this->creator($menu->id, 'menu_items.district', 'admin.district_section.index');
        if (!$menuItem->exists) $this->filler($menuItem, 'voyager-settings', 15, $reportsMenuItem->id);

        $menuItem = $this->creator($menu->id, 'menu_items.more', 'admin.more.index');
        if (!$menuItem->exists) $this->filler($menuItem, 'voyager-settings', 16, $reportsMenuItem->id);



        /*==========================================
           Director menus
        */


        $menu_director = Menu::where('name', 'director')->firstOrFail();

        $menuItem = $this->creator($menu_director->id, 'menu_items.dashboard', 'admin.statistics.index');
        if (!$menuItem->exists) $this->filler($menuItem, 'nav-icon fas fa-tachometer-alt', 21);

        $menuItem = $this->creator($menu_director->id, 'menu_items.users', 'admin.employees.index');
        if (!$menuItem->exists) $this->filler($menuItem, 'nav-icon fas fa-users', 24);

        $menuItem = $this->creator($menu_director->id, 'menu_items.designers', 'admin.designers.index');
        if (!$menuItem->exists) $this->filler($menuItem, 'nav-icon fas fa-pencil-ruler', 24);

        $menuItem = $this->creator($menu_director->id, 'menu_items.montage_firms', 'admin.montage_firms.index');
        if (!$menuItem->exists) $this->filler($menuItem, 'nav-icon fas fa-network-wired', 24);

        $documentsMenuItem = $this->creator($menu_director->id, 'menu_items.documents', '#');
        if (!$documentsMenuItem->exists) $this->filler($documentsMenuItem, 'nav-icon fas fa-folder-open', 22);

        $menuItem = $this->creator($menu_director->id, 'menu_items.propositions', 'admin.propositions.index');
        if (!$menuItem->exists) $this->filler($menuItem, 'voyager-settings', 23, $documentsMenuItem->id);

        $menuItem = $this->creator($menu_director->id, 'menu_items.recommendations', 'admin.recommendations.index');
        if (!$menuItem->exists) $this->filler($menuItem, 'voyager-settings', 23, $documentsMenuItem->id);

        $menuItem = $this->creator($menu_director->id, 'menu_items.tech_conditions', 'admin.tech_conditions.index');
        if (!$menuItem->exists) $this->filler($menuItem, 'voyager-settings', 23, $documentsMenuItem->id);

        $menuItem = $this->creator($menu_director->id, 'menu_items.projects', 'admin.projects.index');
        if (!$menuItem->exists) $this->filler($menuItem, 'voyager-settings', 23, $documentsMenuItem->id);

        $menuItem = $this->creator($menu_director->id, 'menu_items.montages', 'admin.montages.index');
        if (!$menuItem->exists) $this->filler($menuItem, 'voyager-settings', 23, $documentsMenuItem->id);

        $menuItem = $this->creator($menu_director->id, 'menu_items.licenses', 'admin.license.index');
        if (!$menuItem->exists) $this->filler($menuItem, 'voyager-settings', 24, $documentsMenuItem->id);

        $menuItem = $this->creator($menu_director->id, 'menu_items.total_report', 'admin.more.index');
        if (!$menuItem->exists) $this->filler($menuItem, 'nav-icon fas fa-copy', 24);


        /*==========================================
            Engineer menus
         */


        $menu_engineer = Menu::where('name', 'engineer')->firstOrFail();

        $menuItem = $this->creator($menu_engineer->id, 'menu_items.projects', 'admin.projects.process');
        if (!$menuItem->exists) $this->filler($menuItem, 'nav-icon fas fa-pencil-ruler', 21);

        $menuItem = $this->creator($menu_engineer->id, 'menu_items.montages', 'admin.montage_firms.process');
        if (!$menuItem->exists) $this->filler($menuItem, 'nav-icon fas fa-network-wired', 22);

        $menuItem = $this->creator($menu_engineer->id, 'menu_items.licenses', 'admin.license.index');
        if (!$menuItem->exists) $this->filler($menuItem, 'nav-icon fas fa-copy', 23);

        $menuItem = $this->creator($menu_engineer->id, 'menu_items.reports', 'admin.reports');
        if (!$menuItem->exists) $this->filler($menuItem, 'nav-icon fas fa-chart-bar', 24);



        $archiveMenuItem = $this->creator($menu_engineer->id, 'menu_items.archive');
        if (!$archiveMenuItem->exists) $this->filler($archiveMenuItem, 'nav-icon fas fa-box-open', 25);

        $menuItem = $this->creator($menu_engineer->id, 'menu_items.projects', 'admin.projects.archive');
        if (!$menuItem->exists) $this->filler($menuItem, 'nav-icon fas fa-pencil-ruler', 26, $archiveMenuItem->id);

        $menuItem = $this->creator($menu_engineer->id, 'menu_items.montages', 'admin.montage_firms.archive');
        if (!$menuItem->exists) $this->filler($menuItem, 'nav-icon fas fa-pencil-ruler', 27, $archiveMenuItem->id);


        /*===================
            Region menus
        */

        $menu_region = Menu::where('name', 'region')->firstOrFail();

        $menuItem = $this->creator($menu_region->id, 'menu_items.propositions', 'admin.propositions.index');
        if (!$menuItem->exists) $this->filler($menuItem, 'badge-primary', 27);

        $menuItem = $this->creator($menu_region->id, 'menu_items.recommendations', 'admin.recommendations.index');
        if (!$menuItem->exists) $this->filler($menuItem, 'badge-info', 28);

        $menuItem = $this->creator($menu_region->id, 'menu_items.process', 'admin.recommendations.process');
        if (!$menuItem->exists) $this->filler($menuItem, 'badge-success', 28);

        $menuItem = $this->creator($menu_region->id, 'menu_items.cancelled', 'admin.recommendations.cancelled');
        if (!$menuItem->exists) $this->filler($menuItem, 'badge-danger', 29);

        $menuItem = $this->creator($menu_region->id, 'menu_items.archive', 'admin.recommendations.archive');
        if (!$menuItem->exists) $this->filler($menuItem, 'badge-secondary', 30);


        /*============
            Administrator menu items create
        */

        $menu_administrator = Menu::where('name', 'administrator')->firstOrFail();

        $menuItem = $this->creator($menu_administrator->id, 'menu_items.employees', 'admin.employees.index');
        if (!$menuItem->exists) $this->filler($menuItem, 'nav-icon fas fa-users', 31);

        $menuItem = $this->creator($menu_administrator->id, 'menu_items.equipments', 'admin.equipments.index');
        if (!$menuItem->exists) $this->filler($menuItem, 'nav-icon fas fa-drafting-compass', 32);

        $menuItem = $this->creator($menu_administrator->id, 'menu_items.designers', 'admin.designers.index');
        if (!$menuItem->exists) $this->filler($menuItem, 'nav-icon fas fa-pencil-ruler', 33);

        $menuItem = $this->creator($menu_administrator->id, 'menu_items.montage_firms', 'admin.montage_firms.index');
        if (!$menuItem->exists) $this->filler($menuItem, 'nav-icon fas fa-network-wired', 34);

        $actionsMenuItem = $this->creator($menu_administrator->id, 'menu_items.config', '#');
        if (!$actionsMenuItem->exists) $this->filler($actionsMenuItem, 'nav-icon fas fa-chart-line', 35);

        $menuItem = $this->creator($menu_administrator->id, 'menu_items.general', 'admin.general_settings.index');
        if (!$menuItem->exists) $this->filler($menuItem, 'voyager-settings', 36, $actionsMenuItem->id);

        $menuItem = $this->creator($menu_administrator->id, 'menu_items.date_settings', 'admin.date_settings.index');
        if (!$menuItem->exists) $this->filler($menuItem, 'voyager-settings', 37, $actionsMenuItem->id);

        $menuItem = $this->creator($menu_administrator->id, 'menu_items.district', 'admin.regions.index');
        if (!$menuItem->exists) $this->filler($menuItem, 'nav-icon fas fa-map-marker-alt', 38, $actionsMenuItem->id);

        $menuItem = $this->creator($menu_administrator->id, 'menu_items.statuses', 'admin.statuses.index');
        if (!$menuItem->exists) $this->filler($menuItem, 'nav-icon fas fa-lightbulb', 39, $actionsMenuItem->id);

        $menuItem = $this->creator($menu_administrator->id, 'menu_items.montage_firms', 'admin.montage_firms.index');
        if (!$menuItem->exists) $this->filler($menuItem, 'nav-icon fas fa-lightbulb', 40, $actionsMenuItem->id);

        $menuItem = $this->creator($menu_administrator->id, 'menu_items.activity_type', 'admin.activity_type.index');
        if (!$menuItem->exists) $this->filler($menuItem, 'nav-icon fas fa-lightbulb', 41, $actionsMenuItem->id);


        // ===== Designer menus

        $menu_designer = Menu::where('name', 'designer')->firstOrFail();

        $menuItem = $this->creator($menu_designer->id, 'menu_items.projects-menu.project', 'admin.projects.index');
        if (!$menuItem->exists) $this->filler($menuItem, 'badge-primary', 42);

        $menuItem = $this->creator($menu_designer->id, 'menu_items.projects-menu.process', 'admin.projects.process');
        if (!$menuItem->exists) $this->filler($menuItem, 'badge-success', 43);

        $menuItem = $this->creator($menu_designer->id, 'menu_items.projects-menu.cancelled', 'admin.projects.cancelled');
        if (!$menuItem->exists) $this->filler($menuItem, 'badge-danger', 44);

        $menuItem = $this->creator($menu_designer->id, 'menu_items.archive', 'admin.projects.archive');
        if (!$menuItem->exists) $this->filler($menuItem, 'badge-secondary', 45);


        // ====== Montages menus

        $menu_montage_firm = Menu::where('name', 'montage_firm')->firstOrFail();

        $menuItem = $this->creator($menu_montage_firm->id, 'menu_items.general', 'admin.montage_firms.index');
        if (!$menuItem->exists) $this->filler($menuItem, 'badge-primary', 46);

        $menuItem = $this->creator($menu_montage_firm->id, 'menu_items.process', 'admin.montage_firms.process');
        if (!$menuItem->exists) $this->filler($menuItem, 'badge-success', 47);

        $menuItem = $this->creator($menu_montage_firm->id, 'menu_items.cancelled', 'admin.montage_firms.cancelled');
        if (!$menuItem->exists) $this->filler($menuItem, 'badge-danger', 48);

        $menuItem = $this->creator($menu_montage_firm->id, 'menu_items.archive', 'admin.montage_firms.archive');
        if (!$menuItem->exists) $this->filler($menuItem, 'badge-secondary', 49);

    }
}
