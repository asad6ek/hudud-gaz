<?php
use TCG\Voyager\Traits\Seedable;
use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Menu;

class MenusTableSeeder extends Seeder
{
    use Seedable;
    protected $seedersPath;
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        Menu::firstOrCreate(['name' => 'admin']);
        Menu::firstOrCreate(['name' => 'administrator']);
        Menu::firstOrCreate(['name' => 'director']);
        Menu::firstOrCreate(['name' => 'engineer']);
        Menu::firstOrCreate(['name' => 'technic']);
        Menu::firstOrCreate(['name' => 'region']);
        Menu::firstOrCreate(['name' => 'designer']);
        Menu::firstOrCreate(['name' => 'montage_firm']);

    }
}
