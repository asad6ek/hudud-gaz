<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Permission;
use TCG\Voyager\Models\Role;

class PermissionRoleTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::where('name', 'admin')->firstOrFail();

        $permissions = Permission::all();

        $role->permissions()->sync(
            $permissions->pluck('id')->all()
        );

        $role = Role::where('name', 'director')->firstOrFail();
        $permissions_director = Permission::whereIn('key', [
            'browse_propositions', 'read_propositions', 'browse_section_more', 'show_more_information',
            'browse_recommendations', 'read_recommendations', 'edit_recommendations',
            'browse_tech_conditions', 'read_tech_conditions', 'edit_tech_conditions', 'add_tech_conditions', 'delete_tech_conditions',
            'browse_equipments', 'read_equipments', 'edit_equipments',
            'browse_projects', 'read_projects',
            'browse_montage_firms',
            'browse_designers',
            'browse_montages',
            'browse_users',
            'browse_licenses'
        ])
            ->get();
        $role->permissions()->sync(
            $permissions_director->pluck('id')->all(),
        );

        $role_technic = Role::where('name', 'technic')->firstOrFail();
        $permissions_technic = Permission::whereIn('key', [
            'browse_propositions', 'read_propositions', 'edit_propositions', 'add_propositions', 'delete_propositions',
            'browse_recommendations', 'browse_process_recommendations', 'browse_archive_recommendations', 'browse_cancel_recommendations',
                'read_recommendations', 'edit_recommendations',
            'browse_tech_conditions', 'read_tech_conditions', 'edit_tech_conditions', 'add_tech_conditions', 'delete_tech_conditions',
            'browse_equipments', 'read_equipments', 'edit_equipments',
            'browse_technic_index', 'browse_section_region', 'browse_section_district', 'browse_section_more'
        ])
            ->get();
        $role_technic->permissions()->sync(
            $permissions_technic->pluck('id')->all(),
        );
        $role_region = Role::where('name', 'region')->firstOrFail();
        $permissions_region = Permission::whereIn('key', [
            'browse_propositions', 'read_propositions', 'edit_propositions',
            'browse_recommendations', 'browse_process_recommendations', 'browse_archive_recommendations', 'browse_cancel_recommendations',
                'read_recommendations', 'edit_recommendations', 'add_recommendations'
        ])
            ->get();
        $role_region->permissions()->sync(
            $permissions_region->pluck('id')->all(),
        );
        $role_admin = Role::where('name', 'administrator')->firstOrFail();
        $permissions_admin = Permission::whereIn('key', [
            'browse_users', 'read_users', 'edit_users', 'add_users', 'delete_users',
            'browse_equipments', 'read_equipments', 'edit_equipments', 'add_equipments', 'delete_equipments',
            'browse_equipments_types', 'read_equipments_types', 'edit_equipments_types', 'add_equipments_types', 'delete_equipments_types',
            'browse_statuses', 'read_statuses', 'edit_statuses',
            'browse_date_settings', 'read_date_settings', 'edit_date_settings', 'add_date_settings', 'delete_date_settings',
            'browse_designers', 'read_designers', 'edit_designers', 'add_designers', 'delete_designers',
            'browse_montage_firms', 'read_montage_firms', 'edit_montage_firms', 'add_montage_firms', 'delete_montage_firms',
            'browse_montage_employees', 'read_montage_employees', 'edit_montage_employees', 'add_montage_employees', 'delete_montage_employees'
        ])
            ->get();
        $role_admin->permissions()->sync(
            $permissions_admin->pluck('id')->all(),
        );
        $role_designer = Role::where('name', 'designer')->firstOrFail();
        $permissions_designer = Permission::whereIn('key', [
            'browse_projects', 'browse_process_projects', 'browse_archive_projects', 'browse_cancel_projects',
            'read_projects', 'add_projects', 'check_qr_for_projects'
        ])
            ->get();
        $role_designer->permissions()->sync(
            $permissions_designer->pluck('id')->all(),
        );

        $role_engineer = Role::where('name', 'engineer')->firstOrFail();
        $permissions_engineer = Permission::whereIn('key', [
            'browse_process_projects', 'browse_archive_projects', 'read_projects', 'edit_projects',
            'browse_process_montages', 'browse_archive_montages', 'read_montages', 'edit_montages', 'add_montages', 'delete_montages',
            'browse_reports',
            'browse_licenses'
        ])
            ->get();
        $role_engineer->permissions()->sync(
            $permissions_engineer->pluck('id')->all(),
        );

        $role_engineer = Role::where('name', 'montage_firm')->firstOrFail();
        $permissions_engineer = Permission::whereIn('key', [
            'browse_montages', 'browse_process_montages', 'browse_archive_montages', 'browse_cancel_montages',
            'read_montages', 'add_montages', 'check_qr_for_montages'
        ])
            ->get();
        $role_engineer->permissions()->sync(
            $permissions_engineer->pluck('id')->all(),
        );
    }
}
