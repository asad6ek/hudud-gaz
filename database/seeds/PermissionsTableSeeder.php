<?php

use Illuminate\Database\Seeder;
use App\Models\Permission;

class PermissionsTableSeeder extends Seeder
{

    public function createPermission($key, $table_name)
    {
        Permission::firstOrCreate([
            'key' => $key . '_' . $table_name,
            'table_name' => $table_name
        ]);
    }
    /**
     * Auto generated seed file.
     */
    public function run()
    {
        $keys = [
            'browse_admin',
            'browse_bread',
            'browse_database',
            'browse_media',
            'browse_compass',
        ];

        foreach ($keys as $key) {
            Permission::firstOrCreate([
                'key'        => $key,
                'table_name' => null,
            ]);
        }

        Permission::generateFor('menus');

        Permission::generateFor('roles');

        Permission::generateFor('users');

        Permission::generateFor('settings');

        Permission::generateFor('propositions');
        Permission::query()->firstOrCreate(['key' => 'show_more_information', 'table_name' => 'propositions']);

        Permission::generateFor('recommendations');
        Permission::query()->firstOrCreate(['key' => 'browse_process_recommendations', 'table_name' => 'recommendations']);
        Permission::query()->firstOrCreate(['key' => 'browse_archive_recommendations', 'table_name' => 'recommendations']);
        Permission::query()->firstOrCreate(['key' => 'browse_cancel_recommendations', 'table_name' => 'recommendations']);

        Permission::generateFor('tech_conditions');

        Permission::generateFor('equipments');

        Permission::generateFor('equipments_types');

        Permission::generateFor('projects');
        Permission::query()->firstOrCreate(['key' => 'browse_process_projects', 'table_name' => 'projects']);
        Permission::query()->firstOrCreate(['key' => 'browse_archive_projects', 'table_name' => 'projects']);
        Permission::query()->firstOrCreate(['key' => 'browse_cancel_projects', 'table_name' => 'projects']);
        Permission::query()->firstOrCreate(['key' => 'check_qr_for_projects', 'table_name' => 'projects']);

        Permission::generateFor('montages');
        Permission::query()->firstOrCreate(['key' => 'browse_process_montages', 'table_name' => 'montages']);
        Permission::query()->firstOrCreate(['key' => 'browse_archive_montages', 'table_name' => 'montages']);
        Permission::query()->firstOrCreate(['key' => 'browse_cancel_montages', 'table_name' => 'montages']);
        Permission::query()->firstOrCreate(['key' => 'check_qr_for_montages', 'table_name' => 'montages']);

        Permission::generateFor('statuses');

        Permission::generateFor('date_settings');

        Permission::generateFor('designers');

        Permission::generateFor('montage_firms');

        Permission::generateFor('montage_employees');

        Permission::query()->firstOrCreate(['key' => 'browse_reports']);
        Permission::query()->firstOrCreate(['key' => 'browse_technic_index']);
        Permission::query()->firstOrCreate(['key' => 'browse_section_region']);
        Permission::query()->firstOrCreate(['key' => 'browse_section_district']);
        Permission::query()->firstOrCreate(['key' => 'browse_section_more']);
        Permission::query()->firstOrCreate(['key' => 'browse_licenses']);



    }
}
