<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     */
    public function run()
    {
        Role::query()->firstOrCreate(['name' => 'admin'], ['display_name' => 'Админ']);

        Role::query()->firstOrCreate(['name' => 'technic'], ['display_name' =>'Техник']);

        Role::query()->firstOrCreate(['name' => 'administrator'], ['display_name' => 'Администратор']);

        Role::query()->firstOrCreate(['name' => 'engineer'], ['display_name' => 'Инжинер']);

        Role::query()->firstOrCreate(['name' => 'director'], ['display_name' => 'Директор']);

        Role::query()->firstOrCreate(['name' => 'region'], ['display_name' => 'Туман']);

        Role::query()->firstOrCreate(['name' => 'designer'], ['display_name' => 'Лойихачи']);

        Role::query()->firstOrCreate(['name' => 'montage_firm'], ['display_name' => 'Монтажчи']);
    }
}
