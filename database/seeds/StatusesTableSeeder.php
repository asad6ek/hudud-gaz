<?php


use App\Models\Status;
use Illuminate\Database\Seeder;

class StatusesTableSeeder extends Seeder {

    public function createStatus($description, $minutes = null, $permissions = '') {
        Status::query()->firstOrCreate([
            'description' => $description,
            'permissions' => $permissions,
            'minutes' => $minutes
        ]);
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $this->createStatus('Aриза қабул қилинди', 60);

        $this->createStatus('Туман очиб кўрди', 60);
        $this->createStatus('Туман тавсиянома яратди', 60);
        $this->createStatus('Туман имзоланган тавсияномани юклади', 60);

        $this->createStatus('Техник тавсияномани очиб кўрди', 60);
        $this->createStatus('Техник тавсияномани бекор килди', 60);
        $this->createStatus('Техник тавсияномани қабул килди', 60);
        $this->createStatus('Техник техник шартни яратди', 60);
        $this->createStatus('Техник имзоланган шартни юклади', 60);
        $this->createStatus('Техник имзоланган шартни юклади ва аризани бекор қилди');

        $this->createStatus('Лойиҳачи шартни очиб кўрди', 60);
        $this->createStatus('Лойиҳачи инжинерга лойиҳани юклади', 60);

        $this->createStatus('Инжинер лойиҳани очиб кўрди', 60);
        $this->createStatus('Инжинер лойиҳани бекор қилди');
        $this->createStatus('Инжинер лойиҳани ID рақам билан тасдиқлади');
        $this->createStatus('Тасдиқланган лойиҳа юкланди');

        $this->createStatus('Монтажчи ишни очиб кўрди');
        $this->createStatus('Монтажчи ишни бажарди');

        $this->createStatus('Инжинер монтажчи ишини тасдиқлади');
        $this->createStatus('Инжинер монтажчи ишини бекор қилди');

        $this->createStatus('Инжинер рухсатнома берди');
        $this->createStatus('Инжинер имзоланган рухсатномани юклади');

        $this->createStatus('Тавсиянома ўчирилди');
        $this->createStatus('Техник шарт ўчирилди');
        $this->createStatus('Лойиҳа ўчирилди');
        $this->createStatus('Монтаж ўчирилди');
    }
}
