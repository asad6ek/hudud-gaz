<?php



use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use TCG\Voyager\Models\User;
use TCG\Voyager\Traits\Seedable;

class UsersTableSeeder extends Seeder
{
    use Seedable;
    protected $seedersPath;

    public function creator($role_id, $name, $email, $password = 'admin', $locale = 'uzk', $avatar = '')
    {
        User::query()->firstOrCreate(
            ['email' => $email],
            [
                'role_id' => $role_id,
                'name' => $name,
                'email' => $email,
                'password' => Hash::make($password),
                'locale' => $locale,
                'avatar' => $avatar
            ]
        );
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        $this->creator(1, 'admin', 'admin@admin.com');

        $this->creator(2, 'technic', 'technic@gmail.com');

        $this->creator(3, 'administrator', 'admin@gmail.com');

        $this->creator(4, 'engineer', 'engineer@gmail.com');

        $this->creator(5, 'director', 'director@gmail.com');

        User::query()->firstOrCreate(
            ['email' => 'region@gmail.com'],
            [
                'role_id' => 6, 'name' => 'region',
                'email' => 'region@gmail.com', 'password' => Hash::make('admin'),
                'locale' => 'uzk', 'avatar' => '',
                'firm_id' => 1, 'firm_type' => 'App\Models\Region'
            ]
        );

        User::query()->firstOrCreate(
            ['email' => 'designer@gmail.com'],
            [
                'role_id' => 7, 'name' => 'designer',
                'email' => 'designer@gmail.com', 'password' => Hash::make('admin'),
                'locale' => 'uzk', 'avatar' => '',
                'firm_id' => 1, 'firm_type' => 'App\Models\Designer'
            ]
        );

        User::query()->firstOrCreate(
            ['email' => 'firm@gmail.com'],
            [
                'role_id' => 8, 'name' => 'montage_firm',
                'email' => 'firm@gmail.com', 'password' => Hash::make('admin'),
                'locale' => 'uzk', 'avatar' => '',
                'firm_id' => 1, 'firm_type' => 'App\Models\MontageFirm'
            ]
        );
    }
}
