function loader() {
    let wrapper = document.createElement('div');
    wrapper.classList.add('loader-wrapper');
    let loader = document.createElement('img');
    loader.src = "/img/yuklovchi.svg";
    loader.style.display = "none";
    loader.classList.add('loader-img')

    wrapper.append(loader);
    document.body.append(wrapper);
    $(loader).show(100);
}
