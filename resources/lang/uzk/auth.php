<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Ушбу маълумот бизнинг ёзувларимизга мос келмайди.',
    'password' => 'Тақдим этилган паро нотўғри!',
    'throttle' => 'Кириш учун жуда кўп уринишлар. Илтимос :seconds сониядан кейин қайта уриниб кўринг.',

    'name' => 'Исми',
    'surname' => 'Фамилияси',
    'patronymic' => 'Отаси',

];
