<?php

return [
    'dashboard' => 'Панел',
    'documents' => 'Ҳужжатлар',
    'license' => 'Рухсатнома',
    'total_report' => 'Умумий ҳисобот',
    'propositions' => 'Аризалар',
    'employees' => 'Ҳодимлар',
    'equipments' => 'Жиҳозлар',
    'district' => 'Туманлар',
    'designers' => 'Лойиҳачилар',
    'montage_firms' => 'Монтажчилар',
    'recommendations' => 'Тавсияномалар',
    'archive' => 'Архив',
    'reports' => 'Ҳисоботлар',
    'more' => 'Батафсил',
    'regions' => 'Вилоят кесимида',
    'projects' => 'Лойиҳалар',
    'montages' => 'Монтажлар',
    'statuses' => 'Статуслар',
    'action' => 'Ҳодисалар',
    'config' => 'Созламалар',
    'general' => 'Aсосий',
    'activity_type' => "Фаолият тури",
    'tech_conditions'=>'Техник шартлар',
    'date_settings' => "Иш жадвали",
    'process'=>'Жараёнда',
    'cancelled'=>"Бекор қилинган",
    'projects-menu'=>[
        'project'=>'Лойиҳалар',
        'process'=>'Жараёнда',
        'cancelled'=>"Бекор қилинган"
    ],
    'licenses' => "Рухсатномалар",

    'roles' => 'Rollar',
    'users' => 'Foydalanuvchilar',
    'media' => 'Media',
    'tools' => 'Asboblar',
    'settings' => 'Sozlamalar'

];
