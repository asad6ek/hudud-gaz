<?php
return [
    'save_success' => "Муаффақиятли яратилди",
    'update_success' => "Муаффақиятли янгиланди",
    'delete_success' => "Муаффақиятли ўчирилди",
    'btn_save' => "Сақлаш",
    'btn_edit' => "Ўзгартириш",
    'btn_del' => "Ўчириш",
    'btn_cancel' => "Бекор қилиш",
    'btn_clear' => "Тозалаш",
    'btn_upd' => "Юклаш",
    'btn_send' => "Жўнатиш",
    'btn_close' => "Ёпиш",
    'btn_show' => "Кўриш",
    'license' => "Рухсатнома",
    'uploaded' => "Юкланган",
    'show_license' => "Рухсатномани кўриш",
    'propositions' => [
        'individual' => 'Жисмоний шахс',
        'legal_entity' => 'Юридик шахс',
        'pass_id' => 'Паспорт серияси ва рақами',
        'full_name' => 'Тўлиқ исми',
        'tel_number' => 'Телефон рақами',
        'legal_entity_tin' => 'Юридик шахс СТИРи',
        'legal_entity_name' => 'Юридик шахс номи',
        'leader_name' => 'Рахбарнинг номи',
        'leader_tin' => 'Рахбарнинг СТИР',
        'residential_building' => 'Турар',
        'non_residential_building' => 'Нотурар',
        'building_type' => 'Бино тури',
        'email' => 'Электрон почта',
        'customer' => 'Истемолчи',
        'prop_number' => 'Ариза рақами',
        'district' => 'Туман',
        'proposition' => 'Ариза',
        'date' => 'Сана',
        'time_limit' => 'Вақт лимити',
        'action' => 'Амал',
        'add_prop' => 'Ариза қўшиш',
        'edit_prop' => "Аризани ўзгартириш",
        'delete_prop' => 'Аризани ўчириш',
        'type' => "Фаолият тури",
        'physical' => 'Жисмоний шахс',
        'legal' => 'Юридик шахс',
        'stir' => 'СТИР',
        'name' => 'Номи',
        'type2' => 'Фаолият тури',
        'population' => 'Ахоли хонадонлари',
        'business' => 'Ўрта бизнес',
        'industrial' => 'Саноат корхоналари',
        'other' => 'Бошқалар',
        'district-city' => 'Ташкилот номи',
        'upload' => 'Аризани юклаш',
        'choose_file' => 'Файлни танланг',
        'browse' => 'Танлаш',
        'save' => 'Сақлаш',
        'prop' => 'Аризалар',
        'read' => 'Аризани ўқиш',
        'check_props' => "СТИР бўйича аризалар сони",
        'show_prop' => "Кўриш",
        'status' => 'Ҳолат',
        // Validation
        'file_path' => "Файл манзили",
        'delete_alert' => "Аризани бекор қилмоқчимисиз?"
    ],
    'region' => [
        'urgench_city' => 'Урганч шаҳар',
        'urgench_district' => 'Урганч туман',
        'khiva_city' => 'Хива шаҳар',
        'khiva_district' => 'Хива туман',
        'khonqa' => 'Хонқа туман',
        'bagat' => 'Боғот туман',
        'yangiariq' => 'Янгиариқ туман',
        'yangibozor' => 'Янгибозор туман',
        'khazorasp' => 'Хазорасп туман',
        'shavat' => 'Шовот туман',
        'gurlan' => 'Гурлан туман',
        'tuproqqala' => 'Тупроққалъа туман',
        'kushkupir' => 'Қўшкўпир туман',
    ],

    'logout' => 'Чиқиш',
    'back' => 'Орқага',
    'recommendations' => 'Тавсияномалар',
    'cancelled' => 'Бекор қилинганлар',
    'recommendation' => 'Тавсиянома',
    'archive' => 'Архив',
    'submit' => 'Жўнатиш',
    'add' => 'Қўшиш',
    'change' => 'Ўзгартириш',
    'delete' => 'Ўчириш',

    'action' => 'Амаллар',
    'date' => 'Сана',
    'today' => 'Бугунги аризалар',
    'waiting' => 'Кўрилаётган аризалар',
    'late' => 'Муддати ўтган аризалар',
    'year' => 'Йиллик аризалар',

    'rec' => [
        'add_rec' => 'Тавсиянома қўшиш',
        'add_inf' => 'Қуйидаги маълумотларни киритинг',
        'add_equipment' => 'Жиҳоз қўшиш',
        'address' => 'Манзил',
        'input_address' => 'Манзилни киритинг',
        'access_point' => 'Уланиш нуқтаси',
        'input_access_point' => 'Уланиш нуқтасини киритинг',
        'gas_network' => 'Газ тармоғи номи',
        'input_gas_network' => 'Газ тармоғи номини киритинг',
        'pipe_type' => 'Қувур тури',
        'low_pressure' => 'Паст босимли',
        'high_pressure' => 'Юқори босимли',
        'underline' => ' Қувурнинг ер ости узунлиги',
        'input_underline' => 'Ер ости узунликни киритинг',
        'length_type' => 'Қувурнинг ўтказиш тури',
        'label_underline' => 'Ер ости',
        'label_line' => 'Ер усти',
        'length' => 'Қувурнинг узунлиги',
        'input_length' => 'Узунликни киритинг',
        'line' => 'Қувурнинг ер усти узунлиги',
        'input_line' => 'Ер усти узунликни киритинг',

        'diameter' => 'Қувур диаметри',
        'input_diameter' => 'Қувур диаметрини киритинг',

        'pipe_size' => 'Газ қувурининг диаметри',
        'input_pipe_size' => 'Диамтерни киритинг',
        'depth' => 'Уланиш нуқтаси чуқурлиги',
        'input_depth' => 'Чуқурликни киритинг',
        'capacity' => 'Қувурнинг ўтказиш қобилияти',
        'real_capacity' => 'Ҳақиқатдан',
        'input_capacity' => 'Қийматни киритинг',
        'avg_winter' => 'Ўрт. қишги босим',
        'input_avg_winter' => 'Ўртача қишги босимни киритинг',
        'avg_summer' => ' Ўрт. ёзги босим',
        'input_avg_summer' => 'Ўртача ёзги босимни киритинг',
        'size' => 'Талаб қилинган газ ҳажми (йиллик)',
        'input_size' => 'Талаб қилинган газ ҳажмини киритинг',
        'read_rec' => 'Тавсияномани ўқиш',
        'upload' => 'Имзоланган тавсияномани юклаш',
        'upload_btn' => 'Юклаш',
        'rec_number' => 'Тавсиянома рақами',
        'cancel_description' => 'Бекор қилиш сабаби',
        'cancel_information' => 'Aризани бекор қилиш ҳақида маълумотнома',
        'content' => 'Матн',
        'input_cancel_description' => 'Сабабни киритинг...',
        'select_equipment_type' => 'Жиҳоз турини танланг',
        'select_equipment' => 'Жиҳозни танланг',
        'input_parameter' => 'Парамтерни киритинг',
        'confirm' => 'Тасдиқлаш',
        'back' => 'Туманга қайтариш',
        'come_back' => 'Туманга қайтарилди',
        'write_comment' => 'Тавсиянома учун изоҳингизни ёзинг...',
        'grc' => 'Газ қайси ГРCдан берилади?',
        'input_grc' => 'ГРCни киритинг...',
        'prop_id' => "Бу аризанинг тавсияномаси",
    ],
    'statuses' => [
        'index' => '№',
        'heading' => 'Статусни ўзгартириш',
        'description' => 'Изоҳ',
        'permissions' => 'Ўтишлар',
        'hours' => 'Соат'
    ],
    'equipments' => [
        'name' => 'Номи',
        'type' => 'Тури',
        'add_type' => 'Турлар',
        'add_types' => 'Тур қўшиш',
        'edit_type' => 'Тур ўзгартириш',
        'delete_type' => 'Турни ўчириш',
        'order' => 'Тартиб',
        'alert_message' => "Жиҳозни олиб ташламоқчимисиз?",
        'alert_text' => "Ўчириб ташлаш учун тасдиқланг"
    ],
    'equipment' => [
        'name' => "Жиҳоз номи",
        'name_hint' => "Жиҳозни танланг",
        'type' => "Жиҳоз тури",
        'type_hint' => "Жиҳоз турини танланг",
        'number' => "Сони",
        'number_hint' => "Сонини киритинг",
        'note' => "Изох",
        'note_hint' => "Изоҳни киритинг",
        'warning' => "Охиридан бошлаб ўчиринг",
        'gas_meter' => "Газ ҳисоблагич",
    ],
    'date_settings' => [
        'create' => 'Қўшиш',
        'back' => 'Орқага',
        'holidays' => 'Байрам кунлари',
        'working_days' => 'Қўшимча иш кунлари',
        'from' => 'Бошланиш вақти',
        'to' => 'Тугаш вақти',
        'name' => 'Номи',
        'delete' => 'Ўчириш',
        'from_to' => 'Вақт оралиғи',
        'type' => 'Тури'
    ],
    'montage' => [
        'button_add_new' => 'Янгисини қўшиш',
        'search_field' => "Қидириш",
        'col_reg_number' => "Регистрация рақами",
        'col_org_name' => "Ташкилот номи",
        'col_lead' => "Рахбари",
        'col_address' => "Манзили",
        'col_period_activity' => "Фаолият даври",
        'button_employee' => "Ходимлар",
        'button_edit' => "Ўзгартириш",
        'button_delete' => "Ўчириш",
        'record_number' => "Тасдиқлаш рақами",
        'reg_number' => "Регистрация рақами",
        'full_name' => "Фирманинг тўлиқ номи",
        'short_name' => "Фирманинг қисқа номи",
        'reg_leader' => "Ташкилот рахбари",
        'region_name' => "Ташкилот жойлашган туман (шаҳар)",
        'address' => "Ташкилот манзили (қайерда жойлашган)",
        'taxpayer_stir' => "Солиқ учун СТИР рақам",
        'legal_form' => "Ташкилий-ҳуқуқий шакли",
        'given_by' => "Ким томонидан берилган",
        'date_created' => "Рўйхатга олинган сана",
        'date_expired' => "Амал қилиш муддати (гача)",
        'permissions_to' => "Рухсат этилган",
        'implements_for' => "Амалга оширади",
        'file_upload' => "Файлни юклаш",
        'button_save' => "Сақлаш",
        'button_previous' => "Ортга қайтиш",
        'select_hint' => "Танланг",
        'button_yes' => "Ҳа!",
        'button_no' => "Йўқ",
        'delete_message' => "Ташкилотни ўчириб ташламоқчимисиз?",
        'deleting_finish' => "Ўчирилмоқда ...",

        // Validation
        'input_date_error' => "Саналар нотўғри киритилган",
        'input_file_error' => "Файл юкланмаган"
    ],
    'fitter' => [
        'button_add' => "Ишчи қўшиш",
        'col_organization' => "Ташкилот",
        'col_statement' => "Буйруқ рақами",
        'col_name' => "Исм, фамилия",
        'col_period_activity' => "Фаолият даври",
        'col_passport' => "Паспорт серияи",
        'col_function' => "Вазифаси",
        'col_experience' => "Иш тажрибаси",
        'statement_number' => "Буйруқ рақами",
        'first_name' => "Исми",
        'second_name' => "Отасининг исми (шарифи)",
        'last_name' => "Фамилияси",
        'diploma_number' => "Диплом рақами",
        'passport_series' => "Паспорт серияси",
        'date_contract' => "Шартнома тузилган сана",
        'date_contract_end' => "Амал қилиш муддати (гача)",
        'specialized' => "Мутахасислиги",
        'function' => "Вазифаси",
        'experience' => "Иш тажрибаси",
        'delete_message' => "Ходимни ўчириб ташламақчимисиз?",
        'document' => "Ишчининг ҳужжати"
    ],
    'regions' => [
        'button_add_new' => "Ташкилот қўшиш",
        'col_org_number' => "Ташкилот рақами",
        'col_org_name' => "Номланиши",
        'col_org_leader' => "Бош муҳандис",
        'col_org_email' => "Э-почта",
        'col_org_phone' => "Телефон",
        'col_org_address' => "Манзил",
        'col_org_fax' => "Фах",
        'org_region_name' => "Ташкилот жойлашган туман (шаҳар)",
        'org_number' => "Ташкилот рақами",
        'org_name' => "Ташкилот номи",
        'leader' => "Бош муҳандис (Исм, фамилия)",
        'section_leader' => "Бўлим бошлиғи (Исм, фамилия)",
        'email' => "Электрон почта манзили",
        'phone' => "Телефон рақами",
        'address_latin' => "Manzili (lotincha)",
        'address' => "Манзили",
        'fax' => "Фах",

        // Heading
        'add_new_org' => "Янги ташкилот қўшиш",
        'edit_now_org' => "Ташкилот маълумотларини ўзгартириш"
    ],
    'general_settings' => [
        'settings_title' => "Маълумотларни янгилаш",
        'shareholder_name' => "Аксиядорлик жамияти номи",
        'branch_name' => "Газтаъминот филиали номи",
        'engineer' => "Бош муҳандис",
        'full_name' => "Исми, Отасининг исми, Фамилияси (И.О.Фамилияси)",
        'helper' => "Муҳандис ёрдамчиси",
        'button_clear' => "Майдонларни тозалаш",
        'upload_image' => "Расмни юкланг",
        'tech_section' => "Техник ишлаб чиқариш бўлими етакчи муҳандиси",
        'metrology' => "Метрология бўлими бошлиғи",
        'exp_section' => "Газ тармоқларини эксплуатация қилиш бўлими бошлиғи",
        'legal_section' => "Юридик шахсларга гаэ етказиб бериш бўлими бошлиғи"
    ],
    'employees' => [
        'col_firstname' => "Исми",
        'col_lastname' => "Фамилияси",
        'col_position' => "Лавозими",
        'col_email' => "Э-почта",
        'col_role' => "Ўрни",
        'firstname' => "Исми",
        'lastname' => "Фамилияси",
        'second_name' => "Отасиннг исми (шарифи)",
        'patronymic' => "Отасининг исми",
        'email' => "Электрон почта манзили",
        'password' => "Парол",
        'role' => "Ўрни",
        'contestants' => "Ташкилотни танланг",
        'locale' => "Тил",
        'position' => "Лавозими",
        'alert_message' => "Ходимни ўчириб ташламоқчимисиз?",

        // Validation
        'not_org_error' => "Яратишдан олдин ташкилот қўшинг"
    ],
    'activity_type' => [
        'col_number' => 'Т/р',
        'label' => 'Фаолият тури номини киритинг',
        'button_cancel' => 'Бекор қилиш',
        'alert_message' => "Фаолият турини ўчириб ташламоқчимисиз?",
    ],
    'designers' => [
        'phone_number' => "Телефон рақами (ҳар бирини алоҳида қаторга ёзинг)",
        'document' => "Ташкилотнинг расмий ҳужжатини юкланг",
        'phone' => "Телефон рақами"
    ],
    'profile' => [
        'main_changes' => "Асосий ўзгаришлар",
        'change_pass' => "Яширин сўзни алмаштириш",
        'old_password' => "Эски парол",
        'new_password' => "Янги парол",
        'pass_confirm' => "Паролни тасдиқланг",
        'confirming_pass' => "Тасдиқловчи парол",
        'not_confirm' => "Пароллар тасдиқланмади",
        'wrong_pass' => "Парол нотўғри киритилган"
    ],

    'tech_condition' => 'Техник шарт',
    'tech' => [
        'read_tech' => 'Шартни ўқиш',
        'for_prop' => "Бу ариза учун техник шарт"
    ],

    'projects' => [
        'project' => 'Лойиха',
        'tech_condition' => 'Техник шарт раками',
        'tech_read' => 'Техник шартни ўқиш',
        'project_read' => 'Лойиҳани ўқиш',
        'montage_read' => 'Монтажни ўқиш',
        'come_back' => 'Лойиҳачига қайтарилди',
        'write_comment' => 'Лойихачига изоҳ ёзинг',
        'agree_title' => 'Ростдан ҳам лойиҳани тасдиқламоқчимисиз?',
        'agreed' => 'Лойиҳа тасдиқланди. Тасдиқлаш коди: ',
        'uploaded' => 'Муваффақиятли юкланди!',
        'comment' => 'Бекор бўлиш сабаби',
        'cancel' => 'Лойиҳадан бириктирилганликни олиб ташлаш',
        'alert_title' => 'Лойихани рад қилмоқчимисиз?'
    ],

    'confirm' => 'Тасдиқлаш',
    'back_doc' => 'Қайтариш',
    'qrcode' => 'Qr Кодни ўқиш',
    'code' => 'Кодни киритинг',
    'check' => 'Текшириш',
    'choose_of' => 'ни танланг',

    'montages' => [
        'montages_table' => 'Монтаж',
        'read' => 'Ўқиш',
        'pipe_size' => 'Қувур диаметри',
        'alert_title' => 'Монтажчи учун изоҳ қолдиринг.',
        'comment' => 'Бекор қилиш сабаби',
        'delete_title' => 'Ишни бекор қилмоқчимисиз?'
    ],

    'reports' => [
        'id' => '№',
        'created_at' => 'Рўйхатга олинган сана',
        'address' => 'Газлаштирилган тармоқлар ва истемолчи манзили',
        'designer' => 'Лойихачи ташкилот',
        'montage_firm' => 'Қурувчи ташкилот',
        'firms' => 'Лойихачи ва Қурувчи ташкилот',
        'date' => 'Бажариш бошланган ва тугаган сана',
        'start_date' => 'Бажариш бошланган сана',
        'end_date' => 'Бажариш тугаган сана',
        'tech_condition_number_and_date' => 'Техник шарт рақами, санаси ва пдф',
        'project_number_and_date' => 'Лойиҳанинг рақами, санаси ва пдф',
        'montage_number_and_date' => 'Монтаж рақами, санаси ва пдф',
        'annual_gas_consumption' => 'Техник шарт бўйича йиллик газ сарфи (м³)',
        'length_of_pipe' => 'Газ қувурининг узунлиги',
        'pipe_size'
    ],

    'send_to_region' => 'Туманга жўнатиш',
    'day' => 'кун',
    'hours' => 'соат',
    'minute' => 'дақиқа',
    'alerts' => [
        'date_expired' => "Ташкилоитнинг фаолият даври тугаган",
        'upl_success' => "Муаффақиятли юкланди"
    ],
    'full_information' => 'Батафсил'
];
