<?php

return [
    'dashboard' => 'Panel',
    'documents' => 'Hujjatlar',
    'license' => 'Ruxsatnoma',
    'total_report' => 'Umumiy hisobot',
    'propositions' => 'Arizalar',
    'employees' => 'Xodimlar',
    'equipments' => 'Jihozlar',
    'district' => 'Tumanlar',
    'designers' => 'Loyihachilar',
    'montage_firms' => 'Montajchilar',
    'recommendations' => 'Tavsiyanomalar',
    'archive' => 'Arxiv',
    'reports' => 'Hisobotlar',
    'more' => 'Batafsil',
    'regions' => 'Viloyat kesimida',
    'projects' => 'Loyihalar',
    'montages' => 'Montajlar',
    'statuses' => 'Statuslar',
    'action' => 'Hodisalar',
    'config' => 'Sozlamalar',
    'general' => 'Asosiy',
    'activity_type' => "Faoliyat turi",
    'tech_conditions'=>'Texnik shartlar',
    'date_settings' =>'Sanalar',
    'process'=>'Jarayonda',
    'cancelled'=>'Bekor qilingan',
    'projects-menu'=>[
        'project'=>'Loyihalar',
        'process'=>'Jarayonda',
        'cancelled'=>'Bekor qilingan'
    ],
    'licenses' => "Ruxsatnomalar",

    'roles' => 'Rollar',
    'users' => 'Foydalanuvchilar',
    'media' => 'Media',
    'tools' => 'Asboblar',
    'settings' => 'Sozlamalar'
];
