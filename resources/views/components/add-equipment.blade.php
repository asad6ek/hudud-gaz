<div class="card-body col-md-12" id="component_{{$componentIndex}}">
    <div class="row">
        <div class="col-md-4">
            <select name="equipment_{{$componentIndex}}" id="equipment_{{$componentIndex}}" class="form-control"
                    data-dependent="equipment_type" onchange="appender(this, {{$componentIndex}})">
                <option selected>@lang("table.rec.select_equipment")</option>
                @foreach($equipments as $equipment)
                    <option value="{{$equipment->id}}">{{ $equipment->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-4">
            <select name="equipments[{{$componentIndex}}][type_id]" id="equipment_type_{{$componentIndex}}"
                    class="form-control" data-dependent="parameter" required>
                <option selected>@lang("table.rec.select_equipment_type")</option>
            </select>
        </div>
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-10">
                    <input type="text" name="equipments[{{$componentIndex}}][parameter]" id="equipment_parameter_{{$componentIndex}}" class="form-control"
                           placeholder="{{__("table.rec.input_parameter")}}" required>
                </div>
                <div class="col-md-2">
                    <button class="btn btn-danger" onclick="remove_equipment({{$componentIndex}})" type="button">
                        <i class="fas fa-minus-square"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>
