<{{$type}}  {!!   \App\View\AttributeLister::list($attributes, $options ) !!}
@if(!$hasContent && strlen(trim($slot)) === 0)/@endif>

@if($hasContent || strlen(trim($slot)) > 1)
		{{ $content }}
		{{ $slot }}
</{{$type}}>
@endif
