<?php /**
 * @var $attributes \Illuminate\View\ComponentAttributeBag
 */

$options = \App\View\AttributeLister::options($options);
?>
<select name="{{ $name }}" {!! \App\View\AttributeLister::list($attributes, $options, ['name', 'value'],'prompt' ) !!} >
	@if($attributes->get('prompt', $options['prompt']??''))
		<option value=""  >
			{{ $attributes->get('prompt', $options['prompt']??'') }}
		</option>
	@endif

	@if($items)
		@foreach($items as $optionValue => $option)
			@php
				$value = \App\View\AttributeLister::invokableToVal($value);

			@endphp
			<option value="{{$optionValue}}" @if($isSelectedOption($optionValue)) selected @endif >
				{{$option}}
			</option>
		@endforeach
	@endif
</select>
