<?php
/**
 * @var $attributes \Illuminate\View\ComponentAttributeBag
 */


$value = \App\View\AttributeLister::invokableToVal($attributes->get('value', $value));

$checkedValue = \App\View\AttributeLister::invokableToVal($checkedValue);


?>
<x-html.tag type="div" :options="$divOptions">
	<x-html.tag type="label" :options="$labelOptions"/>
	<input type="hidden" name="{{ $attributes->get('name', $attribute) }}" value="0">
	<br>
	<input type="checkbox" name="{{ $attributes->get('name', $attribute) }}"
	       value="{{$attributes->get('value', $checkedValue)}}"

	       @if($value == $checkedValue) checked @endif
			{!! \App\View\AttributeLister::list($attributes, $options, ['name', 'value'] ) !!}
	>

</x-html.tag>
