<?php

$options =  \App\View\AttributeLister::merge($attributes, $options);
$options = \App\View\AttributeLister::invokableToVal($options);
$value = $attributes->get('value', $value);

?>
<x-html.tag type="div" :options="$divOptions">
    <x-html.tag type="label" :options="$labelOptions" />
    <x-inputs.dropdown
            :value="$value"
            name="{{ $attributes->get('name', $attribute) }}"
            :options="$options"
            :items="$items"  />
</x-html.tag>
