<?php
/**
 * @var $attributes \Illuminate\View\ComponentAttributeBag
 */

?>
<input
		type="{{ $attributes->get('type', $type)  }}"
		name="{{ $attributes->get('name', $name)  }}"
		value="{{ $attributes->get('value', $value)  }}"
		id="{{$attributes->get('id', $id)  }}"
		class="{{ $attributes->get('class', $class) }}"

{!! \App\View\AttributeLister::list($attributes, $options, ['type', 'name', 'id', 'class', 'value'] ) !!}

/>

