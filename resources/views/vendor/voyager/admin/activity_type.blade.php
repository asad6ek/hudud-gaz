
@extends('voyager::master')

@section('page_title', getOrganizationName())

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @include('voyager::alerts')
                    <div class="card">
                        <div class="card-header">
                            <button id="myBtn" class="btn btn-success">{{__('table.add')}}</button>
                            <div class="card-tools mt-2">
                                <div class="input-group input-group-sm" style="width: 150px;">
                                    <input type="text" id="table_search" name="table_search" class="form-control float-right"
                                           placeholder="{{__('table.montage.search_field')}}">

                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-default">
                                            <i class="fas fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body table-responsive p-0">
                            <table class="table table-hover text-nowrap">
                                <thead>
                                    <tr>
                                        <th style="width: 1px;">{{__('table.activity_type.col_number')}}</th>
                                        <th class="col-12">{{__('menu_items.activity_type')}}</th>
                                        <th style="width: 1px;"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($activities as $activity)
                                        <tr>
                                            <td class="col-lg-auto">{{$loop->index + 1}}</td>
                                            <td class="col-6">{{$activity->activity_name}}</td>
                                            <td class="col-6">
                                                <form method="post" id="form_delete-{{$activity->id}}"
                                                      action="{{route('admin.activity_type.delete', ['activity_type' => $activity->id])}}">

                                                    @can('edit_equipments_types')
                                                        <button onclick="fillData('{{$activity->activity_name}}', {{$activity->id}})" class="btn btn-warning" type="button"
                                                                data-toggle="tooltip" title="{{__('table.montage.button_edit')}}"><i class="fas fa-pencil-alt"></i></button>
                                                    @endcan
                                                    @can('delete_equipments_types')
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="button" onclick="confirmDelete({{$activity->id}})" class="btn btn-danger"
                                                                role="button" data-toggle="tooltip" title="{{__('table.montage.button_delete')}}"

                                                        ><i class="fas fa-trash-alt"></i>
                                                        </button>
                                                    @endcan
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="modal fade" id="myModal" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body" style="padding:40px 50px;">
                                        <form method="POST" id="form_forType" onsubmit="submit.disabled = true" role="form" >
                                            @csrf
                                            <input type="hidden" name="_method" id="request_type">
                                            <div class="form-group">
                                                <label for="activity_name" class="col-10 col-form-label">{{__('table.activity_type.label')}}</label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="activity_name" class="form-control" id="activity_name" placeholder="{{__('menu_items.activity_type')}}" required>
                                                </div>
                                            </div>

                                            @can('add_users')
                                                <input type="submit" id="submit" class="btn btn-success mr-2 ml-2"  value="{{__('table.add')}}">
                                            @endcan
                                            <button type="button" class="btn btn-danger btn-default pull-right" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> {{__('table.activity_type.button_cancel')}}</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
@section('body_js')
    <script>
        $('#table_search').keyup(function () {
            let qiymat = this.value.toLowerCase();
            $('tbody tr').filter(function () {
                $(this).toggle($(this).text().toLowerCase().indexOf(qiymat) > - 1)
            })
        });

        $(document).ready(function() {
            $("#myBtn").click(function() {
                $('#form_forType').attr('action', '{{route('admin.activity_type.store')}}');
                $("#activity_name").val('');

                $("#myModal").modal();
                $('#request_type').val("POST");
            });
        });

        function fillData(name, id) {
            let route = '{{route('admin.activity_type.update', ['activity_type' => 0])}}';
            $('#form_forType').attr('action', route.substr(0, route.length - 1) + id);
            $("#activity_name").val(name);

            $("#myModal").modal();
            $('#request_type').val("PUT");
        }

        function confirmDelete(id) {
            Swal.fire({
                title: '{{__('table.activity_type.alert_message')}}',
                text: "",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: '{{__('table.montage.button_yes')}}',
                cancelButtonText: '{{__('table.montage.button_no')}}',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $(`#form_delete-${id}`).submit()
                    Swal.fire({
                        title: '{{__('table.montage.deleting_finish')}}',
                        icon: 'success',
                        showConfirmButton: false,
                    })
                }
            });
        }
    </script>
@stop
