<div class="alerts">
    @if(Illuminate\Support\Facades\Session::has('message'))
        <script>
            addEventListener('DOMContentLoaded', function() {
                Swal.fire({
                    title: '{{Illuminate\Support\Facades\Session::get('message')}}',
                    text: "",
                    icon: '{{Illuminate\Support\Facades\Session::get('message-type')}}',
                    showConfirmButton: false,
                    timer: 1500
                });
            });
        </script>
    @endif
</div>
