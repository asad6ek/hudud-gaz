<div class="content-header">
    <div @if($role_name == 'region')
            class="container"
         @else
            class="container-fluid"
         @endif>
        <div class="row mb-2">
            <div class="col-sm-6">
                @if(!user_role('region'))
                <h1 class="m-0">Dashboard</h1>
                @endif
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    @if(!user_role('region'))
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Dashboard v1</li>
                    @endif
                </ol>
            </div>
        </div>
    </div>
</div>
