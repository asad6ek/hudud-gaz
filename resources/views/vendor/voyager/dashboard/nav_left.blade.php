<ul class="navbar-nav">
    <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
    </li>
    @if(user_role('technic'))
        <li class="nav-item pt-2 ml-5">
            <div class="">
                <h3 class="card-title">@lang('table.today'): {{$in_today}}</h3>
            </div>
        </li>
        <li class="nav-item pt-2 ml-5">
            <div class="">
                    <h3 class="card-title">@lang('table.waiting'): {{$in_progress}}</h3>
            </div>
        </li>
        <li class="nav-item pt-2 ml-5">
            <div class="">
                <h3 class="card-title">@lang('table.late'): {{$in_late}}</h3>
            </div>
        </li>
        <li class="nav-item pt-2 ml-5">
            <div class="">
                <h3 class="card-title">@lang('table.year'): {{$in_year}}</h3>
            </div>
        </li>
    @endif
</ul>

