<ul class="navbar-nav ml-auto">
    <li class="nav-item">
        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
            <i class="fas fa-expand-arrows-alt"></i>
        </a>
    </li>
    <li class="nav-item">
        <div class="user-panel d-flex" style="margin-top: 3px">
            <div class="image">
                <img src="{{setImage(auth()->user())}}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="{{route('admin.profile.edit', ['user' => $user])}}" class="d-block" style="color: rgba(0,0,0, 0.5)">
                    {{ $user->name }}
                </a>
            </div>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link logout"  href="{{ route('admin.logout') }}" role="button">
            <i class="fas fa-sign-out-alt"></i>
            <span>{{__('table.logout')}}</span>
        </a>
    </li>

    <style>
        .nav-link.logout {
            display: flex;
            align-items: center;
        }

        .nav-link.logout i {
            margin-right: 2px;
        }
    </style>
</ul>
