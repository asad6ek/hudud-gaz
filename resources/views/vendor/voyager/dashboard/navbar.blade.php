<nav class="main-header navbar navbar-expand navbar-white navbar-light">

    <!-- Left navbar links -->
    @include('voyager::dashboard.nav_left', [
    'in_today' => $in_today, 'in_progress' => $in_progress, 'in_late' => $in_late, 'in_year' => $in_year
])

    <!-- Right navbar links -->
    @include('voyager::dashboard.nav_right')

</nav>
