<style>
    .nav-link.logout {
        display: flex;
        align-items: center;
    }

    .nav-link.logout i {
        margin-right: 2px;
    }
</style>
<nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
    <div class="container" style="max-width: 90%">
        <a href="{{ route('voyager.dashboard') }}" class="navbar-brand col-md-3">
            <img src="{{ asset('img/logo.png') }}" alt="HududGaz" class="col-md-12">
        </a>

        <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse order-3" id="navbarCollapse">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                @foreach(menu_items(menu($role, '_json')) as $menu)
                    <li class="nav-item">
                        <a href="{{ $menu->href }}" class="nav-link">
                            <p>
                                {{ $menu->title }}
                                <span class="badge {{$menu->icon_class}}">
                                    {{get_defined_vars()[$badge_names[$loop->index]]}}
                                </span>
                            </p>
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
        <!-- Right navbar links -->
        <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
            <li class="nav-item">
                <div class="user-panel d-flex" style="margin-top: 3px">
                    <div class="image">
                        <img src="{{setImage(auth()->user())}}" class="img-circle elevation-2" alt="User Image">
                    </div>
                    <div class="info">
                        <a href="{{route('admin.profile.edit', ['user' => $user])}}" class="d-block" style="color: rgba(0,0,0, 0.5)">
                            {{ $user->name }}
                        </a>
                    </div>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link logout"  href="{{ route('admin.logout') }}" role="button" data-toggle="tooltip" title="Roledan chiqish">
                    <i class="fas fa-sign-out-alt"></i>
                    <span>{{__('table.logout')}}</span>
                </a>
            </li>
        </ul>
    </div>
</nav>
