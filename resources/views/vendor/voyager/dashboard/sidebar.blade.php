<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <a href="{{ route('voyager.dashboard') }}" class="brand-link">
        <img src="{{ asset('img/logo.png') }}" alt="HududGazTa'minot" class="w-100" style="opacity: .8">
    </a>
    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <div id="adminmenu">
                <admin-menu :items="{{ menu_items(menu($role_name, '_json')) }}"></admin-menu>
            </div>

        </nav>
    </div>
</aside>
