@extends('voyager::master')
@section('head_css')
    <link rel="stylesheet" type="text/css" href="{{asset('icofont/css/icofont.css')}}">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/jquery.mCustomScrollbar.css')}}">
    <!-- daterange picker -->
    <link rel="stylesheet" href="{{asset('css/daterangepicker.css')}}">
@stop

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- Basic Form Inputs card start -->
                    @include('voyager::alerts')

                    <div class="card">
                        @includeFirst(['voyager::date_setting.form'],[
                              'method'=>'POST',
                              'action'=>route('admin.date_settings.store'),
                              'model'=>$model,
                          ])
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('body_js')
    <script src="{{asset('js/select2.full.min.js')}}"></script>
    <script src="{{asset('js/moment.min.js')}}"></script>
    <script src="{{asset('js/jquery.inputmask.min.js')}}"></script>
    <!-- date-range-picker -->
    <script src="{{asset('js/daterangepicker.js')}}"></script>

    <script>
        $(function () {
            var loadFile = function (event) {
                document.getElementById('label_for_image').innerHTML = event.target.files[0].name;
            };
            $('#reservationtime').daterangepicker({
                timePicker: true,
                timePickerIncrement: 30,
                locale: {
                    format: 'MM/DD/YYYY hh:mm A'
                }
            })
        })
    </script>
@stop
