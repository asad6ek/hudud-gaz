<form method="post" action="{{$action}}" enctype="multipart/form-data">
    @csrf
    @method($method)
    @includeFirst(['voyager::components.errors'],['errors' => $errors])

    <div class="card-body">

        <div class="form-group row">
            <label for="name" class="col-sm-2 col-form-label">{{__('table.date_settings.name')}}</label>
            <div class="col-sm-10">
                <input name="name" type="text" class="form-control" value="{{old('name',$model->name)}}" id="name">
            </div>
        </div>


        <div class="form-group row">
            <label for="name" class="col-sm-2 col-form-label">{{__('table.date_settings.from_to')}}</label>
            <div class="col-sm-10">

                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="far fa-clock"></i></span>
                    </div>
                    <input type="text" name="from_to" class="form-control float-right" id="reservationtime">
                </div>
            </div>
        </div>

        <div class="form-group row">
            <label for="proposition_type" class="col-sm-2 col-form-label">{{__('table.date_settings.type')}}</label>
            <div class="col-sm-10">
                <select name="type" id="proposition_type" class="form-control">
                    @foreach($model->getTypes() as $key => $type)
                        <option value="{{$key}}" @if(isset($model->type) and $model->type === $key) selected @endif>{{$type}}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>

    </div>
    <div class="card-footer">
        <button type="submit" class="btn btn-primary">{{__('table.propositions.save')}}</button>
    </div>
</form>
