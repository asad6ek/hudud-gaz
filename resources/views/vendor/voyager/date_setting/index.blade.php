@extends('voyager::master')
@section('head_css')
    <link rel="stylesheet" href="{{asset('css/datatables/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/datatables/responsive.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/datatables/buttons.bootstrap4.min.css')}}">
@stop

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid" @if(user_role('region')) style="margin: auto; max-width: 90%" @endif>
            <div class="row">
                <div class="col-12">
                    @include('voyager::alerts')
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col">
                                    <ul class="nav nav-pills">
                                        <li class="nav-item"><a class="nav-link active" href="#individuals"
                                                                data-toggle="tab">{{__('table.date_settings.holidays')}}</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#legal_entity" data-toggle="tab">
                                                {{__('table.date_settings.working_days')}}</a>
                                        </li>
                                    </ul>
                                </div>

                                @can('add_date_settings')
                                    <div class="col">
                                        <a class="btn  btn-success float-md-right"
                                           href="{{route('admin.date_settings.create')}}">{{__('table.date_settings.create')}}</a>
                                    </div>
                                @endcan
                            </div>
                        </div>

                        <div class="card-body">
                            <div class="tab-content">
                                <div class="tab-pane active" id="individuals">

                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>{{__('table.date_settings.name')}}</th>

                                            <th>{{__('table.date_settings.from')}}</th>

                                            <th>{{__('table.date_settings.to')}}</th>

                                            @can('delete_date_settings')
                                                <th></th>
                                            @endcan
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($models[1] ?? [] as $model)
                                            <tr>
                                                <td>{{$model->name}}</td>
                                                <td>{{$model->from}}</td>
                                                <td>{{$model->to}}</td>
                                                @can('delete_date_settings')
                                                    <td>
                                                        <form method="post"
                                                            action="{{route('admin.date_settings.delete',['date_setting' => $model->id])}}">

                                                            @csrf
                                                            @method('DELETE')
                                                            <button type="submit" class=" btn  btn-danger"
                                                                    data-toggle="tooltip"
                                                                    title="{{__('table.date_settings.delete')}}">
                                                                <i class="fas fa-trash-alt"></i>
                                                            </button>
                                                        </form>
                                                    </td>
                                                @endcan
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane" id="legal_entity">
                                    <table id="example2" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>{{__('table.date_settings.name')}}</th>

                                            <th>{{__('table.date_settings.from')}}</th>

                                            <th>{{__('table.date_settings.to')}}</th>
                                            @can('delete_date_settings')
                                                <td>
                                                </td>
                                            @endcan
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($models[2] ?? [] as $model)
                                            <tr>
                                                <td>{{$model->name}}</td>
                                                <td>{{$model->from}}</td>
                                                <td>{{$model->to}}</td>
                                                @can('delete_date_settings')
                                                    <td>
                                                        <form method="post"
                                                            action="{{route('admin.date_settings.delete',['date_setting' => $model->id])}}">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button type="submit" class=" btn  btn-danger"
                                                                    data-toggle="tooltip"
                                                                    title="{{__('table.date_settings.delete')}}">
                                                                <i class="fas fa-trash-alt"></i>
                                                            </button>

                                                        </form>
                                                    </td>
                                                @endcan
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
@section('body_js')
    @include('voyager::settings.scripts')
<script>
    $(function () {
        $("#example1").DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            // "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
        $("#example2").DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            // "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

    });
</script>
@stop
