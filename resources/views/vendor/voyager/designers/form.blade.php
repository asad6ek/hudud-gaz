<form action="{{$action}}" method="post" enctype="multipart/form-data">

    @csrf
    @method($method)
    @includeFirst(['voyager::components.errors'],['errors' => $errors])

    <div class="card-body">
        <div class="form-group">
            <label for="organization">{{__('table.regions.org_name')}}</label>
            <input type="text" name="organization" value="{{$model->organization}}" class="form-control" id="organization" required>
        </div>

        <div class="form-group">
            <label for="leader">{{__('table.montage.reg_leader')}}</label>
            <input type="text" name="leader" value="{{$model->leader}}" class="form-control" id="leader" required>
        </div>

        <div class="form-group">
            <label for="address">{{__('table.montage.address')}}</label>
            <input type="text" name="address" value="{{$model->address}}" class="form-control" id="address" required>
        </div>

        <div class="form-group">
            <label for="phone_number">{{__('table.designers.phone_number')}}</label>
            <textarea name="phone_number" class="form-control" id="phone_number" required>{{$model->phone_number}}</textarea>
        </div>

        <div class="form-group data-form">
            <div>
                <label for="date_reg">{{__('table.montage.date_created')}}</label>
                <input type="date" name="date_reg" value="{{$model->date_reg}}" class="form-control" id="date_reg" required>
            </div>
            <div>
                <label for="date_end">{{__('table.montage.date_expired')}}</label>
                <input type="date" name="date_end" value="{{$model->date_end}}" class="form-control" id="date_end" required>
            </div>
        </div>

        <div class="form-group">
            <label for="document">{{__('table.designers.document')}}</label>
            <div class="custom-file">
                <input type="file" name="document" id="document" class="custom-file-input" @if($method == "POST") required @endif>
                <label class="custom-file-label" for="document">
                    <span id="path_label">{{__('table.montage.file_upload')}}</span>
                    <div class="btn btn-info"><i class="far fa-file-pdf"></i></div>
                </label>
            </div>
        </div>

    </div>
    <div class="card-footer">
        <button type="submit" class="btn btn-primary mr-2">{{__('table.montage.button_save')}}</button>
        <a href="{{route('admin.designers.index')}}" class="btn btn-outline-secondary">{{__('table.montage.button_previous')}}</a>
        <button type="reset" class="btn btn-default float-right">{{__('table.general_settings.button_clear')}}</button>
    </div>
</form>

<style>
    .data-form {
        display: flex;
        justify-content: space-between;
    }

    .data-form > div {
        flex-basis: 45%;
    }

    .custom-file label::after {
        content: none;
    }

    .custom-file label {
        display: flex;
        align-items: center;
        justify-content: space-between;
        background-color: transparent;
        padding-right: 0;
    }
</style>

@section('body_js')
    <script>
        $('#document').change(function(input) {
           try {
               $('#path_label').text(input.target.files[0].name)
           } catch (e) {}
        })
    </script>
@stop
