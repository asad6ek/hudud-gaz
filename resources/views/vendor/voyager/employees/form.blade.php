<form action="{{$action}}" method="post" enctype="multipart/form-data">
    @csrf
    @method($method)
    @includeFirst(['voyager::components.errors'],['errors' => $errors])

    <div class="card-body">
        <div class="form-group row">
            <label for="name" class="col-3">{{__('table.employees.firstname')}}</label>
            <div class="col-9">
                <input type="text" name="name" value="{{$model->name}}" class="form-control" id="name">
            </div>
        </div>

        <div class="form-group row">
            <label for="lastname" class="col-3">{{__('table.employees.lastname')}}</label>
            <div class="col-9">
                <input type="text" name="lastname" value="{{$model->lastname}}" class="form-control" id="lastname">
            </div>
        </div>

        <div class="form-group row">
            <label for="patronymic" class="col-3">{{__('table.employees.second_name')}}</label>
            <div class="col-9">
                <input type="text" name="patronymic" value="{{$model->patronymic}}" class="form-control" id="patronymic">
            </div>
        </div>

        <div class="form-group row">
            <label for="email" class="col-3">{{__('table.employees.email')}}</label>
            <div class="col-9">
                <input type="email" name="email" value="{{$model->email ?? ''}}" class="form-control" id="email" required>
            </div>
        </div>

        <div class="form-group row">
            <label for="password" class="col-3">{{__('table.employees.password')}}</label>
            <div class="col-9">
                <input type="password" name="password" class="form-control" id="password" value="">
            </div>
        </div>

        <div class="form-group row">
            <label for="role" class="col-3">{{__('table.employees.role')}}</label>
            <div class="col-9">
                <select class="custom-select" name="role_id" id="role" onchange="appender();">
                    @foreach($roles as $key => $role)
                        <option value="{{$key}}"
                                @if ($key == $model->role_id)selected @endif >{{$role}}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row" style="display: none" id="show_types">
            <div class="col-md-3">
                <label for="type_value" id="type_name">{{__('table.employees.contestants')}} </label>
                <span>(<i> {{ $model->type }} </i>)</span>
            </div>
            <div class="col-md-9">
                <select class="custom-select" name="firm_id" id="type_value">
                    <option value="" id="option_default">{{__('table.employees.contestants')}}</option>
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label for="locale" class="col-3">{{__('table.employees.locale')}}</label>
            <div class="col-9">
                <select class="custom-select" name="locale" id="locale">
                    @foreach($locales as $key => $value)
                        <option  value="{{$key}}"
                                 @if ($key == $model->locale)selected @endif >{{$value}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="position" class="col-3">{{__('table.employees.position')}}</label>
            <div class="col-9">
                <input type="text" name="position" value="{{$model->position}}" class="form-control" id="position">
            </div>
        </div>
    </div>
    <div class="card-footer">
        <button type="submit" class="btn btn-primary mr-2">{{__('table.propositions.save')}}</button>
        <a href="{{route('admin.employees.index')}}" class="btn btn-outline-danger">{{__('table.montage.button_previous')}}</a>
        <button type="reset" class="btn btn-outline-secondary float-right">@lang('table.btn_clear')</button>
    </div>
</form>
