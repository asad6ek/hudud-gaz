@extends('voyager::master')

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @include('voyager::alerts')
                    <div class="card">
                        <div class="card-header">
                            @can('add_users')
                                <a class="btn btn-success" href="{{route('admin.employees.create')}}" role="button">{{__('table.add')}}</a>
                            @endcan
                            <div class="card-tools mt-2">
                                <div class="input-group input-group-sm" style="width: 150px;">
                                    <input type="text" id="table_search" name="table_search" class="form-control float-right"
                                           placeholder="{{__('table.montage.search_field')}}">

                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-default">
                                            <i class="fas fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body table-responsive p-0">
                            <table class="table table-hover text-nowrap">
                                <thead>
                                <tr>
                                    <th>№</th>
                                    <th>{{__('table.employees.col_firstname')}}</th>
                                    <th>{{__('table.employees.col_lastname')}}</th>
                                    <th>{{__('table.employees.col_position')}}</th>
                                    <th>{{__('table.employees.col_email')}}</th>
                                    <th>{{__('table.employees.col_role')}}</th>
                                    <th></th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($models as $user)
                                    @if ($user->role_id == 1 || $user->role_id == 3)
                                        @continue
                                    @endif
                                    <tr>
                                        <td>{{$user->id}}</td>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->lastname}}</td>
                                        <td>{{$user->position}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>{{$user->role->display_name}}</td>
                                        <td>
                                            <form method="post" id="form_delete-{{$user->id}}"
                                                action="{{route('admin.employees.delete',['employee' =>$user->id])}}">
                                                @can('edit_users')
                                                    <a class="btn btn-warning" title="{{__('table.montage.button_edit')}}"
                                                       href="{{route('admin.employees.edit',['employee' =>$user->id])}}"
                                                       role="button"><i class="fas fa-pencil-alt"></i></a>
                                                @endcan
                                                @can('delete_users')
                                                @csrf
                                                @method('DELETE')
                                                <button type="button" onclick="confirmDelete({{$user->id}})" title="{{__('table.montage.button_delete')}}" class="btn btn-danger"
                                                        role="button"><i class="far fa-trash-alt"></i></button>
                                                    @endcan
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
@section('body_js')
<script>
    $('#table_search').keyup(function () {
        let qiymat = this.value.toLowerCase();
        $('tbody tr').filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(qiymat) > - 1);
        })
    });

    function confirmDelete(id) {
        Swal.fire({
            title: '{{__('table.employees.alert_message')}}',
            text: "",
            icon: 'warning',
            showCancelButton: true,
            cancelButtonColor: '#3085d6',
            confirmButtonColor: '#d33',
            cancelButtonText: '{{__('table.montage.button_no')}}',
            confirmButtonText: '{{__('table.montage.button_yes')}}',
            reverseButtons: true
        }).then((result) => {
            if (result.isConfirmed) {
                $(`#form_delete-${id}`).submit()
                Swal.fire({
                    title: '{{__('table.montage.deleting_finish')}}',
                    icon: 'success',
                    showConfirmButton: false,
                })
            }
        });
    }
</script>
@stop
