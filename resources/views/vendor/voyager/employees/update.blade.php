@extends('voyager::master')

@section('page_title', 'Ҳудудгаз таъминот Хоразм филиали')

@section('head_css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        .custom-file label::after {
            content: none;
        }

        .custom-file label {
            display: flex;
            align-items: center;
            justify-content: space-between;
            background-color: transparent;
            padding-right: 0;
        }
    </style>
@stop

{{--@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular') }}
    </h1>
@stop--}}

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    @include('voyager::alerts')

                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title"> </h3>
                        </div>
                        @include('voyager::employees.form',[
                            'model' => $model,
                            'method' => 'PUT',
                            'action' => route('admin.employees.update',['employee' => $model->id])
                        ])
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('body_js')
    <script>
        function appender()
        {
            let type = $('#role option:selected').text()
            let types = ['Туман', 'Лойихачи', 'Монтажчи']
            if (types.includes(type)) {

                $.ajax({
                    url: "/admin/employees/fetch/" + type,
                    type: "GET",
                    data: {_token: $('meta[name="csrf-token"]').attr('content')}
                }).done(function (result) {
                    let elem = $('#type_value')
                    if (type == 'Туман') type = 'Ташкилот'
                    $('#type_name').text(type + '{{__('table.choose_of')}}')
                    elem.html("<option value=\"\" id=\"option_default\">" + type + "{{__('table.choose_of')}}</option>")
                    elem.append(result['types'])
                    $('#show_types').css('display', 'flex')
                    elem.attr('required', true)
                })
            } else {
                $('#show_types').css('display', 'none');
                $('#type_value').attr('required', false);
            }
        }

        appender();

        function changeImg(input) {
            $('#img').attr('src', URL.createObjectURL(input.files[0]));
            $('#img_hint').html(input.files[0].name);
        }

        $('#reset').click(function() {
            $('#img').attr('src', "{{asset('img/avatar.svg')}}");
            $('#img_hint').html('{{__('table.general_settings.upload_image')}}');
        })

    </script>
@stop
