@extends('voyager::master')

@section('page_title', 'Ҳудудгаз таъминот Хоразм филиали')

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                @include('voyager::alerts')
                @include('voyager::components.errors')
                <div class="card">
                    <div class="card-header">
                        @can('add_users')
                            <button id="myBtn" class="btn btn-success" role="button">{{__('table.add')}}</button>
                        @endcan

                        <div class="card-tools mt-2">
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <input type="text" id="table_search" name="table_search" class="form-control float-right"
                                       placeholder="{{__('table.montage.search_field')}}">

                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-default">
                                        <i class="fas fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover text-nowrap">
                            <thead>
                                <tr>
                                    <th class="col-12">{{__('table.equipments.name')}}</th>
                                    <th style="width: 1px;"></th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($models as $model)
                                <tr>
                                    <td>{{$model->name}}</td>
                                    <td>
                                        <form method="post" id="form-{{$model->id}}"
                                            action="{{route('admin.equipments.delete',['equipment' =>$model->id])}}">
                                            @can('add_equipments_types')
                                               <a class="btn btn-info mr-2"
                                                   href="{{route('admin.equipments.show',['equipment' =>$model->id])}}"
                                                   role="button">{{__('table.equipments.add_type')}}</a>
                                            @endcan
                                            @can('edit_equipments')
                                                <button type="button" onclick="fillData({{$model->id}}, '{{$model->name}}')" class="btn btn-warning"
                                                        title="{{__('table.montage.button_edit')}}" role="button">
                                                    <i class="fas fa-pencil-alt"></i>
                                                </button>
                                            @endcan

                                                @if($model->id != 1)
                                            @can('delete_equipments')
                                            @csrf
                                            @method('DELETE')
                                            <button type="button" class="btn btn-danger"
                                                    role="button" onclick="remove('form-{{$model->id}}')" data-toggle="tooltip" title="{{__('table.montage.button_delete')}}"><i class="fas fa-trash-alt"></i>
                                            </button>
                                                @endcan
                                                @endif
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal fade" id="myModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body" style="padding:40px 50px;">
                                <form action="{{route('admin.equipments.store')}}" method="POST" id="form" onsubmit="submit.disabled = true">
                                    @csrf
                                    <input type="hidden" id="_method" name="_method" value="POST">
                                    <div class="form-group">
                                        <label for="name">{{__('table.equipments.name')}}</label>
                                        <input type="text" id="name" name="name" class="form-control" required>
                                    </div>

                                    @can('add_users')
                                        <input type="submit" id="submit" class="btn btn-success" value="{{__('table.add')}}">
                                    @endcan
                                    <button type="button" class="btn btn-danger btn-default pull-right" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span>{{__('table.activity_type.button_cancel')}}</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('body_js')
<script>
    $('#table_search').keyup(function () {
        let qiymat = this.value.toLowerCase();
        $('tbody tr').filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(qiymat) > - 1)
        })
    });

    $(document).ready(function() {
        $("#myBtn").click(function() {
            $('#form').attr('action', '{{route('admin.equipments.store')}}');
            $("#name").val(null);
            $("#_method").val("POST");

            $("#myModal").modal();
        });
    });

    function fillData(id, name) {
        let route = '{{route('admin.equipments.update', ['equipment' => 0])}}';
        $('#form').attr('action', route.substr(0, route.length - 1) + id);
        $("#name").val(name);
        $("#_method").val('PUT');

        $("#myModal").modal();
    }

    function remove(form) {
       Swal.fire({
        title: '{{__('table.equipments.alert_message')}}',
        text: "{{__('table.equipments.alert_text')}}",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#dd3333',
        cancelButtonColor: '#3085d6',
        confirmButtonText: '{{__('table.montage.button_yes')}}',
        cancelButtonText: '{{__('table.montage.button_no')}}',
        reverseButtons: true
       }).then((result) => {
            if (result.isConfirmed) {
                $(`#${form}`).submit()
                Swal.fire({
                    title: '{{__('table.montage.deleting_finish')}}',
                    icon: 'success',
                    showConfirmButton: false,
                })
            }
    })}
</script>
@stop
