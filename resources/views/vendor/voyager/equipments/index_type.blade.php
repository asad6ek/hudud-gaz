@extends('voyager::master')

@section('content')
    <style>
        td form {
            margin-top: -5px;
        }
    </style>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @include('voyager::alerts')
                    <div class="card">
                        <div class="card-header">
                         <button id="myBtn" class="btn btn-success">{{__('table.add')}}</button>

                            <div class="card-tools mt-2">
                                <div class="input-group input-group-sm" style="width: 150px;">
                                    <input type="text" id="table_search" name="table_search" class="form-control float-right"
                                           placeholder="{{__('table.montage.search_field')}}">
                                    <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-search"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card-body table-responsive p-0">
                            <table class="table table-hover text-nowrap">
                                <thead>
                                <tr>
                                    <th>{{__('table.equipments.type')}}</th>
                                    <th>{{__('table.equipments.order')}}</th>
                                    <th style="width: 1px;">{{__('table.propositions.action')}}</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($equipment_types as $type)
                                    <tr>
                                        <td class="col-6">{{$type->type}}</td>
                                        <td class="col-4">{{$type->order}}</td>
                                        <td class="col-2">
                                            <form method="post" id="form-{{$type->id}}"
                                                  action="{{route('admin.equipment_type.delete',['equipmentType' =>$type->id,'equipment'=>$equipment->id])}}">

                                                @can('edit_equipments_types')
                                                    <button onclick="fillData('{{$type->type}}', {{$type->order}}, {{$type->id}})" class="btn btn-warning" type="button" role="dialog"
                                                        data-toggle="tooltip" title="{{__('table.equipments.edit_type')}}"><i class="fas fa-pencil-alt"></i></button>
                                                @endcan
                                                @can('delete_equipments_types')
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="button" onclick="remove('form-{{$type->id}}')" class="btn btn-danger"
                                                            role="button" data-toggle="tooltip" title="{{__('table.equipments.delete_type')}}"
                                                            data-type="{{$type->type}}"
                                                            data-order="{{$type->order}}"

                                                    ><i class="fas fa-trash-alt"></i>
                                                    </button>
                                                @endcan
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="modal fade" id="myModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body" style="padding:40px 50px;">
                                        <form action="{{route('admin.equipment_type.add',['equipment'=>$equipment->id])}}" method="POST" id="form_forType" onsubmit="submit.disabled = true">
                                            @csrf
                                            <div class="form-group">
                                                <label for="type"><span class="glyphicon glyphicon-user"></span> {{__('table.equipments.add_types')}}</label>
                                                <input type="text" class="form-control" id="type" name="type" required>
                                                <label for="order"><span class="glyphicon glyphicon-user"></span> {{__('table.equipments.order')}}</label>
                                                <input type="number" class="form-control" id="order" name="order" required>
                                            </div>

                                            @can('add_users')
                                                <input type="submit" id="submit" class="btn btn-success" value="{{__('table.add')}}">
                                            @endcan
                                            <button type="button" class="btn btn-danger btn-default pull-right" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span>{{__('table.activity_type.button_cancel')}}</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('body_js')
<script>
    $('#table_search').keyup(function () {
        let qiymat = this.value.toLowerCase();
        $('tbody tr').filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(qiymat) > - 1)
        })
    });

    $(document).ready(function() {
        $("#myBtn").click(function() {
            $('#form_forType').attr('action', '{{route('admin.equipment_type.add',['equipment'=>$equipment->id])}}');
            $("#type").val('');
            $("#order").val(null)

            $("#myModal").modal();
        });
    });

    function fillData(type, order, id) {
        let route = '{{route('admin.equipment_type.update',['equipment'=>$equipment->id, 'equipmentType' => 0])}}';
        $('#form_forType').attr('action', route.substr(0, route.length - 1) + id);
        $("#type").val(type);
        $("#order").val(order);

        $("#myModal").modal();
    }

    function remove(form) {
        Swal.fire({
            title: '{{__('table.equipments.alert_message')}}',
            text: "{{__('table.equipments.alert_text')}}",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#dd3333',
            cancelButtonColor: '#3085d6',
            confirmButtonText: '{{__('table.montage.button_yes')}}',
            cancelButtonText: '{{__('table.montage.button_no')}}'
        }).then((result) => {
            if (result.isConfirmed) {
                $(`#${form}`).submit();
                Swal.fire({
                    title: '{{__('table.montage.deleting_finish')}}',
                    icon: 'success',
                    showConfirmButton: false,
                });
            }
        });
    }
</script>
@stop
