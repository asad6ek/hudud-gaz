<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PDF</title>
    <style>
        * {
            padding: 0;
            margin: 0;
            box-sizing: border-box;
            font-size: 14px;
            font-family: 'DejaVu Sans', serif !important;
        }

        @page {
            size: A4;
        }

        body {
            text-align: justify;
            padding: 30px 30px 30px 60px;
        }

        .text-center {
            text-align: center;
        }

        .f-l {
            float: left;
        }

        .f-r {
            float: right;
        }

        .clear {
            clear: both;
        }

        .title {
            margin-bottom: 30px;
            letter-spacing: 5px;
            text-transform: uppercase;
        }

        .permission {
            margin-bottom: 30px;
            letter-spacing: 20px;
            text-transform: uppercase;
        }

        .text-right {
            text-align: right;
        }

        .tables {
            margin-top: 10px;
        }

        table {
            width: 100%;
            text-align: justify;
            font-weight: bold;
        }

        table:first-child {
            margin-bottom: 10px;
            margin-top: 20px;
        }

        td {
            width: 100%;
        }

        .td-left {
            width: 90%;
            margin-left: auto;
        }

        .mar {
            margin: 30px 0;
        }
    </style>
</head>
<body>
<div class="text-center">
    <h2 class="title">Табиий газ таъминотига улаш</h2>
    <div class="permission">Рухсатномаси</div>
    <div>№ {{$proposition->number}}</div>
</div>
<div class="mar">
    <div class="f-l">
        {{dateFixed($recommendation->crated_at)}}
    </div>
    <div class="f-r">
        {{$montage_firm->district()}}
    </div>
    <div class="clear"></div>
</div>
<ol>
    <li>
        Истеъмолчи номи: <strong><u>{{$recommendation->address}} ҳудудида жойлашган,
                {{$applicant->legal_entity_name}}га қарашли бино ва иншоатларни газлаштириш.</u></strong>
        <div class="text-center">
            \иншоат манзили\
        </div>
    </li>

    <li>
        <span>
            Газ тармоғига уланувчи асбоблар тури:
            <b>
            @foreach($equips as $equip)
                    {{$equip->type->equipment->name}} {{$equip->type->type}} (и/қ) - {{$equip->parameter}} дона@if(!$loop->last), @endif
            @endforeach.
            </b>
        </span>
        <div class="text-center">
            / тури, сони /
        </div>
    </li>
    <li>
        Газ ўлчагич асбоб:
        <b>
            @foreach($gas_meters as $gas_meter)
                {{$gas_meter->type}} - {{$gas_meter->number}} дона@if(!$loop->last), @endif
            @endforeach.
        </b>
        <div class="text-center">
            / тури, сони /
        </div>
    </li>
    <li>
        Табиий газдан фойдаланиш учун берилган рухсатнома; "{{$data['region_name']}}" ГТФнинг <b>{{$recommendation->created_at->format('Y')}}-йил {{$recommendation->created_at->format('n')}}-январ {{$recommendation->id}}/{{$proposition->id}}-сони</b> хатига асосан,
        <b>{{$recommendation->gas_consumption}} м<sup>3</sup></b> йилига.
        <div class="text-center">
            / қайси ташкилот ва қайси ҳажмда /
        </div>
    </li>
    <li>
        Захира ёқилғиларининг тури ва ҳажми: ўтин ва кўмир: ___ - ___
    </li>
    <li>
        Лойиҳа-пешҳисоб ишлаган ташкилот номи: <b>"{{$designer->organization}}" МЧЖ</b>.
        Пудратчи ташкилот номи: <b>"{{$montage_firm->full_name}}" МЧЖ</b>.
    </li>
    <li>
        Тоширилган техник ҳужжатларни топширилганлиги ҳақида маълумот: 2 нусахада ишлаб чиқариш бўлимига
    </li>
    <li>
        а/ келтирувчи газ қувури: <b>{{$recommendation->pipe_size}} мм @if($recommendation->pipe_type == 1) паст @else юқори @endif босимли</b>
        б/ ҳовли газ қувури: <b>{{$recommendation->pipe_size2}} мм @if($recommendation->pipe_type == 1) паст @else юқори @endif босимли</b>
        Истеъмолчи табиий газдан фойдаланиш бўйича ҳаракатдаги қонун ва қоидаларга роия қилишни ўз зиммасига олади: <b>Раҳбар</b>.
        <div class="text-center">
            давлат ёки хусусий кархона раҳбарининг фамилияси, имзоси ва муҳри.
        </div>
    </li>
    <li>
        Ўзбекистон Республикасининг Вазирлар Маҳкамасининг 22-сонли қарорига асосан 100% тўлов амалга оширилгандан кейин газ тармоғига улашга рухсат қилинади.
    </li>
    <li>
        Рухсатномада кўрсатилган газ асбобларининг сертификати ва техник шартдан ташқари қўшимча ўрнатилган газ асбобларини назорати шаҳар/тумангаз филиали зиммасига юкланади.
    </li>
</ol>
<div class="tables">
    <table>
        <tr>
            <td style="width: 120%">
                <div style="width: 75%">
                    "Худудгаз Хоразм" газ таъминоти филиали бош муҳандиси
                    <div class="text-right">
                        ___________ {{get_general_settings()['engineer_name'] ?? ''}}
                    </div>
                </div>
            </td>
            <td>
                <div class="td-left">
                    "Худудгаз Хоразм" газ таъминоти филиали, Техник ишлаб чиқариш бўлими етакчи муҳандиси
                    <div class="text-right">
                        ___________ {{get_general_settings()['tech_section'] ?? ''}}
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td style="width: 120%">
                <div style="width: 75%">
                    "Худудгаз Хоразм" газ таъминоти филиали, Юридик шахсларга табиий газ етказиб беришни назорат қилиш бўлими бошлиғи
                    <div class="text-right">
                        ___________ {{get_general_settings()['legal_section'] ?? ''}}
                    </div>
                </div>
            </td>
            <td>
                <div class="td-left">
                    "Худудгаз Метрология" филиали, Хоразм бўлими бошлиғи
                    <div class="text-right">
                        ___________ {{get_general_settings()['metrology'] ?? ''}}
                    </div>
                </div>
            </td>
        </tr>
    </table>
</div>
</body>
</html>
