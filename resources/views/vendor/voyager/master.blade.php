@php
    $user = Auth::user();
    $role_name = $user->role->name;
    #if (\Illuminate\Support\Str::startsWith(Auth::user()->avatar, 'http://') || \Illuminate\Support\Str::startsWith(Auth::user()->avatar, 'https://')) {
    #    $user_avatar = $user->avatar;
    #} else {
    #    $user_avatar = Voyager::image($user->avatar);
    #}
    // $locale = $user->get('locale');
@endphp
<!DOCTYPE html>
<html>
<head>
    <title>{{config('voyager.page_title')}}</title> {{--@yield('page_title', setting('admin.title') . " - " . setting('admin.description'))--}}
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <meta name="assets-path" content="{{ route('voyager.voyager_assets') }}"/>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/adminlte.min.css') }}">
    <link rel="stylesheet" href="{{asset('css/fontawesome/all.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/sweetalert2.css') }}">
    <link rel="stylesheet" href="{{ asset('css/summernote.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/default.css') }}">


    @if(in_array($role_name, ['region', 'designer', 'montage_firm']))
        <!-- Theme style -->
    @else
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Tempusdominus Bootstrap 4 -->
        <link rel="stylesheet" href="{{ asset('css/tempusdominus-bootstrap-4.min.css') }}">
        <!-- iCheck -->
        <link rel="stylesheet" href="{{ asset('css/icheck-bootstrap.min.css') }}">
        <!-- JQVMap -->
        <link rel="stylesheet" href="{{ asset('css/jqvmap.min.css') }}">
        <!-- Theme style -->

        <!-- overlayScrollbars -->
        <link rel="stylesheet" href="{{ asset('css/OverlayScrollbars.min.css') }}">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="{{ asset('css/daterangepicker.css') }}">
        <!-- summernote -->
    @endif

    @yield('head_css')

    <?php $admin_favicon = Voyager::setting('admin.icon_image', ''); ?>
    @if($admin_favicon == '')
        <link rel="shortcut icon" href="{{"/logo-icon.png"}}" type="image/png" size="128x128">
    @else
        <link rel="shortcut icon" href="{{ Voyager::image($admin_favicon) }}" type="image/png">
    @endif
</head>
<body @if(in_array($role_name, ['region', 'designer', 'montage_firm'])) class="hold-transition layout-top-nav" @endif>
<div class="wrapper">
    <!-- Preloader -->
    <div class="preloader flex-column justify-content-center align-items-center">
        <img class="animation__shake" src="{{asset('img/logo.png')}}" alt="HududGaz" height="60" width="260">
    </div>

    <!-- Navbar -->
    @if(in_array($role_name, ['region', 'designer', 'montage_firm']))
        @include('voyager::dashboard.second_navbar', ['role' => $role_name])
    @else
        @include('voyager::dashboard.navbar')

        @include('voyager::dashboard.sidebar')
    @endif

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

{{--        @include('voyager::dashboard.header')--}}

{{--        @dd(menu_items(menu($role_name, '_json')))--}}
        @yield('page_header')
        @yield('content')
    </div>

{{--    @dd( menu_items(menu($user->role->name, '_json')))--}}
</div>

@if(in_array($role_name, ['region', 'designer', 'montage_firm']))
    <!-- jQuery -->
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('js/adminlte.min.js') }}"></script>
@else
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('js/adminlte.min.js') }}"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="{{ asset('js/jquery-ui.min.js') }}"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script>

    <!-- ChartJS -->
    <script src="{{ asset('js/Chart.min.js') }}"></script>
    <!-- Sparkline -->
    <script src="{{ asset('js/sparkline.js') }}"></script>
    <!-- JQVMap -->
    <script src="{{ asset('js/jquery.vmap.min.js') }}"></script>
    <script src="{{ asset('js/jquery.vmap.usa.js') }}"></script>
    <!-- jQuery Knob Chart -->
    <script src="{{ asset('js/jquery.knob.min.js') }}"></script>
    <!-- daterangepicker -->
    <script src="{{ asset('js/moment.min.js') }}"></script>
    <script src="{{ asset('js/daterangepicker.js') }}"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="{{ asset('js/tempusdominus-bootstrap-4.min.js') }}"></script>
    <!-- Summernote -->
    <script src="{{ asset('js/summernote-bs4.min.js') }}"></script>
    <!-- overlayScrollbars -->
    <script src="{{ asset('js/jquery.overlayScrollbars.min.js') }}"></script>

@endif
    <script src="{{ asset('js/sweetalert2.js') }}"></script>

@yield('body_js')
<script src="{{asset('js/default.js')}}"></script>
</body>
</html>
