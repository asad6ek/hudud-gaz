<form action="{{$action}}" method="post" enctype="multipart/form-data">

    @csrf
    @method($method)
    @includeFirst(['voyager::components.errors'],['errors' => $errors])

    <div class="card-body">
        <input type="hidden" name="firm_id" value="{{$montage_firm->id}}" required>

        <div class="form-group row">
            <label for="statement_number" class="col-2">{{__('table.fitter.statement_number')}}</label>
            <div class="col-10">
                <input type="number" name="statement_number" value="{{$model->statement_number}}" class="form-control" id="statement_number" required>
            </div>
        </div>

        <div class="form-group row">
            <label for="first_name" class="col-2">{{__('table.fitter.first_name')}}</label>
            <div class="col-10">
                <input type="text" name="first_name" value="{{$model->first_name}}" class="form-control" id="first_name" required>
            </div>
        </div>

        <div class="form-group row">
            <label for="second_name" class="col-2">{{__('table.fitter.second_name')}}</label>
            <div class="col-10">
                <input type="text" name="second_name" value="{{$model->second_name}}" class="form-control" id="second_name" required>
            </div>
        </div>

        <div class="form-group row">
            <label for="last_name" class="col-2">{{__('table.fitter.last_name')}}</label>
            <div class="col-10">
                <input type="text" name="last_name" value="{{$model->last_name}}" class="form-control" id="last_name" required>
            </div>
        </div>

        <div class="form-group row">
            <label for="diploma_number" class="col-2">{{__('table.fitter.diploma_number')}}</label>
            <div class="col-10">
                <input type="number" name="diploma_number" value="{{$model->diploma_number}}" class="form-control" id="diploma_number" required>
            </div>
        </div>

        <div class="form-group row">
            <label for="passport_series" class="col-2">{{__('table.fitter.passport_series')}}</label>
            <div class="col-10">
                <input type="text" name="passport_series" value="{{$model->passport_series}}" class="form-control" id="passport_series" required>
            </div>
        </div>

        <div class="form-group row">
            <label for="specialization" class="col-2">{{__('table.fitter.specialized')}}</label>
            <div class="col-10">
                <input type="text" name="specialization" value="{{$model->specialization}}" class="form-control" id="specialization" required>
            </div>
        </div>

        <div class="form-group row">
            <label for="function" class="col-2">{{__('table.fitter.function')}}</label>
            <div class="col-10">
                <input type="text" name="function" value="{{$model->function}}" class="form-control" id="function" required>
            </div>
        </div>

        <div class="form-group row">
            <label for="experience" class="col-2">{{__('table.fitter.experience')}}</label>
            <div class="col-10">
                <input type="text" name="experience" value="{{$model->experience}}" class="form-control" id="experience" required>
            </div>
        </div>

        <div class="form-group data-form">
            <div>
                <label for="date_contract">{{__('table.fitter.date_contract')}}</label>
                <input type="date" name="date_contract" value="{{$model->date_contract}}" class="form-control" id="date_contract" required>
            </div>
            <div>
                <label for="date_contract_end">{{__('table.fitter.date_contract_end')}}</label>
                <input type="date" name="date_contract_end" value="{{$model->date_contract_end}}" class="form-control" id="date_contract_end" required>
            </div>
        </div>

        <div class="form-group">
            <label for="path">{{__('table.fitter.document')}}</label>
            <div class="custom-file">
                <input type="file" name="path" class="custom-file-input" id="path" @if($method == "POST") required @endif>
                <label class="custom-file-label" for="path">
                    <span id="path_label">{{__('table.montage.file_upload')}}</span>
                    <div class="btn btn-info"><i class="far fa-file-pdf"></i></div>
                </label>
            </div>
        </div>
    </div>
    <div class="card-footer">
        <button type="submit" class="btn btn-primary mr-2">{{__('table.montage.button_save')}}</button>
        <a href="{{route('admin.montage_employees.index', ['montage_firm' => $montage_firm])}}" type="submit" class="btn btn-outline-secondary">{{__('table.montage.button_previous')}}</a>
        <button type="reset" id="reset_button" class="btn btn-default float-right">{{__('table.general_settings.button_clear')}}</button>
    </div>
</form>

@section('body_js')
    <script>
        document.getElementById('path').onchange = function(input) {
            try {
                $('#path_label').text(input.target.files[0].name)
            } catch (e) {}
        }

        $("#reset_button").click(function () {
            $('#path_label').text('{{__('table.montage.file_upload')}}');
        });
    </script>
@stop
