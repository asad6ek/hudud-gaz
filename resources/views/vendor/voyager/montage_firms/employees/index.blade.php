@extends('voyager::master')

@section('page_title', 'Ҳудудгаз таъминот Хоразм филиали')

@section('content')
    <style>
        .for-delete-button {
            margin-top: -5px;
        }
    </style>
    <section class="content">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    @include('voyager::alerts')
                    <div class="card">

                        <div class="card-header">
                            @can('add_users')
                                <a class="btn btn-success"
                                   href="{{route('admin.montage_employees.create', ['montage_firm' => $montage_firm])}}" role="button">
                                    {{__('table.fitter.button_add')}}
                                </a>
                            @endcan
                            <div class="card-tools mt-2">
                                <div class="input-group input-group-sm" style="width: 150px;">
                                    <input type="text" id="table_search" name="table_search" class="form-control float-right"
                                           placeholder="{{__('table.montage.search_field')}}">

                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-default">
                                            <i class="fas fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="card-body table-responsive p-0">
                            <table class="table table-hover text-nowrap">
                                <thead>
                                <tr>
                                    <th>№</th>
                                    <th>{{__('table.fitter.col_organization')}}</th>
                                    <th>{{__('table.fitter.col_statement')}}</th>
                                    <th>{{__('table.fitter.col_name')}}</th>
                                    <th>{{__('table.fitter.col_function')}}</th>
                                    <th>{{__('table.fitter.col_experience')}}</th>
                                    <th>{{__('table.fitter.col_period_activity')}}</th>
                                    <th></th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($models as $model)
                                    <tr>
                                        <td>{{$loop->index + 1}}</td>
                                        <td>{{$montage_firm->shortname}}</td>
                                        <td>{{$model->statement_number}}</td>
                                        <td>{{$model->first_name[0]}}.{{$model->second_name[0]}}.{{$model->last_name}}</td>
                                        <td>{{$model->function}}</td>
                                        <td>{{$model->experience}}</td>
                                        <td>{{dateFixed($model->date_contract)}} - {{dateFixed($model->date_contract_end)}}</td>
                                        <td>
                                            <form method="post" id="form_delete-{{$model->id}}" class="for-delete-button"
                                                  action="{{route('admin.montage_employees.delete', ['montage_firm' => $montage_firm, 'employee' => $model])}}">

                                                @can('edit_equipments')
                                                    <a class="btn btn-warning" title="{{__('table.montage.button_edit')}}"
                                                       href="{{route('admin.montage_employees.edit', ['montage_firm' => $montage_firm, 'employee' => $model])}}"
                                                       role="button"><i class="fas fa-pencil-alt"></i></a>
                                                @endcan

                                                @can('delete_equipments')
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="button" class="btn btn-danger"
                                                            role="button" onclick="confirm({{$model->id}})" data-toggle="tooltip"
                                                            title="{{__('table.montage.button_delete')}}"> <i class="fas fa-trash-alt"></i>
                                                    </button>
                                                @endcan
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('body_js')
<script>
    $('#table_search').keyup(function () {
        let qiymat = this.value.toLowerCase();
        $('tbody tr').filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(qiymat) > - 1)
        })
    });

    function confirm(id) {
        Swal.fire({
            title: '{{__('table.fitter.delete_message')}}',
            text: "",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            confirmButtonText: '{{__('table.montage.button_yes')}}',
            cancelButtonText: '{{__('table.montage.button_no')}}'
        }).then((result) => {
            if (result.isConfirmed) {
                $(`#form_delete-${id}`).submit()
                Swal.fire({
                    title: '{{__('table.montage.deleting_finish')}}',
                    icon: 'success',
                    showConfirmButton: false,
                })
            }
        });
    }
</script>
@stop
