@extends('voyager::master')
@section('page_title', 'Ҳудудгаз таъминот Хоразм филиали')
@section('head_css')
    <style>
        .data-form {
            display: flex;
            justify-content: space-between;
        }

        .data-form > div {
            flex-basis: 45%;
        }

        .custom-file label::after {
            content: none;
        }

        .custom-file label {
            display: flex;
            align-items: center;
            justify-content: space-between;
            padding-right: 0;
        }
    </style>
@stop

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    @include('voyager::alerts')

                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title"> </h3>
                        </div>

                        @include('voyager::montage_firms.employees.form',[
                            'models' => $model,
                            'method' => 'PUT',
                            'action' => route('admin.montage_employees.update', ['montage_firm' => $montage_firm->id, 'employee' => $model])
                        ])
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
