<style>
    .custom-file label::after {
        content: none;
    }

    .custom-file label {
        display: flex;
        align-items: center;
        justify-content: space-between;
        background-color: transparent;
        padding-right: 0;
    }
</style>

<form action="{{$action}}" method="post" enctype="multipart/form-data">
    @csrf
    @method($method)
    @includeFirst(['voyager::components.errors'],['errors' => $errors])

    <div class="card-body">
        <div class="row">
            <div class="form-group col-6 pr-5">
                <label for="full_name">{{__('table.montage.full_name')}}</label>
                <input type="text" name="full_name" value="{{$model->full_name}}" class="form-control" id="full_name" required>
            </div>

            <div class="form-group col-6 pl-5">
                <label for="shortname">{{__('table.montage.short_name')}}</label>
                <input type="text" name="shortname" value="{{$model->shortname}}" class="form-control" id="shortname" required>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-6 pr-5">
                <label for="leader">{{__('table.montage.reg_leader')}}</label>
                <input type="text" name="leader" value="{{$model->leader}}" class="form-control" id="leader" required>
            </div>

            <div class="form-group col-6 pl-5">
                <label for="taxpayer_stir">{{__('table.montage.taxpayer_stir')}}</label>
                <input type="number" name="taxpayer_stir" value="{{$model->taxpayer_stir}}" class="form-control" id="taxpayer_stir" required>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-6 pr-5">
                <label for="record_number">{{__('table.montage.record_number')}}</label>
                <input type="number" name="record_number" value="{{$model->record_number}}" class="form-control" id="record_number" required>
            </div>

            <div class="form-group col-6 pl-5">
                <label for="reg_number">{{__('table.montage.reg_number')}}</label>
                <input type="text" name="reg_number" value="{{$model->reg_number}}" class="form-control" id="reg_number" required>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-6 pr-5">
                <label for="region">{{__('table.montage.region_name')}}</label>
                <select class="custom-select" name="region" id="region" required>
                    <option value="">{{__('table.montage.select_hint')}}</option>
                    {{$model->region}}
                    @foreach($regions as $key => $region)
                        <option value="{{$key}}"
                                @if ($key == $model->region) selected @endif
                        >
                            {{$region}}
                        </option>
                    @endforeach
                </select>
            </div>

            <div class="form-group col-6 pl-5">
                <label for="address">{{__('table.montage.address')}}</label>
                <input type="text" name="address" value="{{$model->address}}" class="form-control" id="address" required>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-6 pr-5">
                <label for="legal_form">{{__('table.montage.legal_form')}}</label>
                <input type="text" name="legal_form" value="{{$model->legal_form}}" class="form-control" id="legal_form" required>
            </div>

            <div class="form-group col-6 pl-5">
                <label for="given_by">{{__('table.montage.given_by')}}</label>
                <input type="text" name="given_by" value="{{$model->given_by}}" class="form-control" id="given_by" required>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-6 pr-5">
                <label for="date_created">{{__('table.montage.date_created')}}</label>
                <input type="date" name="date_created" value="{{$model->date_created}}" class="form-control" id="date_created" required>
            </div>
            <div class="form-group col-6 pl-5">
                <label for="date_expired">{{__('table.montage.date_expired')}}</label>
                <input type="date" name="date_expired" value="{{$model->date_expired}}" class="form-control" id="date_expired" required>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-6 pr-5">
                <label for="permission_to">{{__('table.montage.permissions_to')}}</label>
                <textarea name="permission_to" class="form-control" id="permission_to"
                          style="resize: none" required>{{$model->permission_to}}</textarea>
            </div>

            <div class="form-group col-6 pl-5">
                <label for="implement_for">{{__('table.montage.implements_for')}}</label>
                <textarea name="implement_for" id="implement_for" class="form-control"
                          style="resize: none" required>{{$model->implement_for}}</textarea>
            </div>
        </div>

        <div class="form-group">
            <label for="path">{{__('table.designers.document')}}</label>
            <div class="custom-file">
                <input type="file" name="path" class="custom-file-input" id="path" @if($method == "POST") required @endif>
                <label class="custom-file-label" for="path">
                    <span id="path_label">{{__('table.montage.file_upload')}}</span>
                    <div class="btn btn-info"><i class="far fa-file-pdf"></i></div>
                </label>
            </div>
        </div>

    </div>
    <div class="card-footer">
        <button type="submit" class="btn btn-primary mr-2">{{__('table.montage.button_save')}}</button>
        <a href="{{route('admin.montage_firms.index')}}" class="btn btn-outline-secondary">{{__('table.montage.button_previous')}}</a>
        <button type="reset" id="reset_button" class="btn btn-default float-right">{{__('table.general_settings.button_clear')}}</button>
    </div>
</form>

@section('body_js')
    <script>
        $('#path').change(function(input) {
           try {
               $('#path_label').text(input.target.files[0].name)
           } catch (e) {}
        });

        $("#reset_button").click(function () {
           $('#path_label').text('{{__('table.montage.file_upload')}}');
        });
    </script>
@stop
