@extends('voyager::master')

@section('page_title', 'Ҳудудгаз таъминот Хоразм филиали')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    @include('voyager::alerts')

                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title"> </h3>
                        </div>

                        @include('voyager::montage_firms.form',[
                            'models' => $model,
                            'method' => 'PUT',
                            'action' => route('admin.montage_firms.update', ['montage_firm' => $model]) //$model
                        ])
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
