@extends('voyager::master')
@section('head_css')
    <link rel="stylesheet" href="{{asset('css/datatables/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/datatables/responsive.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/datatables/buttons.bootstrap4.min.css')}}">
@stop

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @include('voyager::alerts')
                    <div class="card">
                        <div class="card-body pt-3">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>{{__('table.projects.tech_condition')}}</th>
                                        <th>{{__('table.propositions.district')}}</th>
                                        <th>{{__('table.propositions.customer')}}</th>
                                        <th>{{__('table.tech_condition')}}</th>
                                        <th> {{__('table.projects.project')}}</th>
                                        <th>{{__('table.montages.montages_table')}}</th>
                                        @can('edit_montages')
                                        <th></th>
                                        @endcan
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($models as $model)
                                    <tr>
                                        <td>{{$model->id}}</td>
                                        <td>{{$model->proposition->getRegion()}}</td>
                                        <td>{{$model->proposition->applicantable->name}}</td>
                                        <td>
                                            <a target="new"
                                               href="{{asset('storage/tech_conditions/'.$model->path)}}">
                                                {{__('table.tech.read_tech')}}
                                            </a>
                                        </td>
                                        <td>
                                            <a target="new"
                                               href="{{route('admin.montages.show', ['montage' => $model])}}">
                                                {{__('table.projects.project_read')}}
                                            </a>
                                        </td>

                                        <td>
                                            <a target="new"
                                               href="{{asset('storage/montages/'.$model->montage->path)}}">
                                                {{__('table.montages.read')}}
                                            </a>
                                        </td>

                                        @can('edit_montages')
                                            <td>
                                                @if($model->proposition->status === 18)
                                                <a class="btn btn-success" data-toggle="tooltip"
                                                   title="{{__('table.rec.confirm')}}"
                                                   href="{{route('admin.montages.edit',['montage' => $model->id])}}">
                                                    <i class="fas fa-check"></i>
                                                </a>
                                                <button class="btn btn-danger" onclick="write_comment({{$model->id}})" title="{{__('table.rec.back')}}">
                                                    <i class="fas fa-undo-alt"></i>
                                                </button>
                                                @else
                                                @can('add_montages')
                                                    <form action="{{route('admin.montages.upload', ['tech' => $model->id])}}" method="post"
                                                          id="form-{{$model->id}}" target="_blank" enctype="multipart/form-data">
                                                        @csrf
                                                        <input type="file" name="montage" id="file-{{$model->id}}"
                                                               onchange="uploadPdf('form-{{$model->id}}')" hidden>

                                                        <label for="file-{{$model->id}}" class="btn btn-info" title="{{__('table.btn_upd')}}">
                                                            <i class="fas fa-upload"></i>
                                                        </label>
                                                    </form>
                                                @endcan
                                                @endif
                                            </td>
                                        @endcan
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
@section('body_js')
    @include('voyager::settings.scripts')
    <script>
        $("#example1").DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            // "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');


        function write_comment(id)
        {
            Swal.fire({
                title: "{{__('table.montages.alert_title')}}",
                input: 'text',
                inputAttributes: {
                    autocapitalize: 'off'
                },
                showCancelButton: true,
                confirmButtonText: "@lang('table.btn_send')",
                cancelButtonText: "@lang('table.btn_close')",
                showLoaderOnConfirm: true,
                preConfirm: (comment) => {
                    $.ajax({
                        url: '/admin/montages/' + id + '/back',
                        type: 'POST',
                        data: {_token: $('meta[name="csrf-token"]').attr('content'), comment: comment}
                    }).done(function() {
                        location.reload()
                    });
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                if (result.isConfirmed) {

                }
            })
        }

        function uploadPdf(form) {
            $(`#${form}`).submit();
            setTimeout(() => {
                location.reload();
            }, 1000);
        }
    </script>
@stop






