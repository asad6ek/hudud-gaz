@extends('voyager::master')

@section('content')
    <div class="card" @if(user_role('region')) style="margin: auto; max-width: 90%" @endif>
        <div class="card-header p-2">
            <ul class="nav nav-pills">
                <li class="nav-item"><a class="nav-link active" href="#general" data-toggle="tab">{{__('table.profile.main_changes')}}</a></li>
                <li class="nav-item"><a class="nav-link" href="#password" data-toggle="tab">{{__('table.profile.change_pass')}}</a></li>
            </ul>
        </div>
        <div class="card-body">
            @include('voyager::components.errors')
            <div class="tab-content">
                <div class="tab-pane active" id="general">
                    <form class="form-horizontal" action="{{ route('admin.profile.update', ['user' => $user]) }}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group row">
                            <label for="email" class="col-sm-2 col-form-label">{{__('table.regions.col_org_email')}}</label>
                            <div class="col-sm-10">
                                <input type="email" name="email" class="form-control" id="email"
                                       placeholder="{{__('table.regions.email')}}" value="{{$user->email}}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-sm-2 col-form-label">{{__('table.employees.firstname')}}</label>
                            <div class="col-sm-10">
                                <input type="text" name="name" class="form-control" id="name"
                                       placeholder="{{__('table.employees.firstname')}}" value="{{$user->name}}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputLastname" class="col-sm-2 col-form-label">{{__('table.employees.lastname')}}</label>
                            <div class="col-sm-10">
                                <input type="text" name="lastname" class="form-control" id="inputLastname"
                                       placeholder="{{__('table.employees.lastname')}}" value="{{$user->lastname}}" >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPatronymic" class="col-sm-2 col-form-label">{{__('table.employees.patronymic')}}</label>
                            <div class="col-sm-10">
                                <input type="text" name="patronymic" class="form-control" id="inputPatronymic"
                                       placeholder="{{__('table.employees.second_name')}}" value="{{$user->patronymic}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPosition" class="col-sm-2 col-form-label">{{__('table.employees.position')}}</label>
                            <div class="col-sm-10">
                                <input type="text" name="position" class="form-control" id="inputPosition"
                                       placeholder="{{__('table.employees.position')}}" value="{{$user->position}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="locale" class="col-sm-2 col-form-label">{{__('table.employees.locale')}}</label>
                            <div class="col-sm-10">
                                <select class="custom-select" name="locale" id="locale">
                                    @foreach($locales as $key => $value)
                                        <option name="locale" value="{{$key}}"
                                                @if ($key == $user->locale)selected @endif >{{$value}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-2">
                                <img src="{{setImage(auth()->user())}}" id="output" alt="User profile picture" class="profile-user-img img-fluid img-circle" style="width: 100px; height: auto">
                            </div>
                            <div class="col-sm-10 my-auto">
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input name="avatar" value="" type="file" class="custom-file-input" id="exampleInputFile" onchange="loadFile(event)" >
                                        <label class="custom-file-label" for="exampleInputFile" id="label_for_image">{{__('table.general_settings.upload_image')}}</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="offset-sm-2 col-sm-10">
                                <button type="submit" class="btn btn-primary mr-2">{{__('table.montage.button_save')}}</button>
                                <a href="{{route('admin.employees.index')}}" class="btn btn-outline-secondary">{{__('table.btn_cancel')}}</a>
                                <button type="reset" id="reset_button" class="btn btn-default float-right">{{__('table.general_settings.button_clear')}}</button>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="tab-pane" id="password">
                    <form class="form-horizontal" action="{{ route('admin.profile.update', ['user' => $user]) }}" method="post">
                        @csrf
                        @method('PUT')
                        <div class="form-group row">
                            <label for="inputName" class="col-sm-2 col-form-label">{{__('table.profile.old_password')}}</label>
                            <div class="col-sm-10">
                                <input type="password" name="old_password" class="form-control" id="inputName"
                                       placeholder="{{__('table.profile.old_password')}}" value="{{old('old_password')}}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail" class="col-sm-2 col-form-label">{{__('table.profile.new_password')}}</label>
                            <div class="col-sm-10">
                                <input type="password" name="new_password" class="form-control" id="inputEmail"
                                       placeholder="{{__('table.profile.new_password')}}" value="{{old('new_password')}}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputName2" class="col-sm-2 col-form-label">{{__('table.profile.pass_confirm')}}</label>
                            <div class="col-sm-10">
                                <input type="password" name="confirm_new_password" class="form-control" id="inputName2"
                                       placeholder="{{__('table.profile.confirming_pass')}}" value="{{old('confirm_new_password')}}" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="offset-sm-2 col-sm-10">
                                <button type="submit" class="btn btn-primary mr-2">{{__('table.montage.button_save')}}</button>
                                <a href="{{route('admin.employees.index')}}" class="btn btn-outline-secondary">{{__('table.btn_cancel')}}</a>
                                <button type="reset" class="btn btn-default float-right">{{__('table.general_settings.button_clear')}}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@section('body_js')
    <script>
        var loadFile = function(event) {
            let image = document.getElementById('output');
            image.src = URL.createObjectURL(event.target.files[0]);

            let label = document.getElementById('label_for_image');
            label.innerHTML = event.target.files[0].name;
        }

        $('#reset_button').click(function () {
            $('#label_for_image').text('{{__('table.general_settings.upload_image')}}');
        });
    </script>
@stop
