@extends('voyager::master')
@section('head_css')
    <link rel="stylesheet" href="{{asset('css/datatables/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet"
          href="{{asset('css/datatables/responsive.bootstrap4.min.css')}}">
    <link rel="stylesheet"
          href="{{asset('css/datatables/buttons.bootstrap4.min.css')}}">
@stop

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @include('voyager::alerts')
                    <div class="card">

                        <!-- /.card-header -->
                        <div class="card-body pt-3">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>{{__('table.projects.tech_condition')}}</th>
                                    <th>{{__('table.propositions.district')}}</th>
                                    <th>{{__('table.propositions.customer')}}</th>
                                    <th>{{__('table.tech_condition')}}</th>
                                    <th> {{__('table.projects.project')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($models as $model)
                                    <tr>
                                        <td>{{$model->id}}</td>
                                        <td>{{$model->proposition->getRegion()}}</td>
                                        <td>{{$model->proposition->applicantable->name}}</td>
                                        <td>
                                            <a target="new"
                                               href="{{asset('storage/tech_conditions/'.$model->path)}}">
                                                {{__('table.tech.read_tech')}}
                                            </a>
                                        </td>
                                        <td>
                                            <a target="new"
                                               href="{{route('admin.projects.show', ['tech' => $model->id])}}">
                                                {{__('table.projects.project_read')}}
                                            </a>
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
                <!-- /.card -->

            </div>
        </div>
    </section>
@stop

@section('body_js')

    @include('voyager::settings.scripts')

    <script>

        $(document).ready(function () {
            $("#myBtn").click(function () {
                $("#type").val('');
                $("#order").val(null)

                $("#myModal").modal();
            });
        });


        $("#example1").DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            "scrollX" :true
            // "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');


    </script>
@stop






