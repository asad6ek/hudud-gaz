@extends('voyager::master')
@section('head')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/icon/icofont/css/icofont.css')}}">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/jquery.mCustomScrollbar.css')}}">
@stop

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @include('voyager::alerts')
                    <div class="card">
                        <div class="card-header">
                            <div class="card-tools mt-2">
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body p-0">
                            <table class="table table-hover text-nowrap">
                                <thead>
                                <tr>
                                    <th>{{__('table.projects.tech_condition')}}</th>
                                    <th>{{__('table.propositions.district')}}</th>
                                    <th>{{__('table.propositions.customer')}}</th>
                                    <th>{{__('table.tech_condition')}}</th>
                                    <th>{{__('table.projects.project')}}</th>
                                    <th>{{__('table.projects.comment')}}</th>
                                    <th>{{__('table.propositions.date')}}</th>
                                    <th>{{__('table.propositions.action')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($models as $model)
                                    <tr>
                                        <td>{{$model->id}}</td>
                                        <td>{{$model->proposition->getRegion()}}</td>
                                        <td>{{$model->proposition->applicantable->name}}</td>
                                        <td>
                                            <a target="new"
                                               href="{{asset('/storage/tech_conditions/'.$model->path)}}">
                                                {{__('table.tech.read_tech')}}
                                            </a>
                                        </td>
                                        <td>
                                            <a target="new"
                                               href="{{asset('/storage/projects/'.$model->project->path)}}">
                                                {{__('table.projects.project_read')}}
                                            </a>
                                        </td>
                                        <td>{{ $model->project->comment }}</td>
                                        <td>{{$model->project->created_at}}</td>
                                        <td>
                                            <input type="file" class="" id="exampleInputFile"
                                                   onchange="uploadPdf(this, {{$model->id}},
                                                       '{{route('admin.projects.upload',['tech_condition' => $model])}}',
                                                   {{$model->prop_id}})"
                                                   style="opacity: 0; width: 1px">

                                            <a class="btn btn-info" href="#" data-toggle="tooltip" title="{{__('table.rec.upload')}}">
                                                <label class="" for="exampleInputFile" style="margin-bottom: 0;">

                                                    <img src="http://100dayscss.com/codepen/upload.svg"
                                                         class="upload-icon w-25"/>
                                                    {{__('table.rec.upload_btn')}}
                                                </label>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>
@stop

@section('body_js')
    <script>
        function uploadPdf(el, id) {
            let myFormData = new FormData();

            myFormData.append('project', el.files[0]);
            myFormData.append('tech_id', id);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: '/admin/projects/' + id + '/upload',
                type: 'POST',
                processData: false, // important
                contentType: false, // important,
                cache: false,
                dataType: 'html',
                data: myFormData,
                success: function(data) {
                    // console.log(data);
                    // jQuery(data).wordExport('tushuntirish');
                    location.reload();
                }
            });
        }
    </script>
@stop

