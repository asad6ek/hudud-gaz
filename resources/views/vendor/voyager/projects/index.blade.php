@extends('voyager::master')
@section('head_css')
    <link rel="stylesheet" href="{{asset('css/datatables/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/datatables/responsive.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/datatables/buttons.bootstrap4.min.css')}}">
@stop

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @include('voyager::alerts')
                    <div class="card">
                        <div class="card-body pt-3">
                            @can('add_projects')
                            <button type="button" class="btn btn-success" id="myBtn">{{__('table.qrcode')}}</button>
                            @endcan
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>{{__('table.projects.tech_condition')}}</th>
                                        <th>{{__('table.propositions.district')}}</th>
                                        <th>{{__('table.propositions.customer')}}</th>
                                        <th>{{__('table.tech_condition')}}</th>
                                        <th>{{__('table.propositions.action')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($models as $model)
                                    <tr>
                                        <td>{{$model->id}}</td>
                                        <td>{{$model->proposition->region->region_name}}</td>
                                        <td>{{$model->proposition->applicantable->name}}</td>
                                        <td>
                                            <a target="new"
                                               href="{{asset('/storage/tech_conditions/'.$model->path)}}">
                                                {{__('table.projects.tech_read')}}
                                            </a>
                                        </td>
                                        <td>
                                            <form action="{{ route('admin.projects.cancel', ['tech' => $model->id]) }}" id="form-{{$model->id}}" class="row" method="POST">
                                                @csrf
                                                @method('POST')
                                                <label class="btn btn-info my-0 mr-2" for="file_{{$model->id}}"
                                                        title="{{__('table.btn_upd')}}">
                                                    <i class="fas fa-upload"></i>
                                                </label>
                                                <input type="file" id="file_{{$model->id}}"
                                                       onchange="uploadPdf(this, {{$model->id}})" hidden>

                                                <button type="button" class="btn btn-danger my-0" onclick="confirmDelete('form-{{$model->id}}')" title="{{__('table.projects.cancel')}}">
                                                    <i class="fas fa-trash-alt"></i>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="modal fade" id="myModal" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body" style="padding:40px 50px;">
                                    <form id="form_forType" role="form" method="GET" action="{{route('admin.projects.check')}}">
                                        @csrf
                                        <div class="form-group">
                                            <label for="type"><span class="glyphicon glyphicon-user"></span> {{__('table.code')}}</label>
                                            <input type="text" class="form-control" id="type" name="code" autocomplete="off">
                                        </div>

                                        @can('add_projects')
                                            <input type="submit" class="btn btn-success" value="{{__('table.check')}}">
                                        @endcan
                                        <button type="button" class="btn btn-danger btn-default pull-right" data-dismiss="modal"><span
                                                class="glyphicon glyphicon-remove"></span>{{__('table.activity_type.button_cancel')}}</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('body_js')
    <script src="{{asset('js/package/FileSaver.min.js')}}"></script>
    <script src="{{asset('js/package/jquery.wordexport.js')}}"></script>

    @include('voyager::settings.scripts')

    <script>
        $(document).ready(function () {
            $("#myBtn").click(function () {
                $('#form_forType').attr('action', '{{route('admin.projects.check')}}');
                $("#type").val('');
                $("#order").val(null)

                $("#myModal").modal();
            });
        });

        $("#example1").DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');


        function write_comment(id) {
            Swal.fire({
                title: "{{__('table.rec.write_comment')}}",
                input: 'text',
                inputAttributes: {
                    autocapitalize: 'off'
                },
                showCancelButton: true,
                confirmButtonText: "@lang('table.btn_send')",
                showLoaderOnConfirm: true,
                preConfirm: (comment) => {
                    $.ajax({
                        url: '/admin/recommendations/' + id + '/back',
                        type: 'POST',
                        data: {_token: $('meta[name="csrf-token"]').attr('content'), comment: comment}
                    }).done(function () {
                        location.reload()
                    })
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire({
                        title: '{{__('table.rec.come_back')}}',
                    })
                }
            })
        }

        function uploadPdf(el, id) {
            loader();
            let myFormData = new FormData();

            myFormData.append('project', el.files[0]);
            myFormData.append('tech_id', id);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: '/admin/projects/' + id + '/upload',
                type: 'POST',
                processData: false, // important
                contentType: false, // important,
                cache: false,
                dataType: 'html',
                data: myFormData,
                success: function (data) {
                    if (data)
                        jQuery(data).wordExport('tushuntirish');
                    location.reload();
                }
            });
        }

        function confirmDelete(form) {
            Swal.fire({
                title: '{{__('table.projects.alert_title')}}',
                text: "",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: '{{__('table.montage.button_yes')}}',
                cancelButtonText: '{{__('table.montage.button_no')}}',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $(`#${form}`).submit()
                    Swal.fire({
                        title: '{{__('table.montage.deleting_finish')}}',
                        icon: 'success',
                        showConfirmButton: false,
                    })
                }
            });
        }
    </script>
@stop
