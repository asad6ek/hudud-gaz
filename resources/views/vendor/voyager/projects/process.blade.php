@extends('voyager::master')
@section('head_css')
    <link rel="stylesheet" href="{{asset('css/datatables/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/datatables/responsive.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/datatables/buttons.bootstrap4.min.css')}}">
@stop

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @include('voyager::alerts')
                    <div class="card">
                        <div class="card-body pt-3">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>{{__('table.projects.tech_condition')}}</th>
                                        <th>{{__('table.propositions.district')}}</th>
                                        <th>{{__('table.propositions.customer')}}</th>
                                        <th>{{__('table.tech_condition')}}</th>
                                        <th> {{__('table.projects.project')}}</th>
                                        <th>{{__('table.propositions.time_limit')}}</th>
                                        @if(user_role('engineer'))
                                            <th>{{__('table.propositions.action')}}</th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($models as $model)
                                    @php
                                        $var = $model->percent($model->proposition);
                                        $percent = $var['percent'];
                                    @endphp
                                    <tr>
                                        <td>{{$model->id}}</td>
                                        <td>{{$model->proposition->region->region_name}}</td>
                                        <td>{{$model->proposition->applicantable->name}}</td>
                                        <td>
                                            <a target="new"
                                               href="{{url('storage/tech_conditions/'.$model->path)}}">
                                                {{__('table.tech.read_tech')}}
                                            </a>
                                        </td>
                                        <td>
                                            <a target="new"
                                               href="{{route('admin.projects.show', ['tech' => $model])}}">
                                                {{__('table.projects.project_read')}}
                                            </a>
                                        </td>
                                        <td style="width: 150px; text-align: center">
                                            <div class="progress progress-xs" style="margin-top: 10px;">
                                                <div class="progress-bar"
                                                     style="width: {{$percent}}%; background-color: rgb({{$model->colorForPercent($percent)}})"></div>
                                            </div>
                                            {{ $var['text'] }}
                                        </td>
                                        @if($model->proposition->status == 12)
                                            @if(user_role('engineer'))
                                                <td>
                                                    <a class="btn btn-success" title="{{__('table.confirm')}}" onclick="confirm({{$model->id}})" href="#agree">
                                                        <i class="fas fa-check"></i>
                                                    </a>
                                                    <a class="btn btn-danger" onclick="write_comment({{$model->id}})" title="{{__('table.back_doc')}}" href="#back">
                                                        <i class="fas fa-undo-alt"></i>
                                                    </a>
                                                </td>
                                            @endif
                                        @else
                                            <td>
                                                <div class="text-center">
                                                    <input type="file" id="form-{{$model->id}}" onchange="uploadPdf(this, {{$model->id}})" hidden>
                                                    <label class="btn btn-info" for="form-{{$model->id}}">
                                                        <i class="fas fa-upload"></i>
                                                    </label>
                                                </div>
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
@stop

@section('body_js')
    @include('voyager::settings.scripts')
    <script>
        $("#example1").DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            // "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');


        function confirm(id) {
            Swal.fire({
                title: "{{__('table.projects.agree_title')}}",
                icon: "question",
                showCancelButton: true,
                confirmButtonText: 'Жўнатиш',
                cancelButtonText: 'Бекор қилиш',
                showLoaderOnConfirm: true,
                preConfirm: (agree) => {
                    $.ajax({
                        url: '/admin/projects/' + id + '/agree',
                        type: 'GET',
                        data: {_token: $('meta[name="csrf-token"]').attr('content'), agree: agree}
                    }).done(function (result) {


                    })

                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then(() => {
                location.reload();
            })


        }

        function write_comment(id) {
            Swal.fire({
                title: "{{__('table.projects.write_comment')}}",
                input: 'text',
                inputAttributes: {
                    autocapitalize: 'off'
                },
                showCancelButton: true,
                confirmButtonText: 'Жўнатиш',
                cancelButtonText: 'Бекор қилиш',
                showLoaderOnConfirm: true,
                preConfirm: (comment) => {
                    $.ajax({
                        url: '/admin/projects/' + id + '/back',
                        type: 'POST',
                        data: {_token: $('meta[name="csrf-token"]').attr('content'), comment: comment}
                    }).done(function (result) {
                        location.reload()
                    })
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire({
                        title: '{{__('table.projects.come_back')}}',
                    })
                }
            })
        }


        function uploadPdf(el, id) {
            let myFormData = new FormData();

            myFormData.append('project', el.files[0]);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: '/admin/projects/' + id + '/upload',
                type: 'POST',
                processData: false, // important
                contentType: false, // important,
                cache: false,
                dataType: 'html',
                data: myFormData,
                success: function (data) {
                    // console.log(data);
                    Swal.fire({
                        title: "{{__('table.projects.uploaded')}}",
                        icon: "success",
                        confirmButtonText: 'OK',
                    }).then(function () {
                        location.reload();
                    });
                }
            });
        }
    </script>
@stop






