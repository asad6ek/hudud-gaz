@extends('voyager::master')
@section('head_css')
    <link rel="stylesheet" type="text/css" href="{{asset('icofont/css/icofont.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/jquery.mCustomScrollbar.css')}}">
@stop

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    @include('voyager::alerts')
                    <div class="card">
                        @includeFirst(['voyager::propositions.form'],[
                                'method'=>'POST',
                                'action'=>route('admin.propositions.store'),
                                'model'=>$model,
                            ])
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('body_js')
    <script src="{{ asset('js/jquery.inputmask.min.js') }}"></script>
    <script>
        $('[data-mask]').inputmask()

        var loadFile = function (event) {
            document.getElementById('label_for_image').innerHTML = event.target.files[0].name;
        };

        function change_type(type) {
            $('#user_type').attr('value', type);
        }

        function check_tin(el, type) {
            let value = $(el).val();
            $.ajax({
                url: '/admin/propositions/check_tin',
                type: 'POST',
                data: {_token: $('meta[name="csrf-token"]').attr('content'), tin: value, type: type}
            }).done(function (result) {
                $('#tin_span').text(result);
                if (result != 0) {
                    // console.log(result)
                    $('#tin_href1').css('display', 'inline');
                    $('#tin_href').attr('href', '/admin/propositions/' + value + '/individual')
                } else {
                    $('#tin_href1').css('display', 'none');
                }
                // document.getElementById('tin_span').innerText = result;
            })
        }

        function legal_check_tin(el, type) {
            let value = $(el).val();
            $.ajax({
                url: '/admin/propositions/check_tin',
                type: 'POST',
                data: {_token: $('meta[name="csrf-token"]').attr('content'), tin: value, type: type}
            }).done(function (result) {
                $('#legal_tin_span').text(result);
                if (result != 0) {
                    $('#legal_tin_href1').css('display', 'inline');
                    $('#legal_tin_href').attr('href', '/admin/propositions/' + value + '/legal_entity')
                } else {
                    $('#legal_tin_href1').css('display', 'none');
                }
            })
        }
    </script>
@stop
