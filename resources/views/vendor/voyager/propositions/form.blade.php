<form method="post" action="{{$action}}" enctype="multipart/form-data">
    @csrf
    @method($method)
    @includeFirst(['voyager::components.errors'],['errors' => $errors])

    <div class="card-body">
        @if(user_role('technic'))
            <div class="form-group row">
                <label for="number" class="col-sm-2 col-form-label">{{__('table.propositions.prop_number')}}</label>
                <div class="col-sm-10">
                    <input type="number" name="number" class="form-control" value="{{$model->number}}" id="number">
                </div>
            </div>
        @endif

        <div class="tab-content">
            <div class="tab-pane {{$method =='POST'?'active':($model->isLegalEntity()?:'active')}}" id="individual">
                @if(user_role('region'))
                    <input type="hidden" name="mode" value="{{request()->get('mode')}}">
                    <div class="form-group row">
                        <label for="tin" class="col-sm-2 col-form-label">{{__('table.propositions.stir')}}</label>
                        <div class="col-sm-10">

                            <input id="tin" type="number" name="tin" class="form-control"
                                   value="{{!is_null($model->applicantable_type)?$model->applicantable->tin : null}}"
                                   @if($method=="PUT")oninput="check_tin(this, 'individual')"@endif>

                            <p style="display: none" id="tin_href1"><br>@lang('table.propositions.check_props')<span
                                    id="tin_span"></span> <a target="new" href=""
                                                             id="tin_href">@lang('table.propositions.show_prop')</a></p>
                        </div>

                    </div>

                    <div class="form-group row">
                        <label for="pass_id"
                               class="col-sm-2 col-form-label">{{__('table.propositions.pass_id')}}</label>
                        <div class="col-sm-10">
                            <input id="pass_id" type="text" name="pass_id" class="form-control" autocomplete="off"
                                   value="{{!is_null($model->applicantable_type)?$model->applicantable->pass_id: null}}">
                        </div>
                    </div>


                    <div class="form-group row">
                        <label for="full_name"
                               class="col-sm-2 col-form-label">{{__('table.propositions.full_name')}}</label>
                        <div class="col-sm-10">
                            <input id="full_name" type="text" name="full_name" class="form-control"
                                   value="{{!is_null($model->applicantable_type)? $model->applicantable->full_name : null}}">
                        </div>
                    </div>

                @endif
            </div>

            @if(user_role('region'))
                <div class="tab-pane {{($model->isLegalEntity()?'active' : null)}}" id="legal_entity">
                    <div class="form-group row">
                        <label for="legal_entity_tin"
                               class="col-sm-2 col-form-label">{{__('table.propositions.legal_entity_tin')}}</label>
                        <div class="col-sm-10">
                            <input id="legal_entity_tin" type="number" name="legal_entity_tin" class="form-control"
                                   @if($method=="PUT")oninput="legal_check_tin(this, 'legal')"
                                   @endif value="{{!is_null($model->applicantable_type)?$model->applicantable->legal_entity_tin : null}}">

                            <p style="display: none" id="legal_tin_href1">
                                <br>
                                @lang('table.propositions.check_props')
                                <span id="legal_tin_span"></span>
                                <a target="new" href="" id="legal_tin_href">@lang('table.propositions.show_prop')</a>
                            </p>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="legal_entity_name"
                               class="col-sm-2 col-form-label">{{__('table.propositions.legal_entity_name')}}</label>
                        <div class="col-sm-10">
                            <input id="legal_entity_name" type="text" name="legal_entity_name" class="form-control"
                                   value="{{!is_null($model->applicantable_type)?$model->applicantable->legal_entity_name : null}}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="leader_name"
                               class="col-sm-2 col-form-label">{{__('table.propositions.leader_name')}}</label>
                        <div class="col-sm-10">
                            <input id="leader_name" type="text" name="leader_name" class="form-control"
                                   value="{{!is_null($model->applicantable_type)?$model->applicantable->leader_name : null}}">
                        </div>
                    </div>


                    <div class="form-group row">
                        <label for="email"
                               class="col-sm-2 col-form-label">{{__('table.propositions.email')}}</label>
                        <div class="col-sm-10">
                            <input id="email" type="text" name="email" class="form-control"
                                   value="{{!is_null($model->applicantable_type)?$model->applicantable->email : null}}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="leader_tin"
                               class="col-sm-2 col-form-label">{{__('table.propositions.leader_tin')}}</label>
                        <div class="col-sm-10">
                            <input id="leader_tin" type="text" name="leader_tin" class="form-control"
                                   value="{{!is_null($model->applicantable_type)?$model->applicantable->leader_tin : null}}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="activity_type" class="col-sm-2 col-form-label">{{__('table.propositions.type2')}}</label>
                        <div class="col-sm-10">
                            <select name="activity_type" id="activity_type" class="form-control">
                                @foreach($model->activityTypes() as $key => $propositionType)
                                    <option value="{{$key}}"
                                            @if(isset($model->type) and $model->type === $key) selected @endif>{{$propositionType}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            @endif
        </div>

        @if(user_role('region'))
            <div class="form-group row">
                <label for="tel_number"
                       class="col-sm-2 col-form-label">{{__('table.propositions.tel_number')}}</label>
                <div class="col-sm-10">
                    <input id="tel_number" type="text" name="tel_number" class="form-control" autocomplete="off"
                           data-inputmask='"mask": "(99)-999-99-99"' data-mask
                           value="{{!is_null($model->applicantable_type)?(isset($model->applicantable->tel_number) ? '+'.$model->applicantable->tel_number : null):null}}">
                </div>
            </div>

            <div class="form-group row">
                <label for="building_type"
                       class="col-sm-2 col-form-label">{{__('table.propositions.building_type')}}</label>
                <div class="col-sm-10">
                    <select name="building_type" id="building_type" class="form-control">
                        <option value="1" selected>
                            {{__('table.propositions.residential_building')}}
                        </option>
                        <option value="2">
                            {{__('table.propositions.non_residential_building')}}
                        </option>
                    </select>
                </div>
            </div>
        @endif

        @if(user_role('technic'))
            <div class="form-group row">
                <label for="region_id"
                       class="col-sm-2 col-form-label">{{__('table.propositions.district-city')}}</label>
                <div class="col-sm-10">
                    <select id="region_id" name="region_id" class="form-control">
                        @foreach($model->getRegions() as $key => $region)
                            <option value="{{$key}}"
                                    @if(isset($model->region_id) and $model->region_id === $key) selected @endif>{{$region}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label for="exampleInputFile"
                       class="col-sm-2 col-form-label">{{__('table.propositions.upload')}}</label>
                <div class="col-sm-10">
                    <div class="input-group">
                        <div class="custom-file">
                            <input name="path" type="file" class="custom-file-input" onchange="loadFile(event)"
                                   id="exampleInputFile">
                            <label id="label_for_image" class="custom-file-label"
                                   for="exampleInputFile">{{$model->path ?? __('table.propositions.choose_file')}}</label>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
    <div class="card-footer">

        <input type="hidden" name="type" value="{{($model->isLegalEntity()? 2 : 1)}}" id="user_type">

        <button type="submit" class="btn btn-primary mr-2">{{__('table.propositions.save')}}</button>

        <a href="{{route('admin.propositions.index')}}" class="btn btn-outline-danger">
            {{__('table.montage.button_previous')}}
        </a>

        <button type="reset" class="btn btn-outline-secondary float-right">
            {{__('table.general_settings.button_clear')}}
        </button>
    </div>
</form>
