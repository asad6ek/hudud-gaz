@extends('voyager::master')
@section('head_css')
    <link rel="stylesheet" href="{{asset('css/datatables/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/datatables/responsive.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/datatables/buttons.bootstrap4.min.css')}}">
@stop

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @include('voyager::alerts')
                    <div class="card">
                        @can('add_propositions')
                            <div class="card-header">
                                <div class="row">
                                    <div class="col">
                                        <div class="col">
                                            <a class="btn btn-success"
                                               href="{{route('admin.propositions.create')}}">{{__('table.propositions.add_prop')}}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endcan

                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>{{__('table.propositions.prop_number')}}</th>
                                        @if (!user_role('region'))
                                            <th>{{__('table.propositions.district')}}</th>
                                        @endif
                                        <th>{{__('table.propositions.proposition')}}</th>
                                        <th>{{__('table.propositions.date')}}</th>
                                        <th>{{__('table.propositions.time_limit')}}</th>
                                        <th>{{__('table.propositions.action')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($models as $model)
                                    @php
                                        $var = $model->percent($model);
                                        $percent = $var['percent'];
                                    @endphp
                                    <tr>
                                        <td>{{$model->number}}</td>
                                        @if (!user_role('region'))
                                            <td>{{$model->region->region_name}}</td>
                                        @endif
                                        <td>
                                            <a target="new" href="{{route('admin.propositions.show',['proposition' => $model->id])}}">
                                                {{__('table.propositions.read')}}
                                            </a>
                                        </td>
                                        <td>{{$model->created_at}}</td>
                                        <td style="width: 150px; text-align: center">
                                            <div class="progress progress-xs" style="margin-top: 10px;">
                                                <div class="progress-bar"
                                                     style="width: {{$percent}}%; background-color: rgb({{$model->colorForPercent($percent)}})"></div>
                                            </div>
                                            {{ $var['text'] }}
                                        </td>
                                        <td>
                                            <form method="post" class="" id="individual-{{$model->id}}"
                                                  action="{{route('admin.propositions.delete',['proposition' => $model->id])}}">
                                                @can('delete_propositions')
                                                    <a class="btn btn-warning" data-toggle="tooltip"
                                                       title="{{__('table.propositions.edit_prop')}}"
                                                       href="{{route('admin.propositions.edit',['proposition' => $model->id])}}">
                                                        <i class="fas fa-pen-alt"></i>
                                                    </a>
                                                @endcan
                                                @can('delete_propositions')
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="button"
                                                            onclick="confirmDelete('individual-{{$model->id}}')"
                                                            class="btn btn-danger"
                                                            data-toggle="tooltip"
                                                            title="{{__('table.propositions.delete_prop')}}">
                                                        <i class="fas fa-trash-alt"></i>
                                                    </button>
                                                @endcan
                                                @if (user_role('region'))
                                                    <a class="btn btn-success" data-toggle="tooltip"
                                                       title="{{__('table.propositions.edit_prop')}}"
                                                       href="{{ route('admin.propositions.edit', ['proposition' => $model->id, 'mode' => 'success']) }}">
                                                        <i class="fas fa-plus-square"></i>
                                                    </a>
                                                    <a class="btn  btn-danger" data-toggle="tooltip"
                                                       title="{{__('table.propositions.edit_prop')}}"
                                                       href="{{ route('admin.propositions.edit', ['proposition' => $model->id, 'mode' => 'fail']) }}">
                                                        <i class="fas fa-minus-square"></i>
                                                    </a>
                                                @endif
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('body_js')
    @include('voyager::settings.scripts')

    <script>
        function confirmDelete(form) {
            Swal.fire({
                title: '{{__('table.propositions.delete_alert')}}',
                text: "",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: '{{__('table.montage.button_yes')}}',
                cancelButtonText: '{{__('table.montage.button_no')}}',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $(`#${form}`).submit()
                    Swal.fire({
                        title: '{{__('table.montage.deleting_finish')}}',
                        icon: 'success',
                        showConfirmButton: false,
                    })
                }
            });
        }

        $(function () {
            $("#example1").DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
                "order": false,
                // "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
        });
    </script>
@stop
