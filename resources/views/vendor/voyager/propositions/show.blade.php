@extends('voyager::master')
@section('head_css')
    <link rel="stylesheet" href="{{asset('css/datatables/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/datatables/responsive.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/datatables/buttons.bootstrap4.min.css')}}">
@stop

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">

                        <div class="card-body">

                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>{{__('table.propositions.prop_number')}}</th>

                                    @if (!user_role('region'))
                                        <th>{{__('table.propositions.district')}}</th>
                                    @endif

                                    <th>{{__('table.propositions.proposition')}}</th>

                                    <th>{{__('table.propositions.date')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($models as $model)
                                    <tr>
                                        <td>{{$model->number}}</td>

                                        @if (!user_role('region'))
                                            <td>{{$model->getRegion()}}</td>
                                        @endif
                                        <td>
                                            <a target="new" href="{{url('storage/propositions/'.$model->path)}}">
                                                {{__('table.propositions.read')}}
                                            </a>
                                        </td>
                                        <td>{{$model->created_at}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('body_js')

    <!-- DataTables  & Plugins -->
    @include('voyager::settings.scripts')

    <script>
        $(function () {
            $("#example1").DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
                // "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

        });
    </script>
@stop
