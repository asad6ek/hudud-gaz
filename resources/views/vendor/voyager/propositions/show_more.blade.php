@extends('voyager::master')
@section('head_css')
    <link rel="stylesheet" href="{{asset('css/datatables/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet"
          href="{{asset('css/datatables/responsive.bootstrap4.min.css')}}">
    <link rel="stylesheet"
          href="{{asset('css/datatables/buttons.bootstrap4.min.css')}}">
@stop

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="invoice p-3 mb-3">
                        <!-- title row -->
                        <div class="row">
                            <div class="col-12">
                                <div class="card">

                                    @dd($model->getRelations())
                                    @foreach($model->getAttributes() as $key => $value)
                                    {{$key}} = {{ $value }} <br>
                                    @endforeach
{{--                                    --}}
{{--                                    <div class="row card-header">--}}
{{--                                        <div class="col-md-4">--}}
{{--                                            <h3 class="card-title"><strong> №:</strong></h3>--}}
{{--                                        </div>--}}
{{--                                        <div class="col-md-8">--}}
{{--                                            {{ $model->id }}--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                    <div class="row card-header">--}}
{{--                                        <div class="col-md-4">--}}
{{--                                            <h3 class="card-title"><strong> {{__('table.reports.created_at')}}:</strong></h3>--}}
{{--                                        </div>--}}
{{--                                        <div class="col-md-8">--}}
{{--                                            {{$model->created_at->format('d.m.Y H:i')}}--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                    <div class="row card-header">--}}
{{--                                        <div class="col-md-4">--}}
{{--                                            <h3 class="card-title"><strong> {{__('table.reports.address')}}:</strong></h3>--}}
{{--                                        </div>--}}
{{--                                        <div class="col-md-8">--}}
{{--                                            {{ $model->region->region_name . ' ' . $model->recommendation->address}} manzilda joylashgan--}}
{{--                                            {{$model->applicantable->name}}ga qarashli {{ $model->building_type }} bino(lar)ini gazlashtirish--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                    <div class="row card-header">--}}
{{--                                        <div class="col-md-4">--}}
{{--                                            <h3 class="card-title"><strong> {{__('table.reports.designer')}}:</strong></h3>--}}
{{--                                        </div>--}}
{{--                                        <div class="col-md-8">--}}
{{--                                            {{ $model->tech_condition->designer->name }}--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                    <div class="row card-header">--}}
{{--                                        <div class="col-md-4">--}}
{{--                                            <h3 class="card-title"><strong> {{__('table.reports.montage_firm')}}:</strong></h3>--}}
{{--                                        </div>--}}
{{--                                        <div class="col-md-8">--}}
{{--                                            {{ $model->tech_condition->montage_firm->full_name }}--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                    <div class="row card-header">--}}
{{--                                        <div class="col-md-4">--}}
{{--                                            <h3 class="card-title"><strong> {{__('table.reports.date')}}:</strong></h3>--}}
{{--                                        </div>--}}
{{--                                        <div class="row col-md-8">--}}
{{--                                            <div class="col-md-6">{{ $model->tech_condition->created_at }}</div>--}}
{{--                                            <div class="col-md-6">{{ $model->tech_condition->updated_at }}</div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                    <div class="row card-header">--}}
{{--                                        <div class="col-md-4">--}}
{{--                                            <h3 class="card-title"><strong> {{__('table.reports.tech_condition_number_and_date')}}:</strong></h3>--}}
{{--                                        </div>--}}
{{--                                        <div class="row col-md-8">--}}
{{--                                            <div class="col-md-4">02/{{ $model->tech_condition->id }}</div>--}}
{{--                                            <div class="col-md-4">{{ $model->tech_condition->created_at }}</div>--}}
{{--                                            <div class="col-md-4">--}}
{{--                                                <a href="{{ url('storage/tech_conditions/' . $model->tech_condition->path) }}" target="new">--}}
{{--                                                    @lang('table.projects.tech_read')--}}
{{--                                                </a>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                    <div class="row card-header">--}}
{{--                                        <div class="col-md-4">--}}
{{--                                            <h3 class="card-title"><strong> {{__('table.reports.project_number_and_date')}}:</strong></h3>--}}
{{--                                        </div>--}}
{{--                                        <div class="row col-md-8">--}}
{{--                                            <div class="col-md-4">{{ $model->tech_condition->project->id }}</div>--}}
{{--                                            <div class="col-md-4">{{ $model->tech_condition->project->created_at }}</div>--}}
{{--                                            <div class="col-md-4">--}}
{{--                                                <a href="{{ url('storage/projects/' . $model->tech_condition->project->path) }}" target="new">--}}
{{--                                                    @lang('table.projects.project_read')--}}
{{--                                                </a>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                    <div class="row card-header">--}}
{{--                                        <div class="col-md-4">--}}
{{--                                            <h3 class="card-title"><strong> {{__('table.reports.montage_number_and_date')}}:</strong></h3>--}}
{{--                                        </div>--}}
{{--                                        <div class="row col-md-8">--}}
{{--                                            <div class="col-md-4">{{ $model->tech_condition->montage->id }}</div>--}}
{{--                                            <div class="col-md-4">{{ $model->tech_condition->montage->created_at }}</div>--}}
{{--                                            <div class="col-md-4">--}}
{{--                                                <a href="{{ url('storage/montages/' . $model->tech_condition->montage->path) }}" target="new">--}}
{{--                                                    @lang('table.projects.montage_read')--}}
{{--                                                </a>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                    <div class="row card-header">--}}
{{--                                        <div class="col-md-4">--}}
{{--                                            <h3 class="card-title"><strong> {{__('table.reports.annual_gas_consumption')}}:</strong></h3>--}}
{{--                                        </div>--}}
{{--                                        <div class="row col-md-8">--}}
{{--                                            {{ $model->recommendation->gas_consumption }}--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                    <div class="row card-header">--}}
{{--                                        <div class="col-md-4">--}}
{{--                                            <h3 class="card-title"><strong> {{__('table.reports.length_of_pipe') . $data['key']}}:</strong></h3>--}}
{{--                                        </div>--}}
{{--                                        <div class="row col-md-8">--}}
{{--                                            {{ $data['len'] }}--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                    <div class="row card-header">--}}
{{--                                        <div class="col-md-4">--}}
{{--                                            <h3 class="card-title"><strong> {{__('table.rec.pipe_size')}}:</strong></h3>--}}
{{--                                        </div>--}}
{{--                                        <div class="row col-md-8">--}}
{{--                                            {{ $model->recommendation->pipe_size }}--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

                                    <!-- /.card-footer-->
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
