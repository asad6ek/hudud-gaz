@extends('voyager::master')
@section('head')
    <link rel="stylesheet" type="text/css" href="{{asset('icofont/css/icofont.css')}}">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/jquery.mCustomScrollbar.css')}}">
@stop

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- Basic Form Inputs card start -->
                    @include('voyager::alerts')


                    <div class="card">
                        <div class="card-header">
                            @if(user_role('region'))
                                @php
                                    $type = $model->isLegalEntity();
                                @endphp
                                <ul class="nav nav-pills">
                                    <li class="nav-item"><a class="nav-link @if(!$type) active @endif" href="#individual" data-toggle="tab"
                                                            onclick="change_type(1)">{{__('table.propositions.individual')}}</a>
                                    </li>
                                    <li class="nav-item"><a class="nav-link @if($type) active @endif" href="#legal_entity" data-toggle="tab"
                                                            onclick="change_type(2)">{{__('table.propositions.legal_entity')}}</a>
                                    </li>
                                </ul>
                            @endif
                        </div>

                        @includeFirst(['voyager::propositions.form'],[
                                                'method'=>'PUT',
                                                'action'=>route('admin.propositions.update',['proposition' => $model->id]),
                                                'model'=>$model,
                                            ])
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop


@section('body_js')
    <script src="{{ asset('js/jquery.inputmask.min.js') }}"></script>
    <script>
        $('[data-mask]').inputmask()

        var loadFile = function (event) {
            let image = document.getElementById('exampleInputFile');
            console.log(event.target.files[0])
            image.src = URL.createObjectURL(event.target.files[0]);

            let label = document.getElementById('label_for_image');
            label.innerHTML = event.target.files[0].name;
        };

        function change_type(type) {
            $('#user_type').attr('value', type);
        }

        function check_tin(el, type) {
            let value = $(el).val();
            console.log(value)
            $.ajax({
                url: '/admin/propositions/check_tin',
                type: 'POST',
                data: {_token: $('meta[name="csrf-token"]').attr('content'), tin: value, type: type}
            }).done(function (result) {
                $('#tin_span').text(result);
                if (result != 0) {
                    // console.log(result)
                    $('#tin_href1').css('display', 'inline');
                    $('#tin_href').attr('href', '/admin/propositions/' + value + '/individual')
                } else {
                    $('#tin_href1').css('display', 'none');
                }
                // document.getElementById('tin_span').innerText = result;
            })
        }

        function legal_check_tin(el, type) {
            let value = $(el).val();
            $.ajax({
                url: '/admin/propositions/check_tin',
                type: 'POST',
                data: {_token: $('meta[name="csrf-token"]').attr('content'), tin: value, type: type}
            }).done(function (result) {
                $('#legal_tin_span').text(result);
                if (result != 0) {
                    $('#legal_tin_href1').css('display', 'inline');
                    $('#legal_tin_href').attr('href', '/admin/propositions/' + value + '/legal_entity')
                } else {
                    $('#legal_tin_href1').css('display', 'none');
                }
            })
        }
    </script>
@stop
