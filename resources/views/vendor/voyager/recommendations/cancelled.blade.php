@extends('voyager::master')

@section('head_css')
    <link rel="stylesheet" href="{{asset('css/datatables/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/datatables/responsive.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/datatables/buttons.bootstrap4.min.css')}}">
@stop

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @include('voyager::alerts')
                    <div class="card">
                        <div class="card-body">
                            <table id="table1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>{{__('table.propositions.prop_number')}}</th>
                                    <th>{{__('table.propositions.district')}}</th>
                                    <th>{{__('table.propositions.customer')}}</th>
                                    <th>{{__('table.recommendation')}}</th>
                                    <th>{{__('table.propositions.date')}}</th>
                                    <th>{{__('table.propositions.time_limit')}}</th>
                                    <th>{{__('table.projects.comment')}}</th>
                                    <th>{{__('table.propositions.action')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($cancels as $model)
                                    @php
                                        $var = $model->percent($model);
                                        $percent = $var['percent'];
                                    @endphp
                                    <tr>
                                        <td>{{$model->number}}</td>
                                        <td>{{$model->getRegion()}}</td>
                                        <td>{{$model->applicantable->name}}</td>
                                        <td>
                                            <a target="new" href="{{url('storage/recommendations/'.$model->recommendation->path)}}">
                                                {{__('table.rec.read_rec')}}
                                            </a>
                                        </td>
                                        <td>{{$model->recommendation->created_at}}</td>
                                        <td style="width: 150px; text-align: center">
                                            <div class="progress progress-xs" style="margin-top: 10px;">
                                                <div class="progress-bar"
                                                     style="width: {{$percent}}%; background-color: rgb({{$model->colorForPercent($percent)}})"></div>
                                            </div>
                                            {{ $var['text'] }}
                                        </td>
                                        <td>
                                            {{ $model->recommendation->comment }}
                                        </td>
                                        <td>
                                            <a class="btn btn-success" data-toggle="tooltip"
                                               title="{{__('table.propositions.edit_prop')}}"
                                               href="{{ route('admin.propositions.edit', ['proposition' => $model->id, 'mode' => 'success']) }}">
                                                <i class="fas fa-plus-square"></i>
                                            </a>
                                            <a class="btn  btn-danger" data-toggle="tooltip"
                                               title="{{__('table.propositions.edit_prop')}}"
                                               href="{{ route('admin.propositions.edit', ['proposition' => $model->id, 'mode' => 'fail']) }}">
                                                <i class="fas fa-minus-square"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>
@stop

@section('body_js')
    @include('voyager::settings.scripts')
    <script>
        $(function () {
            $("#table1").DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
                "order": false,
                // "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
        });
    </script>
@stop

