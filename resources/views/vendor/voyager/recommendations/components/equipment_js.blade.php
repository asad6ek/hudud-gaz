<script>
    let count = 0;
    let equips_input = $('#equips');
    let submit = $('#submit');
    let gas_meter = $('#gas_meter');
    let equips = [];
    let gas_meters = [];
    class Equipment {
        constructor() {
            this.equipment = '';
            this.type = '';
            this.number = null;
            this.note = null;
        }
    }

    class GasMeter {
        constructor() {
            this.type = 0;
            this.number = null;
        }
    }

    function ajax(url, callback) {
        $.ajax({
            url: url,
            dataType: 'json',
            success: function(data) {
                callback(data);
            }
        });
    }

    function addEquipment(equips_data = new Equipment()) { // equips_data - for select necessary options and input fields
        submit.attr('disabled', false);
        ajax("{{route('admin.equipment.add')}}", function(data) {
            create(data, equips_data);
        });
    }

    function create(data, equips_data) {
        equips.push(new Equipment());
        let equipments = $('#equipments');
        let row = document.createElement('div');
        let row2 = document.createElement('div');
        let col = document.createElement('div');
        let col2 = document.createElement('div');

        let equipment = document.createElement('select');
        let type = document.createElement('select');
        let number = document.createElement('input');
        let note = document.createElement('input');
        let btn = document.createElement('button');
        append(row2, equipment, 'equipment', null, "@lang('table.equipment.name')");
        append(row2, type, 'type', null,"@lang('table.equipment.type')");
        append(row2, number, 'number', 'number', "@lang('table.equipment.number')", "@lang('table.equipment.number_hint')");
        append(row2, note, 'note', 'text', "@lang('table.equipment.note')", "@lang('table.equipment.note_hint')");
        append(col2, btn, '', null, '', '', 'btn btn-danger');

        fillSelect(equipment, data, "@lang('table.equipment.name_hint')", equips_data.equipment);
        fillSelect(type, [], "@lang('table.equipment.type_hint')", equips_data.type);

        equipment.onchange = change;
        function change(use = false) {
            $(type).empty();
            equips[equipment.hint]['equipment'] = parseInt(equipment.value);
            if (!use)
                equips[equipment.hint]['type'] = '';

            ajax("{{route('admin.equipment.type')}}" + `/${equipment.value}`, function(data) {
                fillSelect(type, data, "@lang('table.equipment.type_hint')", equips_data.type);
            });
        }

        count ++;
        row.id = `row-${count}`;
        row.classList.add('row', 'align-items-end', 'mb-3');
        row.style.display = "none";
        col.classList.add('col-11');
        col2.classList.add('col-1');
        row2.classList.add('row');

        row.append(col, col2);
        col.append(row2);
        equipments.append(row);
        $(`#row-${count}`).show(300);

        /* For updating data */
        change(true);

        function append(root, element, name, type, label, placeholder, classes = 'form-control') {
            let div = document.createElement('div');
            div.classList.add('col-3');

            setProperty(element, name, type, placeholder, equips_data);
            if (label) {
                let l = document.createElement('label');
                l.textContent = label;
                l.htmlFor = name;
                div.append(l);
            }

            for (let cls of classes.split(' ')) {
                element.classList.add(cls);
            }

            if (!label) {
                div.classList.remove('col-3');
                div.classList.add('row', 'justify-content-end', 'pr-3');
            }

            div.append(element);
            root.append(div);
        }
    }

    function setProperty(element, name, type, placeholder, data = {}) {
        if (type) {
            element.type = type;
            element.value = data[name];
            element.onkeyup = function() {
                if (type === 'number') {
                    equips[element.hint][name] = parseInt(element.value);
                } else {
                    equips[element.hint][name] = element.value;
                }
                Stringify(equips);
            }
        }

        if (name) {
            element.id = name + '-' + (count + 1);
            element.name = name;
            element.placeholder = placeholder;
            element.hint = count;
            if (type !== 'text')
                element.required = true;
            element.onchange = function() {
                equips[element.hint][name] = parseInt(element.value);
                Stringify(equips);
            }

            equips[element.hint][name] = data[name];
        } else {
            element.type = 'button';
            element.id = count + 1;
            element.innerHTML = '<i class="fas fa-minus"></i>';
            element.title = "@lang('global.btn_del')";
            element.onclick = function() {
                if (count === parseInt(element.id)) {
                    $(`#row-${element.id}`).hide(300, function() {
                        $(this).remove();
                        equips.pop();
                        count --;
                    });
                } else {
                    Swal.fire({
                        icon: 'warning',
                        title: "@lang('table.equipment.warning')",
                        confirmButtonText: "<i class='fas fa-check'></i>"
                    });
                }

                if (count === 1) {
                    submit.attr('disabled', true);
                }
            }
        }
    }
    // You must rewrite this code!

    function fillSelect(element, data, first, value = '') {
        let option = document.createElement('option');
        option.value = '';
        option.text = first;
        element.append(option);

        let j = 0;
        for (let i in data) {
            j = i;
            option = document.createElement('option');
            option.value = i;
            option.text = data[j];
            if (i === value.toString()) {
                option.selected = true;
            }
            element.append(option);
        }
    }

    function changeEquips() {
        let data;
        if (equips_input.length)
            data = JSON.parse(equips_input.val());
        else
            data = [];

        data.forEach(equip => {
            addEquipment(equip);
        });

        if (data.length) {
            submit.attr('disabled', false);
        }
    }

    function Stringify(data, input) {
        if (input) {
            input.val(JSON.stringify(data))
            return;
        }

        equips_input.val(JSON.stringify(data));
    }

    $(document).ready(function() {
        if ($('#type').val() === 'success') {
            $('#submit').attr('disabled', true);
        }

        let tools = JSON.parse(gas_meter.val());
        if (tools.length) {
            tools.forEach(tool => {
                addGasMeter(tool);
            });
        } else {
            addGasMeter();
        }

        setTimeout(() => {
            changeEquips();
        });
    });

    /* For gas-meter */
    let gas_count = 0;
    function addGasMeter(tool = new GasMeter()) {
        let gas = $('#gas');
        let row = document.createElement('div');
        let type = document.createElement('select');
        let number = document.createElement('input');
        let add = document.createElement('button');
        let remove = document.createElement('button');
        gas_meters.push(new GasMeter());

        row.classList.add('row', 'mb-2');
        type.classList.add('custom-select');
        number.classList.add('form-control');
        number.type = 'number';
        number.value = tool.number;
        add.classList.add('btn', 'btn-info', 'mr-2');
        remove.classList.add('btn', 'btn-danger');

        type.name = `${gas_count}`;
        type.required = true;
        number.name = `${gas_count}`;
        number.required = true;
        number.placeholder = "@lang('table.equipment.number_hint')";

        add.innerHTML = '<i class="fas fa-plus"></i>';
        add.type = 'button';
        remove.innerHTML = '<i class="fas fa-minus"></i>';
        remove.type = 'button';
        remove.name = gas_count.toString();
        add.onclick = addGasMeter;
        remove.onclick = function() {
            if (gas_count === parseInt(remove.name) + 1) {
                row.remove();
                gas_count --;
                gas_meters.pop();
                Stringify(gas_meters, gas_meter);
            } else {
                Swal.fire({
                    icon: 'warning',
                    title: "@lang('table.equipment.warning')",
                    confirmButtonText: "<i class='fas fa-check'></i>"
                });
            }

            if (gas_count === 0) {
                gas.remove();
                Stringify([], gas_meter);
            }
        }

        type.onchange = change;
        number.onchange = change;
        number.oninput = change;

        function change(event) {
            let input = event.target;
            let attr = input.type === 'number' ? 'number' : 'type';
            gas_meters[input.name][attr] = input.value;
            Stringify(gas_meters, gas_meter);
        }

        ajax("{{route('admin.equipment.type')}}/1", function(data) {
            fillSelect(type, data, "@lang('table.equipment.type_hint')", tool.type);
        });

        appendElement(row, type);
        appendElement(row, number);
        appendElement(row, add, remove);
        gas.append(row)
        gas_count ++;
    }

    function appendElement(root, ...elem) {
        let col = document.createElement('div');

        if (elem.length === 1) {
            col.classList.add('col-5');
        } else {
            col.classList.add('col-2', 'text-right');
        }

        elem.forEach(e => {
            col.append(e);
        });

        root.append(col);
    }
</script>
