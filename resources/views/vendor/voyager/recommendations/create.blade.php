@extends('voyager::master')
@section('head_css')
    <link rel="stylesheet" type="text/css" href="{{asset('icofont/css/icofont.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/jquery.mCustomScrollbar.css')}}">
    <link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
    <style>
        .select2-selection .select2-selection--single {
            padding-bottom: 30px !important;
        }
    </style>
@stop

@section('body_js')
    <!-- Summernote -->
    <script src="{{asset('js/summernote-bs4.min.js')}}"></script>
    <script src="{{ asset('js/select2.full.min.js') }}"></script>

    <!-- Page specific script -->
    <script>
        $(function () {
            // Summernote
            $('#summernote').summernote()

            // CodeMirror
            // CodeMirror.fromTextArea(document.getElementById("codeMirrorDemo"), {
            //     mode: "htmlmixed",
            //     theme: "monokai"
            // });
        })

    </script>
    <script src="{{ asset('js/moment.min.js') }}"></script>
    <script src="{{ asset('js/jquery.inputmask.min.js') }}"></script>
    <script src="{{ asset('js/daterangepicker.js') }}"></script>
    <script src="{{ asset('js/tempusdominus-bootstrap-4.min.js') }}"></script>
{{--    <script src="{{ asset('js/main.js') }}"></script>--}}
    <script>
        $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
        //Datemask2 mm/dd/yyyy
        $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
        //Money Euro
        $('[data-mask]').inputmask();

    </script>
    @include('voyager::recommendations.components.equipment_js')
@stop

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!-- Basic Form Inputs card start -->
                @include('voyager::alerts')

                <div class="card">
                    <div class="card-header row">
                        <a href="{{route('admin.recommendations.index')}}" class="btn btn-warning">
                            {{__('table.back')}}
                        </a>
                        <h3 class="my-auto ml-3">{{__('table.rec.add_rec')}}</h3>
                    </div>
                    <div class="card-inline-block p-2">
                        @includeFirst(['voyager::recommendations.form'], [
                                        'method' => 'POST',
                                        'action' => route('admin.recommendations.store')
                                    ])
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

