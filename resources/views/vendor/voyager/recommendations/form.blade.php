<div class="col-md-12">
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">{{__('table.rec.add_inf')}}</h3>
        </div>
        <form role="form" method="POST" action="{{$action}}">
            @csrf
            @method($method)
            @includeFirst(['voyager::components.errors'],['errors' => $errors])

            @if($type == 'success')
                <div class="row">
                    <div class="card-body col-md-6">
                        <div class="form-group">
                            <label for="exampleInputAddress">
                                {{__('table.rec.address')}}<span aria-hidden="true" style="color: red;">*</span>
                            </label>
                            <input type="text" name="address" class="form-control" id="exampleInputAddress"
                                   placeholder="{{__('table.rec.input_address')}}" value="{{$model->address ?? null}}" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputAccessPoint">
                                {{__('table.rec.access_point')}}<span aria-hidden="true" style="color: red;">*</span>
                            </label>
                            <input type="text" name="access_point" class="form-control" id="exampleInputAccessPoint"
                                   placeholder="{{__('table.rec.input_access_point')}}" value="{{$model->access_point ?? null}}" required>
                        </div>
                        <div class="form-group">
                            <label for="gas_network">
                                {{__('table.rec.gas_network')}}<span style="color: red;">*</span>
                            </label>
                            <input type="text" name="gas_network" class="form-control" id="gas_network"
                                   placeholder="{{__('table.rec.input_gas_network')}}" value="{{$model->gas_network ?? null}}" required>
                        </div>
                        <div class="form-group">
                            <label for="length_type">
                                {{__('table.rec.length_type')}}<span aria-hidden="true" style="color: red;">*</span>
                            </label>
                            <select class="custom-select" name="length_type" id="length_type">
                                <option value="1">{{__('table.rec.label_underline')}}</option>
                                <option value="2">{{__('table.rec.label_line')}}</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputAboveLen">
                                {{__('table.rec.length')}}<span aria-hidden="true" style="color: red;">*</span>
                            </label>
                            <input type="number" name="length" class="form-control" id="exampleInputAboveLen"
                                   placeholder="{{__('table.rec.input_length')}}" step=".01" value="{{$model->length ?? null}}" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPipeSize">
                                {{__('table.rec.pipe_size')}}<span aria-hidden="true" style="color: red;">*</span>
                            </label>
                            <input type="text" name="pipe_size" class="form-control" id="exampleInputPipeSize"
                                   placeholder="{{__('table.rec.input_pipe_size')}}" value="{{$model->pipe_size ?? null}}" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputDepth">
                                {{__('table.rec.depth')}}<span aria-hidden="true" style="color: red;">*</span>
                            </label>
                            <input type="number" name="depth" class="form-control" id="exampleInputDepth"
                                   placeholder="{{__('table.rec.input_depth')}}" value="{{$model->depth ?? null}}" required>
                        </div>
                    </div>
                    <div class="card-body col-md-6">
                        <div class="form-group">
                            <label for="pipe_type">
                                {{__('table.rec.pipe_type')}}<span style="color: red;">*</span>
                            </label>
                            <select name="pipe_type" id="pipe_type" class="custom-select">
                                <option value="1">{{__('table.rec.low_pressure')}}</option>
                                <option value="2">{{__('table.rec.high_pressure')}}</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputCapacity">
                                {{__('table.rec.capacity')}}<span aria-hidden="true" style="color: red;">*</span>
                            </label>
                            <input type="number" name="capacity" class="form-control" id="exampleInputCapacity"
                                   placeholder="{{__('table.rec.input_capacity')}}" step=".01" value="{{$model->capacity ?? null}}" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputRealCapacity">
                                {{__('table.rec.real_capacity')}}<span aria-hidden="true" style="color: red;">*</span>
                            </label>
                            <input type="number" name="real_capacity" class="form-control" id="exampleInputRealCapacity"
                                   placeholder="{{__('table.rec.input_capacity')}}" value="{{$model->real_capacity ?? null}}" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputAvgWinter">
                                {{__('table.rec.avg_winter')}}<span aria-hidden="true" style="color: red;">*</span>
                            </label>
                            <input type="text" name="avg_winter" class="form-control" id="exampleInputAvgWinter"
                                   placeholder="{{__('table.rec.input_avg_winter')}}" step=".01" value="{{$model->avg_winter ?? null}}" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputAvgSummer">
                                {{__('table.rec.avg_summer')}}<span aria-hidden="true" style="color: red;">*</span>
                            </label>
                            <input type="text" name="avg_summer" class="form-control" id="exampleInputAvgSummer"
                                   placeholder="{{__('table.rec.input_avg_summer')}}" step=".01" value="{{$model->avg_summer ?? null}}" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputGrc">
                                {{__('table.rec.grc')}}<span aria-hidden="true" style="color: red;">*</span>
                            </label>
                            <input type="text" name="grc" class="form-control" id="exampleInputGrc"
                                   placeholder="{{__('table.rec.input_grc')}}" value="{{$model->grc ?? null}}" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputGasConsumption">
                                {{__('table.rec.size')}} (m<sup>3</sup>)<span aria-hidden="true" style="color: red;">*</span>
                            </label>
                            <input type="number" name="gas_consumption" class="form-control" id="exampleInputGasConsumption"
                                   placeholder="{{__('table.rec.input_size')}}" step=".01" value="{{$model->gas_consumption ?? null}}" required>
                        </div>
                    </div>


                    <div class="card-body col-md-12">
                      <textarea id="summernote" name="additional">
                          @if(isset($model->additional) && $model->status)
                            {{$model->additional}}
                          @else
                            <ol>
                                <li value="9">&nbsp;Табиий газ қандай мақсадларда ишлатилади: <b>Иситиш ва маиший эҳтиёжлар учун</b></li>
                                <li>&nbsp;Буюртма учун алоҳида шартлар:&nbsp; <b>КМКга риоя қилган паст босимли газ қувурига улансин</b></li>
                                <li>Буюртмачининг манзили: </li>
                            </ol>
                          @endif
                      </textarea>
                    </div>

                    <div class="card-body">
                        <div id="gas" class="col mb-5">
                            <div class="row text-bold">
                                <div class="col-12">@lang('table.equipment.gas_meter')</div>
                            </div>
                        </div>
                        <div id="equipments" class="col-12"></div>
                        <input type="hidden" name="equipments" id="equips" value="{{$model->equipments ?? "[]"}}">
                        <input type="hidden" name="gas_meter" id="gas_meter" value="{{$model->gas_meter ?? "[]"}}">
                    </div>


                    <div class="card-body col-md-12" id="add_rec">
                        <button type="button" id="add_equip" class="btn btn-block btn-outline-success" onclick="addEquipment()" data-index="1">
                            @lang('table.rec.add_equipment') <i class="fas fa-plus-square"></i>
                        </button>
                    </div>
                </div>

            @elseif($type == 'fail')
                <div class="row">
                    <div class="card-body col-md-6">
                        <div class="form-group row">
                            <label for="type" class="col-sm-4 col-form-label">@lang('table.rec.rec_number')</label>
                            <div class="col-8">
                                <input type="text" class="form-control" id="exampleInputAddress" disabled
                                       placeholder="{{__('table.rec.input_address')}}"
                                       value="{{$model->id ?? \App\Models\Recommendation::getNextId()}}">
                            </div>
                        </div>
                    </div>
                    <div class="card-body col-md-6">
                        <div class="form-group row">
                            <label for="type" class="col-sm-4 col-form-label">@lang('table.date')</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" style="padding-top: 10px; padding-bottom: 10px"><i class="far fa-calendar-alt"></i></span>
                                    </div>
                                    <input type="date" name="rec_date" {{--id="data-mask"--}} class="form-control"
                                           value="{{ now()->format('Y-m-d') }}" disabled>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-body col-md-12">
                        <label for="exampleInputDescription"
                               class="col-sm-4 col-form-label">@lang('table.rec.cancel_description')</label>
                        <input type="text" name="description" class="form-control" id="exampleInputDescription"
                               placeholder="@lang('table.rec.input_cancel_description')" value="{{$model->description ?? null}}">
                    </div>

                    <div class="card-body col-md-12">
                        <label for="summernote"
                               class="col-sm-4 col-form-label"><b>@lang('table.rec.cancel_information')</b></label>
                        <textarea class="form-control" rows="3" placeholder="@lang('table.rec.content') ..." name="additional" style="margin-top: 0; margin-bottom: 0; height: 200px;">@if($model->status == 0){{$model->additional}}@endif</textarea>
                    </div>
                </div>
            @endif

            <input type="hidden" name="prop_id" value="{{ $proposition['id'] }}">
            <input type="hidden" id="type" name="type" value="{{ $type }}">
            <button type="submit" id="submit" class="btn btn-success">{{__('table.propositions.save')}}</button>
        </form>
    </div>
</div>
