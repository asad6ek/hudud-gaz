@extends('voyager::master')
@section('head_css')
    <link rel="stylesheet" href="{{asset('css/datatables/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/datatables/responsive.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/datatables/buttons.bootstrap4.min.css')}}">
@stop

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
{{--                    @dd($errors->all(), $errors->any())--}}
                    @include('voyager::alerts')
                    @include('voyager::components.errors', ['errors' => $errors])
                    <div class="card">
                        <div class="card-body pt-3">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>{{__('table.propositions.prop_number')}}</th>
                                        <th>{{__('table.propositions.district')}}</th>
                                        <th>{{__('table.propositions.customer')}}</th>
                                        <th>{{__('table.recommendation')}}</th>
                                        <th>{{__('table.propositions.date')}}</th>
                                        <th>{{__('table.propositions.time_limit')}}</th>
                                        <th>{{__('table.propositions.action')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($models as $model)
                                    @php
                                        $var = $model->percent($model);
                                        $percent = $var['percent'];
                                    @endphp
                                    <tr>
                                        <td>{{$model->number}}</td>
                                        <td>{{$model->region->region_name}}</td>
                                        <td>{{$model->applicantable->name}}</td>
                                        <td>
                                            <a target="new"
                                               href="{{route('admin.recommendations.show', ['proposition' => $model, 'path' => $model->recommendation->path])}}">
                                                {{__('table.rec.read_rec')}}
                                            </a>
                                        </td>
                                        <td>{{$model->recommendation->created_at}}</td>
                                        <td style="width: 150px; text-align: center">
                                            <div class="progress progress-xs" style="margin-top: 10px;">
                                                <div class="progress-bar"
                                                     style="width: {{$percent}}%; background-color: rgb({{$model->colorForPercent($percent)}})"></div>
                                            </div>
                                            {{ $var['text'] }}
                                        </td>
                                        @if(user_role('region'))
                                            <td>
                                                <div class="text-center">
                                                    <input type="file" accept=".pdf" id="fileInput_{{$loop->index}}" onchange="uploadPdf(this, {{$model->recommendation->id}},
                                                        '{{route('admin.recommendations.upload', ['recommend' => $model->recommendation->id])}}')"
                                                           style="opacity: 0; width: 1px">
                                                    <a class="btn  btn-info" href="#" data-toggle="tooltip" title="{{__('table.rec.upload')}}">
                                                        <label class="" for="fileInput_{{$loop->index}}" style="margin-bottom: 0;">
                                                            <img src="http://100dayscss.com/codepen/upload.svg"
                                                                 class="upload-icon w-25"/>
                                                            {{__('table.rec.upload_btn')}}
                                                        </label>
                                                    </a>
                                                </div>
                                            </td>
                                        @else
                                            <td>
                                                <a class="btn btn-success" data-toggle="tooltip"
                                                   title="{{__('table.rec.confirm')}}"
                                                   href="{{route('admin.tech_conditions.create',['recommendation' => $model->recommendation->id])}}">
                                                    <i class="fas fa-check"></i>
                                                </a>
                                                <a class="btn btn-danger" data-toggle="tooltip" onclick="write_comment({{$model->recommendation->id}})"
                                                   title="{{__('table.rec.back')}}"
                                                   href="#back">
                                                    <i class="fas fa-undo-alt"></i>
                                                </a>
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('body_js')

    @include('voyager::settings.scripts')

    <script>
        $("#example1").DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            // "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');


        function write_comment(id) {
            Swal.fire({
                title: "{{__('table.rec.write_comment')}}",
                input: 'text',
                inputAttributes: {
                    autocapitalize: 'off'
                },
                showCancelButton: true,
                confirmButtonText: "{{__('table.send_to_region')}}",
                cancelButtonText: "{{__('table.btn_cancel')}}",
                showLoaderOnConfirm: true,
                preConfirm: (comment) => {
                    $.ajax({
                        url: '/admin/recommendations/' + id + '/back',
                        type: 'POST',
                        data: {_token: $('meta[name="csrf-token"]').attr('content'), comment: comment}
                    }).done(function (result){
                        location.reload()
                    })
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire({
                        title: '{{__('table.rec.come_back')}}',
                    })
                    location.reload()
                }
            })
        }

        function uploadPdf(el, id, url) {

            let myFormData = new FormData();
            let file = el.files[0];

            if (file.type != 'application/pdf')
            {
                return Swal.fire({
                    title: '{{__('validation.mimes', ['attribute' => __('table.recommendation')])}}',
                    text: "",
                    icon: 'warning',
                    showConfirmButton: true,
                });
            }

            loader();

            myFormData.append('recommendation', file);
            myFormData.append('id', id);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: url,
                type: 'POST',
                processData: false, // important
                contentType: false, // important,
                cache: false,
                dataType: 'html',
                data: myFormData,
                success: function() {
                    location.reload()
                }
            });
        }
    </script>
@stop

