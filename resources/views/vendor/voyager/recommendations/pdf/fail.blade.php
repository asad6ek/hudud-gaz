<!DOCTYPE html>
<html lang="uz">
<head>
    <meta charset="UTF-8">
    <title>Tavsiyanoma</title>
    <style>
        * {
            padding: 0;
            margin: 0;
            font-size: 14px;
        }

        html, body {
            height: 100%;
        }

        body {
            display: flex;
            flex-direction: column;
            padding-left: 20mm;
            padding-right: 15mm;
            padding-top: 20mm;
            text-align: justify;
            line-height: 1.3;
        }

        table {
            width: 100%;
        }

        td {
            width: 50%;
            vertical-align: top;
        }

        hr {
            border: 0;
            border-top: 1px solid #000000;
        }

        li {
            margin-top: 10px;
            margin-bottom: 10px;
        }

        .clear {
            clear: both;
        }

        .mt-20 {
            margin-top: 20px;
        }

        .text-right {
            text-align: right;
        }

        .text-bold {
            font-weight: bold;
        }

        .lowercase {
            text-transform: lowercase;
        }

        .f-l {
            float: left;
        }

        .f-r {
            float: right;
        }

        .f-10 {
            font-size: 10px;
        }
    </style>
</head>
<body>
    <div class="text-bold">
        <span class="f-l">№ {{$data['rec_id']}}</span>
        <span class="f-r"><u>{{now()->format('m.d.Y')}}</u></span>
        <div class="clear"></div>
    </div>

    <div class="text-bold">
        <div class="text-right mt-20">
            <span>Давлат хизматлари агентлиги</span><br>
            <span>Хоразм вилояти бошқармаси</span><br>
            <span>{{$data['region']['region_name']}} марказига</span>
        </div>
    </div>

    <p class="mt-20">
        &nbsp;&nbsp;&nbsp;&nbsp;"{{$data['general']['shareholder_name'] ?? 'XududGazTa\'minot'}}" AЖ "{{$data['general']['branch_name'] ?? 'Xududgaz Xorazm'}}" ГТФ "{{$data['org_name']}}" газ таъминоти бўлими
        сизнинг <span class="lowercase">{{extendedDate($data['proposition']->created_at, true)}}</span>даги {{$data['prop_id']}}-сонли хатингизга жавобан қуйидагиларни маълум қилади.
    </p>

    <div>
        <span class="f-l">&nbsp;&nbsp;&nbsp;&nbsp;</span>
        {{$data['additional']}}
    </div>

    <table class="text-bold mt-20">
        <tr>
            <td>
                "{{$data['org_name']}}" газ <br>
                таъминоти бўлими бош муҳандиси:
            </td>
            <td class="text-right">
                {{$data['region_engineer']}}
            </td>
        </tr>
    </table>
    <br>

    <table class="text-bold mt-20">
        <tr>
            <td class="f-10">
                {{$data['region']['org_number'] ?? ''}}, {{$data['region']['address_latin'] ?? ''}} <br>
                Telefonlar: {{$data['region']['phone'] ?? ''}}, Faks: {{$data['region']['fax'] ?? ''}} <br>
                Email: {{$data['region']['email'] ?? ''}}
            </td>
            <td>
                <div class="f-r f-10">
                    {{$data['region']['org_number'] ?? ''}}, {{$data['region']['address'] ?? ''}} <br>
                    Телефонлар: {{$data['region']['phone'] ?? ''}}, Факс: {{$data['region']['fax'] ?? ''}} <br>
                    Эмаил: {{$data['region']['email'] ?? ''}}
                </div>
            </td>
        </tr>
    </table>
</body>
</html>
