<!DOCTYPE html>
<html lang="uz">
<head>
    <meta charset="UTF-8">
    <title>Tavsiyanoma</title>
    <style>
        * {
            font-size: 14px;
        }

        .container {
            margin-top: 0;
        }

        .f-l {
            float: left;
        }

        .f-r {
            float: right;
        }

        .clear {
            clear: both;
        }
    </style>
</head>
<body>
<div class="container">
    <div style="position:relative; right: 50px; text-align: right;">
        <h4>"ТAСДИҚЛAЙМAН"<br>"{{$data['org_name']}}" бўлими <br>
            бошлиғи <span style="text-decoration: underline;">  {{$data['region_leader']}}  </span>
        </h4>
    </div><br>
    <p style="margin: 0; text-align: center;">
        ТЕХНИК ТAВСИЯНОМA <br>
        <b>
            № <span style="text-decoration: underline;">{{$data['rec_id']}}</span>
        </b>
    </p>

    <div>
        <span class="f-l" style="margin-left: 40px"><b>{{now()->format('d.m.Y')}} йил</b></span>
        <span class="f-r"><b>{{$data['region_name']}}</b></span>
        <div class="clear"></div>
    </div>
    <ol>
        <li>
            <span><b>Обектнинг номи: </b> {{$data['address']}}даги фуқоро {{$data['customer']}}га қарашли</span>
        @if($data['activity_type']->id > 1)
            <span>"{{$data['activity_type']->activity_name}}"</span>
        @endif
            <span style="text-transform: lowercase">{{strtolower($data['building_type'])}} биносини табиий газ таминотига улаш</span>
        </li>
        <li>
            <b>Уланиш имкони бор манзил: {{$data['access_point']}} </b><br>
            <p style="width: 80%; border-bottom: 1px solid black; margin-bottom: 10px; margin-top: 30px;"></p> <br><br>
            <p style="margin: 0; font-size: 12px; text-align: center;">
                (уланиш нуқтасининг аниқ манзили)
            </p>
            <p style="width: 80%; border-bottom: 1px solid black; margin-bottom: 3px;">
                {{$data['key']}}: {{$data['length']}} м
            </p>
            <p style="margin: 0; font-size: 12px; text-align: center;">
                (уланиш нуқтасидан газ тармоғигача бўлган масофа)
            </p>
        </li>

        <li>
            <b>
                Уланиш нуқтасида газ қувурининг диамтери ва чуқурлиги:
            </b>
            {{$data['pipe_size']}}, {{$data['depth']}} м
        </li>

        <li>
            <b>Уланиш нуқтасидаги газ босими: </b> <br>
            Ўртача қишги: {{$data['avg_winter']}} <span style="margin-left: 50px;">Ўртача ёзги:  {{ $data['avg_summer'] }}</span>
        </li>

        <li>
            <div>
                <span><b>Газ қувурининг лойиҳавий ўтказиш қобилияти:</b></span>
                <span style="float: right"><u>{{$data['capacity']}}</u> м<sup>3</sup>/c йил</span>
            </div>
            <div style="clear: both"></div>
            <table style="text-align: right; width: 100%">
                <tr>
                    <td>Ҳақиқатдан:</td>
                    <td><u>{{$data['real_capacity']}}</u> м<sup>3</sup>/c йил</td>
                </tr>
            </table>
        </li>

        <li>
            <span>Лойиҳада газ таъминоти қайси <b>ГРC</b>дан берилади: <b>{{$data['grc']}}-ГРC</b></span>
            <table style="text-align: right; width: 100%">
                <tr>
                    <td>Ҳақиқатдан:</td>
                    <td><b><u>{{$data['grc']}}</u>-ГРC</b></td>
                </tr>
            </table>
        </li>

        <li>Келтирувчи газ тармоғининг номи: {{$data['gas_network']}} @if($data['pipe_type'] == 1) паст @else юқори @endif босимли ер ости ______</li>

        <li>Ўртача талаб қилинадиган газ ҳажми: <u>{{$data['gas_consumption']}}</u> м<sup>3</sup></li>
    </ol>
    {!! $data['additional'] !!}

    <ol>
        <li value="12">
            Ўрнатилган газ асбоблари:
            @foreach($data['json'] ?? [] as $equipment)
                {{$equipment->number}} та {{ $equipment->type}}@if(!$loop->last).@else,@endif
            @endforeach
        </li>
    </ol>

    <p style="margin: 0; text-align: center; bottom: 50px">
        ТЕХНИК ТAВСИЯНОМA ЛОЙИҲAЛAШ УЧУН AСОС БЎЛA ОЛМAЙДИ
    </p>

    <div style="position: absolute; float: left; bottom: 50px;">
        <div style="font-weight: bold">
            "{{$data['branch_name']}}" ГТФга қарашли <br>
            "{{$data['org_name']}}" бўлими бош муҳандиси: <span style="float: right">{{$data['region_engineer']}}</span>
        </div>
    </div>
</div>
</body>
</html>
