@extends('voyager::master')

@section('page_title', 'Ҳудудгаз таъминот Хоразм филиали')

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    @include('voyager::alerts')

                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">{{__('table.regions.edit_now_org')}}</h3>
                        </div>

                        @include('voyager::regions.form',[
                             'model' => $model,
                             'method' => 'PUT',
                             'action' => route('admin.regions.update', ['region'=>$model])
                         ])
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
