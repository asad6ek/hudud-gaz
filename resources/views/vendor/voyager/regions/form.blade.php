<form action="{{$action}}" method="post">
    @csrf
    @method($method)
    @includeFirst(['voyager::components.errors'],['errors' => $errors])

    <div class="card-body">
        <div class="form-group row">
            <label for="org_number" class="col-3">{{__('table.regions.org_number')}}</label>
            <div class="col-9">
                <input type="number" name="org_number" value="{{$model->org_number}}" class="form-control" id="org_number">
            </div>
        </div>

        <div class="form-group row">
            <label for="region_name" class="col-3">{{__('table.regions.org_region_name')}}</label>
            <div class="col-9">
                <select name="region_name" id="region_name" class="custom-select" required>
                    <option value="">{{__('table.montage.select_hint')}}</option>
                    @foreach($regions as $key => $region)
                        <option value="{{$key}}"
                                @if ($region == $model->region_name) selected @endif>
                            {{$region}}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label for="org_name" class="col-3">{{__('table.regions.org_name')}}</label>
            <div class="col-9">
                <input type="text" name="org_name" value="{{$model->org_name}}" class="form-control" id="org_name">
            </div>
        </div>

        <div class="form-group row">
            <label for="lead_engineer" class="col-3">{{__('table.regions.leader')}}</label>
            <div class="col-9">
                <input type="text" name="lead_engineer" value="{{$model->lead_engineer}}" class="form-control" id="lead_engineer">
            </div>
        </div>

        <div class="form-group row">
            <label for="section_leader" class="col-3">{{__('table.regions.section_leader')}}</label>
            <div class="col-9">
                <input type="text" name="section_leader" value="{{$model->section_leader}}" class="form-control" id="section_leader">
            </div>
        </div>

        <div class="form-group row">
            <label for="email" class="col-3">{{__('table.regions.email')}}</label>
            <div class="col-9">
                <input type="email" name="email" value="{{$model->email}}" class="form-control" id="email" required>
            </div>
        </div>

        <div class="form-group row">
            <label for="phone" class="col-3">{{__('table.regions.phone')}}</label>
            <div class="col-9">
                <textarea name="phone" id="phone" class="form-control">{{$model->phone}}</textarea>
            </div>
        </div>

        <div class="form-group row">
            <label for="address_latin" class="col-3">{{__('table.regions.address_latin')}}</label>
            <div class="col-9">
                <input type="text" name="address_latin" value="{{ $model->address_latin }}" class="form-control" id="address_latin">
            </div>
        </div>

        <div class="form-group row">
            <label for="address" class="col-3">{{__('table.regions.address')}}</label>
            <div class="col-9">
                <input type="text" name="address" value="{{ $model->address }}" class="form-control" id="address">
            </div>
        </div>

        <div class="form-group row">
            <label for="fax" class="col-3">{{__('table.regions.fax')}}</label>
            <div class="col-9">
                <input type="text" name="fax" value="{{ $model->fax }}" class="form-control" id="fax">
            </div>
        </div>
    </div>
    <div class="card-footer">
        <button type="submit" class="btn btn-primary">{{__('table.montage.button_save')}}</button>
        <a href="{{route('admin.regions.index')}}" class="btn btn-outline-danger ml-2">{{__('table.montage.button_previous')}}</a>
        <button type="reset" class="btn btn-outline-secondary float-right">{{__('table.general_settings.button_clear')}}</button>
    </div>
</form>
