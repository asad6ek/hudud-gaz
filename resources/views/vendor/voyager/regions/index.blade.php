
@extends('voyager::master')

@section('page_title', 'Ҳудудгаз Хоразм филиали')

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @include('voyager::alerts')
                    <div class="card">
                        <div class="card-header">
                            @can('add_users')
                                <a class="btn btn-success" href="{{ route('admin.regions.create') }}" role="button">{{__('table.regions.button_add_new')}}</a>
                            @endcan

                            <div class="card-tools mt-2">
                                <div class="input-group input-group-sm" style="width: 150px; font-size: 14px;">
                                    <input type="text" id="table_search" name="table_search" class="form-control float-right"
                                           placeholder="{{__('table.montage.search_field')}}">

                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-default">
                                            <small><i class="fas fa-search"></i></small>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body table-responsive p-0">
                            <table class="table table-hover text-nowrap">
                                <thead>
                                <tr>
                                    <th>№</th>
                                    <th>{{__('table.regions.col_org_number')}}</th>
                                    <th>{{__('table.regions.col_org_name')}}</th>
                                    <th>{{__('table.regions.col_org_leader')}}</th>
                                    <th>{{__('table.regions.col_org_email')}}</th>
                                    <th>{{__('table.regions.col_org_phone')}}</th>
                                    <th>{{__('table.regions.col_org_address')}}</th>
                                    <th>{{__('table.regions.col_org_fax')}}</th>
                                    <th></th>
                                </tr>
                                </thead>

                                <tbody>

                                @foreach($model as $region)
                                    <tr>
                                        <td>{{ $loop->index + 1 }}</td>
                                        <td>{{ $region->org_number }}</td>
                                        <td>{{ $region->org_name }}</td>
                                        <td>{{ $region->lead_engineer }}</td>
                                        <td>{{ $region->email }}</td>
                                        <td>{{ $region->phone }}</td>
                                        <td>{{ $region->address }}</td>
                                        <td>{{ $region->fax }}</td>
                                        <td>
                                            <form method="post" id="form_delete-{{$region->id}}" style="margin-top: -5px;"
                                                  action="{{ route('admin.regions.delete', ['region'=>$region->id]) }}">
                                                @can('edit_users')
                                                    <a class="btn btn-warning" title="{{__('table.montage.button_edit')}}"
                                                       href="{{ route('admin.regions.edit', ['region'=>$region]) }}"
                                                       role="button"><i class="fas fa-pencil-alt"></i></a>
                                                @endcan
                                                @can('delete_users')
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="button" onclick="confirm('{{$region->id}}')" title="{{__('table.montage.button_delete')}}" class="btn btn-danger"
                                                            role="button"><i class="far fa-trash-alt"></i>
                                                    </button>
                                                @endcan
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
@section('body_js')
    <script>
        // $(this).val()
        document.addEventListener('DOMContentLoaded', function () {
            $('#table_search').keyup(function () {
                let qiymat = this.value.toLowerCase();
                $('tbody tr').filter(function () {
                    $(this).toggle($(this).text().toLowerCase().indexOf(qiymat) > - 1)
                })
            });
        })

        function confirm(id) {
            Swal.fire({
                title: '{{__('table.montage.delete_message')}}',
                text: "",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: '{{__('table.montage.button_yes')}}',
                cancelButtonText: '{{__('table.montage.button_no')}}',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $(`#form_delete-${id}`).submit()
                    Swal.fire({
                        title: '{{__('table.montage.deleting_finish')}}',
                        icon: 'success',
                        showConfirmButton: false,
                    })
                }
            });
        }
    </script>
@stop
