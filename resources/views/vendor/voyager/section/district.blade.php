@extends('voyager::master')
@section('head_css')
    <link rel="stylesheet" href="{{asset('css/datatables/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/datatables/responsive.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/datatables/buttons.bootstrap4.min.css')}}">
@stop
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <form action="">
                                <div class="row">
                                    <div class="col-3 form-group">
                                        <select name="section" id="type" class="form-control">
                                            <option value="0" @if($section == 0)selected @endif>Hammasi</option>
                                            <option value="1" @if($section == 1)selected @endif>Ijobiy</option>
                                            <option value="2" @if($section == 2)selected @endif> Salbiy</option>
                                        </select>
                                    </div>
                                    <div class="col-3 form-group">
                                        <button class="btn btn-success" type="submit">Излаш</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Туман</th>
                                    <th>Жами</th>
                                    @foreach ($activityTypes as $key => $item)
                                        <th>{{$item}}</th>
                                    @endforeach

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($models as $model)
                                    <tr>
                                        <td>{{$model->org_name}}</td>

                                        <td>{{$model->propositions->count()}}</td>
                                        @php
                                            $activity_types = $model->propositions->groupBy('activity_type')
                                        @endphp

                                        @foreach ($activityTypes as $key => $item)
                                            <td>{{isset($activity_types[$key])?$activity_types[$key]->count():0}}</td>
                                        @endforeach
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('body_js')
    @include('voyager::settings.scripts')

    <script>
        $(function () {
            $("#example1").DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": false,
                "autoWidth": false,
                "responsive": true,
                "order": false,
                // "scrollX": true,
                // "scrollY": true
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
        });
    </script>
@stop
