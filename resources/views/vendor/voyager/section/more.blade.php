@extends('voyager::master')
@section('head_css')
    <link rel="stylesheet" href="{{asset('css/datatables/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/datatables/responsive.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/datatables/buttons.bootstrap4.min.css')}}">
@stop

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @include('voyager::alerts')
                    <div class="card">
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>{{__('table.propositions.prop_number')}}</th>
                                    <th>{{__('table.propositions.district')}}</th>
                                    <th>{{__('table.propositions.proposition')}}</th>
                                    <th>{{__('table.propositions.date')}}</th>
                                    <th>{{__('table.propositions.status')}}</th>
                                    @can('show_more_information')
                                        <th>{{__('table.full_information')}}</th>
                                    @endcan
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($models as $model)
                                    <tr>
                                        <td>{{$model->number}}</td>
                                        <td>{{$model->region->region_name}}</td>
                                        <td>
                                            <a target="new" href="{{route('admin.propositions.show',['proposition' => $model->id])}}">
                                                {{__('table.propositions.read')}}
                                            </a>
                                        </td>
                                        <td>{{$model->created_at}}</td>
                                        <td>{{ $model->getStatus->description }}</td>
                                        @can('show_more_information')
                                            <td>
                                                <a class="btn btn-primary" href="{{route('admin.propositions.show_more', ['proposition' => $model->id])}}">
                                                    <i class="fas fa-eye"></i>
                                                </a>
                                            </td>
                                        @endcan
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('body_js')
    @include('voyager::settings.scripts')

    <script>
        function confirmDelete(form) {
            Swal.fire({
                title: '{{__('table.propositions.delete_alert')}}',
                text: "",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: '{{__('table.montage.button_yes')}}',
                cancelButtonText: '{{__('table.montage.button_no')}}',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $(`#${form}`).submit()
                    Swal.fire({
                        title: '{{__('table.montage.deleting_finish')}}',
                        icon: 'success',
                        showConfirmButton: false,
                    })
                }
            });
        }

        $(function () {
            $("#example1").DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
                "order": false,
                // "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
        });
    </script>
@stop
