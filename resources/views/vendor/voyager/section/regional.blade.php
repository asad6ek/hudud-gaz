@extends('voyager::master')
@section('head_css')
    <link rel="stylesheet" href="{{asset('css/datatables/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/datatables/responsive.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/datatables/buttons.bootstrap4.min.css')}}">
@stop

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row ">
                <div class="col-md-12 mt-2">
                    <div class="card card-outline card-primary">
                        <div class="card-header">

                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Жами</th>
                                        @foreach ($activities as $item)
                                            <th>{{$item}}</th>
                                        @endforeach
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Жами аризалар</td>
                                        <td> {{$models->count()}}</td>
                                        @php
                                            $model = $models->groupBy('activity_type')
                                        @endphp
                                        @foreach ($activities as $key => $item)

                                            <td> {{isset($model[$key])?$model[$key]->count():0}}</td>
                                        @endforeach


                                    </tr>
                                    @php
                                        $model = $models->groupBy('status');
                                    @endphp
                                    @foreach($statuses as $status)
                                        <tr>
                                            <td>
                                                {{$status->description}}
                                            </td>
                                            <td>
                                                {{ isset($model[$status->id]) ? $model[$status->id]->count() : 0}}
                                            </td>
                                            @php

                                                $data = isset($model[$status->id]) ?$model[$status->id]->groupBy('activity_type') : null;
                                            @endphp
                                            @foreach ($activities as $key => $item)
                                                <td> {{isset($data[$key])?$data[$key]->count():0}}</td>
                                            @endforeach
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
@section('body_js')
    @include('voyager::settings.scripts')

    <script>
        $(function () {
            $("#example1").DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": false,
                "autoWidth": false,
                "responsive": true,
                "order": false,
                // "scrollX": true,
                // "scrollY": true
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
        });
    </script>
@stop
