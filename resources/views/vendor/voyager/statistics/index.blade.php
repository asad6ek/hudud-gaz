@extends('voyager::master')

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3>{{$all}}</h3>
                            <p>Jami arizalar soni</p>
                        </div>
                        <div class="icon">
                            <i class="fas fa-shopping-cart"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <!-- small card -->
                    <div class="small-box bg-success">
                        <div class="inner">
                            <h3>{{$success}}</h3>

                            <p>Ijobiy hal etilgan arizalar</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-stats-bars"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <!-- small card -->
                    <div class="small-box bg-warning">
                        <div class="inner">
                            <h3>{{$process}}</h3>

                            <p>Jarayondagi arizalar</p>
                        </div>
                        <div class="icon">
                            <i class="fas fa-user-plus"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="small-box bg-danger">
                        <div class="inner">
                            <h3>{{$fail}}</h3>
                            <p>Bekor qilinganlar</p>
                        </div>
                        <div class="icon">
                            <i class="fas fa-chart-pie"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <!-- AREA CHART -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Kelib tushgan arizalar</h3>
                        </div>
                        <div class="card-body">
                            <div class="chart">
                                <canvas id="areaChart" style="min-height: 250px; height: 275px; max-height: 100%; max-width: 100%;"></canvas>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4" style="overflow-y: scroll; height: 360px;">
                    @php($colors = ['bg-info', 'bg-success', 'bg-danger', 'bg-warning'])
                    @foreach($activities as $key => $activity)
                    <div class="info-box">
                        <span class="info-box-icon {{$colors[$key % count($colors)]}}">
                            <i class="fas fa-envelope-open-text"></i>
                        </span>
                        <div class="info-box-content">
                            <span class="info-box-text">{{$activity}}</span>
                        @if(isset($props_group[$key]))
                            <span class="info-box-number">{{$props_group[$key]->count()}}</span>
                        @else
                            <span>0</span>
                        @endif
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="card">
                <div class="card-header ui-sortable-handle">
                    <h3 class="card-title">
                        <i class="ion ion-clipboard mr-1"></i>
                        Bugungi arizalar
                    </h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">Jami arizalar: <span class="ml-5"><b>{{$in_today}}</b></span></div>
                        <div class="col-md-4">Muddati o'tgan arizalar: <span class="ml-5"><b>{{$in_late}}</b></span></div>
                        <div class="col-md-4">Jarayondagi arizalar: <span class="ml-5"><b>{{$in_progress}}</b></span></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('body_js')

    <script src="{{ asset('js/Chart.min.js') }}"></script>

    <script>let points = @json($points);</script>

    <script src="{{ asset('js/chart.js') }}"></script>

@stop
