@extends('voyager::master')
@section('head_css')
    <link rel="stylesheet" href="{{asset('css/datatables/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/datatables/responsive.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/datatables/buttons.bootstrap4.min.css')}}">
@stop

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @include('voyager::alerts')
                    <div class="card">
                        <div class="card-body pt-3">
                            <table id="example" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>№</th>
                                        <th>{{__('table.propositions.customer')}}</th>
                                        <th>{{__('table.license')}}</th>
                                        <th>{{__('table.propositions.district')}}</th>
                                        <th>{{__('table.propositions.date')}}</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($models as $key=>$model)
                                    <tr>
                                        <td>{{$key + 1}}</td>
                                        <td>{{$model->proposition->applicantable->name ?? $proposition->applicantable->full_name}}</td>
                                        <td>
                                            <a href="{{route('admin.license.show', ['license' => $model])}}" target="_blank">@lang('table.show_license')</a>
                                        </td>
                                        <td>{{$regions[$model->district]}}</td>
                                        <td>{{dateFixed($model->created_at)}}</td>
                                        <td>
                                            @if($model->status == 1)
                                                @can('edit_montages')
                                                    <form action="{{route('admin.license.upload', ['license' => $model])}}" method="post" enctype="multipart/form-data">
                                                        @csrf
                                                        <input type="file" name="file" id="file-{{$key}}" onchange="this.parentNode.submit()" class="d-none">
                                                        <label for="file-{{$key}}" class="btn btn-info ml-5 my-0" title="@lang('table.btn_upd')">
                                                            <i class="fas fa-upload"></i>
                                                        </label>
                                                    </form>
                                                @endcan
                                            @else
                                                <span>@lang('table.uploaded')</span>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('body_js')
    @include('voyager::settings.scripts')
    <script>
        $("#example").DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
        });
    </script>
@stop
