@extends('voyager::master')
@section('content')
<div class="container">
    @include('voyager::alerts')
    <div class="card">
        <div class="card-header">
            <h3 class="my-auto ml-3">{{__('table.statuses.heading')}}</h3>
        </div>
        <div class="card-inline-block p-2">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">{{__('table.rec.add_inf')}}</h3>
                </div>
                <form method="post" action="{{route('admin.statuses.update', ['status' => $status])}}">
                    @csrf
                    @method('PUT')
                    @includeFirst(['voyager::components.errors'], ['errors' => $errors])
                    <div class="card-body">
                        <div class="form-group row">
                            <label for="description" class="col-sm-2 col-form-label">{{__('table.statuses.description')}}</label>
                            <div class="col-sm-10">
                                <input id="description" type="text" name="description" class="form-control" value="{{$status->description}}" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="permissions" class="col-sm-2 col-form-label">{{__('table.statuses.permissions')}}</label>
                            <div class="col-sm-10">
                                <input id="permissions" type="text" name="permissions" class="form-control" value="{{$status->permissions}}" disabled>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="minutes" class="col-sm-2 col-form-label">{{__('table.statuses.hours')}}</label>
                            <div class="col-sm-10">
                                <input id="minutes" type="number" name="minutes" class="form-control" value="{{round($status->minutes / 60, 2)}}" step=".01">
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-success">{{__('table.propositions.save')}}</button>
                        <a href="{{route('admin.statuses.index')}}" class="btn btn-outline-secondary ml-2">{{__('table.montage.button_previous')}}</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop

