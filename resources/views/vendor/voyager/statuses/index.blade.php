@extends('voyager::master')

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @include('voyager::alerts')
                    <div class="card">
                        <div class="card-header">
                            <div class="card-tools mt-2">
                            </div>
                        </div>
                        <div class="card-body table-responsive p-0">
                            <table class="table table-hover text-nowrap">
                                <thead>
                                <tr>
{{--                                    <th>@lang('table.statuses.index')</th>--}}
                                    <th>@lang('table.statuses.description')</th>
                                    <th>@lang('table.statuses.permissions')</th>
                                    <th>@lang('table.statuses.hours')</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($models as $model)
                                    <tr>
                                        <td>{{$model->description}}</td>
                                        <td>{{$model->permissions}}</td>
                                        <td>{{round($model->minutes / 60, 2)}}</td>
                                        <td>
                                            <a href="{{route('admin.statuses.edit',['status' =>$model])}}" class="btn btn-info"
                                               role="button">
                                                {{__('table.montage.button_edit')}}
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

