@extends('voyager::master')

@section('head_css')
    <link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
    <style>
        .select2-selection .select2-selection--single {
            padding-bottom: 30px !important;
        }
    </style>
@stop

@section('body_js')
    <!-- Summernote -->
    <script src=" {{asset('js/summernote-bs4.min.js')}} "></script>
    <script src="{{ asset('js/select2.full.min.js') }}"></script>

    <!-- Page specific script -->
    <script>
        $(function () {
            // Summernote
            $('#summernote').summernote()
            $('#summernote-1').summernote()
        })

    </script>

@stop

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!-- Basic Form Inputs card start -->
                    @include('voyager::alerts')

                    <div class="card">
                        <div class="card-header row">
                            <a href="{{route('admin.tech_conditions.index')}}" class="btn btn-warning">
                                {{__('table.back')}}
                            </a>
                            <h3 class="my-auto ml-3">{{__('table.rec.add_rec')}}</h3>
                        </div>
                        <div class="card-inline-block p-2">
                            @includeFirst(['voyager::tech_conditions.form'],[
                                            'method'=>'POST',
                                            'action'=>route('admin.tech_conditions.store'),
                                        ])
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

