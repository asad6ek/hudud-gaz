<div class="col-md-12">
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">{{__('table.rec.add_inf')}}</h3>
        </div>
        <form role="form" method="{{$method}}" action="{{$action}}">
            @csrf
            @includeFirst(['voyager::components.errors'],['errors' => $errors])
            @if($recommendation->status == 1)

                <div class="row">
                    <div class="card-body col-md-12">
                      <textarea id="summernote" name="content">
                            <ol>
                                <li>
                                    Техник тавсия берган газ таъминоти номи, санаси рақами ва истеъмолчи номи, манзили ва фаолият тури: "{{$recommendation->proposition->region->name}}"
                                    филиалининг {{$recommendation->created_at->format('')}}даги № {{$recommendation->proposition->recommendation->id}} - сонли техник тавсияномасига асосан, {{$recommendation->address}}да жойлашган,
                                    {{$recommendation->applicant->name}}га қарашли нотурар биносини газлаштириш

                                </li>
                                <li>
                                    Газ қувурига уланиш жойи, ҳаракатдаги ер ости/усти, <b>паст</b> босимли газ тармогига, уланиш нуқтасига бўлган масофа:
                                    {{$recommendation->length}}-p/m, D-{{$recommendation->pipe_size}}mm,
                                    ўрт. қишги - {{$recommendation->avg_summer}} kgc/cm<sup>2</sup>, ўрт. ёзги - {{$recommendation->avg_winter}} kgc/cm<sup>2</sup>, <b>{{$recommendation->grc}}</b>-ГТC
                                </li>
                                <li>
                                    Соатлик, йиллик газ истеъмоли сарфи:  0 m<sup>3</sup>/soat, {{$recommendation->gas_consumption}} m<sup>3</sup>/yil
                                </li>
                                <li>
                                    Газ жиҳозлари:
                                    @foreach($equipments as $equipment)
                                      {{$equipment->type}} - {{$equipment->number}} дона @if(!$loop->last),@endif
                                    @endforeach.
                                </li>
                            </ol>
                      </textarea>
                    </div>

                    <div class="card-body col-md-12">
                      <textarea id="summernote-1" name="additional">@include('voyager::tech_conditions.pdf.static')</textarea>
                    </div>
                </div>

            @elseif($recommendation->status == 0)
                <div class="row">
                    <div class="card-body col-md-6">
                        <div class="form-group row">
                            <label for="type" class="col-sm-4 col-form-label">@lang('table.rec.rec_number')</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="exampleInputAddress" disabled
                                       placeholder="{{__('table.rec.input_address')}}"
                                       value="{{\App\Models\Recommendation::getNextId()}}">
                            </div>
                        </div>
                    </div>
                    <div class="card-body col-md-6">
                        <div class="form-group row">
                            <label for="type" class="col-sm-4 col-form-label">@lang('table.date')</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" style="padding-top: 10px; padding-bottom: 10px"><i class="far fa-calendar-alt"></i></span>
                                    </div>
                                    <input type="date" name="rec_date" {{--id="data-mask"--}} class="form-control"
                                           value="{{ now()->format('Y-m-d') }}" disabled>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-body col-md-12">
                        <label for="exampleInputAddress"
                               class="col-sm-4 col-form-label">@lang('table.rec.cancel_description')</label>
                        <input type="text" name="description" class="form-control" id="exampleInputAddress"
                               placeholder="@lang('table.rec.input_cancel_description')">
                    </div>

                    <div class="card-body col-md-12">
                        <label for="summernote"
                               class="col-sm-4 col-form-label"><b>@lang('table.rec.cancel_information')</b></label>
                        <textarea class="form-control" rows="3" placeholder="@lang('table.rec.content') ..."
                                  name="additional" style="margin-top: 0; margin-bottom: 0; height: 200px;"></textarea>
                    </div>

                </div>

            @endif

            <input type="hidden" name="prop_id" value="{{ $recommendation->prop_id }}">
            <input type="hidden" name="type" value="{{ $recommendation->status }}">

            <button type="submit" class="btn btn-success">{{__('table.propositions.save')}}</button>

        </form>
    </div>
</div>
