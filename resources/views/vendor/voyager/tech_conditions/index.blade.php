@extends('voyager::master')
@section('head_css')
    <link rel="stylesheet" href="{{asset('css/datatables/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/datatables/responsive.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/datatables/buttons.bootstrap4.min.css')}}">
@stop
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @include('voyager::alerts')
                    <div class="card">
                        <div class="card-body pt-3">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>{{__('table.propositions.prop_number')}}</th>
                                        <th>{{__('table.propositions.district')}}</th>
                                        <th>{{__('table.propositions.customer')}}</th>
                                        <th>{{__('table.tech_condition')}}</th>
                                        <th>{{__('table.propositions.date')}}</th>
                                        <th>{{__('table.propositions.time_limit')}}</th>
                                        <th>{{__('table.propositions.action')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($models as $model)
                                    @php
                                        $var = $model->percent($model);
                                        $percent = $var['percent'];
                                        $time = $var['time'];
                                    @endphp
                                    <tr>
                                        <td>{{$model->number}}</td>
                                        <td>{{$model->region->org_name}}</td>
                                        <td>{{$model->applicantable->name}}</td>
                                        <td>
                                            <a target="new"
                                               href="{{url('storage/tech_conditions/'.$model->tech_condition->path)}}">
                                                {{__('table.tech.read_tech')}}
                                            </a>
                                        </td>
                                        <td>{{$model->tech_condition->created_at}}</td>
                                        <td style="width: 150px; text-align: center">
                                            <div class="progress progress-xs" style="margin-top: 10px;">
                                                <div class="progress-bar"
                                                     style="width: {{$percent}}%; background-color: rgb({{$model->colorForPercent($percent)}})"></div>
                                            </div>
                                            {{ $var['text'] }}
                                        </td>
                                        <td>
                                            <input type="file" accept=".pdf" id="file_{{$model->id}}" hidden onchange="uploadPdf(this, {{$model->tech_condition->id}})">
                                            <label class="btn btn-info" for="file_{{$model->id}}" title="{{__('table.btn_upd')}}">
                                                <i class="fas fa-upload"></i>
                                            </label>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop


@section('body_js')

    @include('voyager::settings.scripts')

    <script>
        $("#example1").DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

        function uploadPdf(el, id) {
            let myFormData = new FormData();
            let file = el.files[0]
            myFormData.append('file', file);

            if (file.type != 'application/pdf')
            {
                return Swal.fire({
                    title: '{{__('validation.mimes', ['attribute' => __('table.tech_condition')])}}',
                    text: "",
                    icon: 'warning',
                    showConfirmButton: true,
                });
            }
            loader();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: '/admin/tech_conditions/' + id + '/upload',
                type: 'POST',
                processData: false, // important
                contentType: false, // important
                dataType: 'json',
                data: myFormData
            }).done(function (result){
                location.reload()
            });
        }
    </script>
@stop
