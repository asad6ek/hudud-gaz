<!DOCTYPE html>
<html lang="uz">
<head>
    <meta charset="UTF-8">
    <title>Success tech</title>
    <style>
        * {
            margin: 0;
            padding: 0;
        }

        body {
            font-size: 10px;
            padding-top: 10mm;
            padding-left: 15mm;
            padding-right: 15mm;
        }

        ol {
            padding-left: 20px;
            margin-top: 0;
        }

        .text-bold {
            font-weight: bold;
        }

        table {
            width: 100%;
        }

        .f-r {
            float: right;
        }
    </style>
</head>
<body>
    <div>
        <div style="float: left">
            <img style="width: 80px; margin-top: 5px" src="{{ 'storage/tech_conditions/qrcode.svg' }}" alt="img">
        </div>
        <div>
            <h4 style="text-align: right;">
                Давлат хизматлари маркази<br>
                Хоразм вилояти бошқармаси <br>
                {{$data['region']['region_name']}} марказига
            </h4>
        </div>
        <div style="clear: both"></div>
    </div>
    <br>

    <p style="margin: 0; text-align: center;">
        Сизнинг {{ today()->format('Y йил d-') . $data['month'] . 'даги' }} {{$data['prop_id']}}-сонли талабномангизга жавобан: <br>
        Табиий газ билан газлаштириш лойиҳасини ишлаб чиқиш ва газлаштиришга <br>
        <b>ТЕХНИК ШAРТ №</b> <span style="text-decoration: underline;">{{$data['id']}}  {{ today()->format(' «d» .m.') }}  </span> {{today()->year}}
    </p>
    <br>
    {!! $data['content'] !!}

    {!! $data['additional']  !!}

    <table class="text-bold" style="margin-top: 5px;">
        <tr>
            <td>"{{$data['region']['org_name'] ?? 'Test Filial'}}" газ <br>
                таъминоти бўлими бош муҳандиси:</td>
            <td>____________________</td>
            <td>
                {{ $data['region']['lead_engineer'] ?? '' }}
            </td>
        </tr>

        <tr>
            <td>
                Табиий газ тармоқларини комплекс эксплуатация<br>
                қилинишини назорат қилиш бўлим бошлиғи:
            </td>
            <td>____________________</td>
            <td>
                {{$data['general']['metrology'] ?? ''}}
            </td>
        </tr>
    </table>

    <table class="text-bold">
        <tr>
            <td>
                {{$data['general']['reg_num'] ?? '' }}, {{$data['general']['address_latin'] ?? ''}} <br>
                Telefonlar: {{$data['general']['phone_number'] ?? ''}}, Faks: {{$data['general']['fax'] ?? ''}} <br>
                Email: {{$data['general']['email_address'] ?? ''}}
            </td>
            <td>
                <div class="f-r">
                    {{$data['general']['reg_num'] ?? '' }}, {{$data['general']['address'] ?? ''}} <br>
                    Телефонлар: {{$data['general']['phone_number'] ?? ''}}, Факс: {{$data['general']['fax'] ?? ''}} <br>
                    Эмаил: {{$data['general']['email_address'] ?? ''}}
                </div>
            </td>
        </tr>
    </table>
</body>
</html>
