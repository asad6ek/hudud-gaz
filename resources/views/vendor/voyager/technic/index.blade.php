@extends('voyager::master')

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                @foreach($models as $model)
                    <div class="col-lg-3 col-6 m-3">
                        <div class="small-box bg-success">
                            <div class="inner">
                                <h3>{{$model->prop_count}}</h3>

                                <p>{{$model->org_name}}</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-stats-bars"></i>
                            </div>
                            <a href="{{route('technic.propositions',['region' => $model->id])}}" class="small-box-footer py-2">
                                <span>@lang('table.btn_show')</span>
                                <i class="fas fa-arrow-circle-right"></i>
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection
