<?php

use Illuminate\Support\Facades\Route;
Route::prefix('admin')->middleware('auth')->name('admin.')->group(function (){
//    if (auth()->user() == null) return redirect()->route('voyager.login');

    Route::get('/regional_section',[\App\Http\Controllers\SectionController::class,'region'])->name('regional_section.index');
    Route::get('/district_section',[\App\Http\Controllers\SectionController::class,'district'])->name('district_section.index');
    Route::get('/more',[\App\Http\Controllers\SectionController::class,'more'])->name('more.index');

});
