<?php

use Illuminate\Support\Facades\Route;


Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
    # Director
    Route::get('/statistics', [\App\Http\Controllers\StatisticController::class, 'index'])->name('admin.statistics.index');
    Route::get('/licenses', [\App\Http\Controllers\StatisticController::class, 'licenses'])->name('admin.license.index');
    Route::get('/licenses/{license}', [\App\Http\Controllers\StatisticController::class, 'show'])->name('admin.license.show');


    Route::get('/regional-section', [\App\Http\Controllers\SectionController::class, 'region'])->name('admin.regional_section.index');
    Route::get('/district-section', [\App\Http\Controllers\SectionController::class, 'district'])->name('admin.district_section.index');
    Route::get('/more', [\App\Http\Controllers\SectionController::class, 'more'])->name('admin.more.index');

    Route::post('/propositions/check_tin', [\App\Http\Controllers\PropositionController::class, 'check_tin'])->name('admin.propositions.check_tin');
    Route::get('/propositions/{proposition}/show_more', [\App\Http\Controllers\PropositionController::class, 'show_more'])->name('admin.propositions.show_more');
    resource('propositions', \App\Http\Controllers\PropositionController::class, 'admin.propositions');
    Route::get('/propositions/{tin}/{type}', [\App\Http\Controllers\PropositionController::class, 'show_application'])->name('admin.propositions.show_applicant');
    Route::get('/recommendations', [\App\Http\Controllers\RecommendationController::class, 'index'])->name('admin.recommendations.index');
    Route::get('/recommendations/process', [\App\Http\Controllers\RecommendationController::class, 'process'])->name('admin.recommendations.process');
    Route::get('/recommendations/cancelled', [\App\Http\Controllers\RecommendationController::class, 'cancelled'])->name('admin.recommendations.cancelled');
    Route::get('/recommendations/archive', [\App\Http\Controllers\RecommendationController::class, 'archive'])->name('admin.recommendations.archive');
    Route::post('/recommendations', [\App\Http\Controllers\RecommendationController::class, 'store'])->name('admin.recommendations.store');
    Route::put('/recommendations/{recommendation}', [\App\Http\Controllers\RecommendationController::class, 'update'])->name('admin.recommendations.update');
    Route::delete('/recommendations/{recommendation}', [\App\Http\Controllers\RecommendationController::class, 'destroy'])->name('admin.recommendations.delete');
    Route::post('/recommendations/{recommendation}/back', [\App\Http\Controllers\RecommendationController::class, 'back'])->name('admin.recommendations.back');
    Route::get('/recommendations/{recommendation}/edit', [\App\Http\Controllers\RecommendationController::class, 'edit'])->name('admin.recommendations.edit');
    Route::get('/recommendations/{proposition}/create/{type}', [\App\Http\Controllers\RecommendationController::class, 'create'])->name('admin.recommendations.create');
    Route::get('/recommendations/{proposition}/show/{path}', [\App\Http\Controllers\RecommendationController::class, 'show'])->name('admin.recommendations.show');
    Route::post('/recommendations/{recommend}/upload', [\App\Http\Controllers\RecommendationController::class, 'upload'])->name('admin.recommendations.upload');
    Route::get('/recommendation/pdf', [\App\Http\Controllers\RecommendationController::class, 'createPDF'])->name('admin.recommendations.create_pdf');
    Route::get('/employees/fetch/{type}', [\App\Http\Controllers\SettingsController::class, 'fetch_data'])->name('admin.employees.fetch_data');
    resource('employees', \App\Http\Controllers\EmployeesController::class, 'admin.employees');
    resource('equipment', \App\Http\Controllers\EquipmentController::class, 'admin.equipments');
    Route::post('/equipments/add_component', [\App\Http\Controllers\EquipmentController::class, 'add_component'])->name('admin.equipments.add_component');
    Route::post('/equipments/{equipment}', [\App\Http\Controllers\EquipmentController::class, 'save_equipment_type'])->name('admin.equipment_type.add');
    Route::post('/equipments/{equipment}/fetch_types', [\App\Http\Controllers\EquipmentController::class, 'fetch_types'])->name('admin.equipments.fetch_types');
    Route::delete('/equipments/{equipment}/delete/{equipmentType}', [\App\Http\Controllers\EquipmentController::class, 'delete_equipment_type'])->name('admin.equipment_type.delete');
    Route::post('/equipments/{equipment}/update/{equipmentType}', [\App\Http\Controllers\EquipmentController::class, 'update_equipment_type'])->name('admin.equipment_type.update');
    resource('regions', \App\Http\Controllers\RegionController::class, 'admin.regions');

    //projects routes
    Route::get('projects/index', [\App\Http\Controllers\ProjectController::class, 'index'])->name('admin.projects.index');
    Route::get('projects/process', [\App\Http\Controllers\ProjectController::class, 'process'])->name('admin.projects.process');
    Route::get('projects/cancelled', [\App\Http\Controllers\ProjectController::class, 'cancelled'])->name('admin.projects.cancelled');
    Route::get('projects/check', [\App\Http\Controllers\ProjectController::class, 'check'])->name('admin.projects.check');

    Route::get('projects/archive', [\App\Http\Controllers\ProjectController::class, 'archive'])->name('admin.projects.archive');
    Route::post('projects/{tech_condition}/upload', [\App\Http\Controllers\ProjectController::class, 'upload'])->name('admin.projects.upload');
    Route::get('projects/{tech}/show', [\App\Http\Controllers\ProjectController::class, 'show'])->name('admin.projects.show');
    Route::get('projects/{tech}/agree', [\App\Http\Controllers\ProjectController::class, 'agree'])->name('admin.projects.agree');
    Route::post('/projects/{tech}/back', [\App\Http\Controllers\ProjectController::class, 'back'])->name('admin.projects.back');
    Route::post('/projects/{tech}/cancel', [\App\Http\Controllers\ProjectController::class, 'cancel'])->name('admin.projects.cancel');

    // engineer routes
    Route::get('reports', [\App\Http\Controllers\ReportController::class,'index'])->name('admin.reports');
    Route::get('reports/{proposition}', [\App\Http\Controllers\ReportController::class,'show'])->name('admin.reports.show');
    Route::post('licenses/{license}/upload', [\App\Http\Controllers\ReportController::class,'upload'])->name('admin.license.upload');

//montages routes
    Route::get('/montages/archive', [\App\Http\Controllers\MontagesController::class, 'archive'])->name('admin.montage_firms.archive');
    Route::get('montages/check', [\App\Http\Controllers\MontagesController::class, 'check'])->name('admin.montages.check');
    Route::get('/montages/process', [\App\Http\Controllers\MontagesController::class, 'process'])->name('admin.montage_firms.process');
    Route::get('/montages/cancelled', [\App\Http\Controllers\MontagesController::class, 'cancelled'])->name('admin.montage_firms.cancelled');
    Route::post('/montages/{tech}/upload', [\App\Http\Controllers\MontagesController::class, 'upload'])->name('admin.montages.upload');
    Route::post('/montages/{tech}/back', [\App\Http\Controllers\MontagesController::class, 'back'])->name('admin.montage_firms.back');
    Route::post('/montages/{tech}/cancel', [\App\Http\Controllers\MontagesController::class, 'cancel'])->name('admin.montage_firms.cancel');
    resource('montages', \App\Http\Controllers\MontagesController::class, 'admin.montages');


    resource('designers', \App\Http\Controllers\DesignerController::class, 'admin.designers');
    resource('statuses', \App\Http\Controllers\StatusController::class, 'admin.statuses');
    resource('montage-firms', \App\Http\Controllers\MontageFirmController::class, 'admin.montage_firms');
    resource('montage-firms/{montage_firm}/employee', \App\Http\Controllers\MontageEmployeeController::class, 'admin.montage_employees');

    resource('date-settings', \App\Http\Controllers\DateSettingController::class, 'admin.date_settings');

    # Activity type
    resource('activity-type', \App\Http\Controllers\ActivityController::class, 'admin.activity_type');


    #Tech_conditions routes
    Route::get('/tech_conditions', [\App\Http\Controllers\TechConditionController::class, 'index'])->name('admin.tech_conditions.index');
    Route::post('/tech_conditions', [\App\Http\Controllers\TechConditionController::class, 'store'])->name('admin.tech_conditions.store');
    Route::get('/tech_conditions/{recommendation}/create', [\App\Http\Controllers\TechConditionController::class, 'create'])->name('admin.tech_conditions.create');
    Route::post('/tech_conditions/{tech_condition}/upload', [\App\Http\Controllers\TechConditionController::class, 'upload'])->name('admin.tech_conditions.upload');
    Route::get('/tech_conditions/{tech_condition}/edit', [\App\Http\Controllers\TechConditionController::class, 'edit'])->name('admin.tech_conditions.edit');

    # Admin
    Route::get('technic/dashboard',[\App\Http\Controllers\TechnicDashboardController::class,'index'])->name('technic.dashboard');
    Route::get('region/{region}/propositions',[\App\Http\Controllers\TechnicDashboardController::class,'show'])->name('technic.propositions');
    Route::get('/general_settings', [\App\Http\Controllers\SettingsController::class, 'index'])->name('admin.general_settings.index');
    Route::post('/general_settings', [\App\Http\Controllers\SettingsController::class, 'update'])->name('admin.general_settings.update');


    Route::get('/profile/{user}/edit', [\App\Http\Controllers\ProfileController::class, 'edit'])->name('admin.profile.edit');
    Route::put('/profile/{user}', [\App\Http\Controllers\ProfileController::class, 'update'])->name('admin.profile.update');
    Route::put('/profile/password/{user}', [\App\Http\Controllers\ProfileController::class, 'update'])->name('admin.profile_password.update');
    Route::get('/logout', [\TCG\Voyager\Http\Controllers\VoyagerController::class, 'logout'])->name('admin.logout');

    Route::get('/add-equipment', [App\Http\Controllers\EquipmentController::class, 'add'])->name('admin.equipment.add');
    Route::get('/add-type/{equipment?}', [App\Http\Controllers\EquipmentController::class, 'types'])->name('admin.equipment.type');
});

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::get('/', function () {
    return redirect('/admin');
});


Route::get('admin/', function () {
    if (auth()->user() == null) return redirect()->route('voyager.login');
//    if(checkMac()) {
//        auth()->logout();
//        return redirect('/');
//    }
    $role_name = auth()->user()->role->name;
    if ($role_name == 'admin') return view('voyager::index');
    elseif ($role_name == 'administrator') return redirect()->route('admin.employees.index');
    elseif ($role_name == 'director') return redirect()->route('admin.statistics.index');
    elseif ($role_name == 'technic') return redirect()->route('technic.dashboard');
    elseif ($role_name == 'region') return redirect()->route('admin.propositions.index');
    elseif ($role_name == 'designer') return redirect()->route('admin.projects.process');
    elseif ($role_name == 'montage_firm') return redirect()->route('admin.montages.index');
    elseif ($role_name == 'engineer') return redirect()->route('admin.projects.process');
})->name('voyager.dashboard');
